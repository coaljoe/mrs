package main

import (
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
	//. "rx/math"
)

type TrainCarView struct {
	*View
	m    *TrainCar
	node *rx.Node
}

func newTrainCarView(m *TrainCar) *TrainCarView {
	v := &TrainCarView{
		View: newView(),
		m:    m,
	}
	return v
}

func (v *TrainCarView) load() {
	//path := "res/enemies/enemy1/enemy1.dae"
	//path := "res/ship/ship.dae"
	//path := "res/models/primitives/box/box.dae"
	path := "res/models/traincar/traincar.dae"
	v.model.Load(path)
	//v.node = v.model.GetNode("body")
	//v.node = v.model.GetNode("box")
	v.node = v.model.Nodes()[0]
	v.node.SetScale(Vec3{4, 4, 4})
	//__xpp(v.node.Mesh.Material())
	//pp(v.node.Mesh.Material().Texture())
	if v.node == nil {
		panic("no v.node")
	}
	v.loaded = true
}

func (v *TrainCarView) spawn() {
	if !v.loaded {
		v.load()
	}
	//v.node.SetRot(Vec3{90, 0, -90})
	//v.node.SetRot(Vec3{0, 0, 90})
	rx.Rxi().Scene.Add(v.node)
	//v.spawned = true
	//pp(v.node.Material().Diffuse)
}

func (v *TrainCarView) destroy() {
	rx.Rxi().Scene.RemoveMeshNode(v.node)
}

func (v *TrainCarView) update(dt float64) {
	v.node.SetPos(v.m.Pos())
	v.node.SetRot(v.m.Rot())
}
