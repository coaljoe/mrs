package main

import (
	//"lib/xlog"
	//"github.com/go-test/deep"
	"kristallos.ga/lib/xlog"
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
	"flag"
	"fmt"
	"github.com/go-gl/glfw/v3.3/glfw"
	"os"
	"strconv"
	"strings"
)

func main() {
	println("main()")
	initApp()

	game, app := initBaseGame()
	initDefaultGame()

	/*
		// set config
		rx.SetDefaultConf()
		//rx.ConfSetResPath("res")
		rx.ConfSetResPath("res/")
		//time.Sleep(1 * time.Second)
		//rx.ConfSetDebugDraw(true)
	*/

	//rx.Init()
	/*
		win := rx.NewWindow()
		//win.Init(640, 480)
		//win.Init(1024, 576)
		win.Init(960, 540) // 1920/2, 1080/2
		app := rx.NewApp(win)
		app.Init()
	*/

	rxi := rx.Rxi()
	_ = rxi
	//time.Sleep(1 * time.Second)
	//rxi := rx.Rxi()
	//rxi.SetResPath("res/")
	//rxi.SetResPath("/home/j/dev/misc/ltest/go/src/rx/res/")
	_ = rxi

	rxi.Renderer.DebugRenderer().GridSize = 500
	rxi.Renderer.DebugRenderer().GridCellSize = 10

	// setup camera
	c := game.scene.camera
	//c.setNamedCamera("topdown")
	c.setNamedCamera("iso")
	//pp(c.cam.Camera.Fov())
	c.cam.Camera.SetZoom(1)
	//c.cam.Camera.SetZoom(0.25)
	//c.cam.Camera.SetFov(200)
	//c.cam.Camera.SetFov(800)
	//c.cam.Camera.SetFov(275)
	//c.cam.Camera.SetFov(400) // real scale
	c.cam.Camera.SetFov(2000) // archi
	//c.cam.Camera.SetFov(450)

	//c.cam.Camera.SetZnear(0.1)
	c.cam.Camera.SetZnear(1)
	//c.cam.Camera.SetZfar(2000)
	//c.cam.Camera.SetZfar(10000)
	//c.cam.Camera.SetZfar(20000)
	c.cam.Camera.SetZfar(50000)
	//p(c.cam.Camera.Znear(), c.cam.Camera.Zfar())
	//c.cam.MoveByVec(Vec3{0, 0, 1000})
	//c.cam.MoveByVec(Vec3{0, 0, 5000})
	c.cam.MoveByVec(Vec3{0, 0, 10000})
	//pp(2)
	/*
		c.cam.Camera.SetZnear(1)
		c.cam.Camera.SetZfar(100)
		//c.cam.SetZfar(100)
		c.cam.Camera.SetFov(10)
		//c.pos = Vec3{5, 5, 5}
		c.setPos(Vec3{10, 5, 10})
		//c.pos = Vec3{15, 10, 15}
		//c.pos = Vec3{30, 30, 30}

		// new rot-based coords
		c.setPos(Vec3{10, -10, 5})
		c.setRot(Vec3{70.562, 0, 45})

		c.zoom = 1
		c.defaultZoom = 1
		p(c.cam.Camera.Zoom())
	*/

	// render target

	/*
		w, h := app.Win().Size()
		//rxi.Camera.AddRenderTarget("camRT", w, h, true)
		c.cam.AddRenderTarget("camRT", w, h, true)
	*/

	if true {
		f := game.field
		hmOpt := newHeightMapOptions()
		fgOpt := newFieldGenerateOptions()
		fgOpt.checkZlevels = false
		f.generate(16, 16, 16, 16, hmOpt, fgOpt)
		f.spawn()
	}

	if true {

		spr1 := newSprite()
		spr1.SetPos(Vec3{30, 0, 0})
		//spr1.pos = Vec3{0, 0, 0}
		spr1.load("res/buildings/building2/building2.png")
		spr1.spawn()
		spr1.mesh.Mesh.Material().Name = "qwe"
		spr1.update(1)

		/*
			spr := newSprite()
				spr.load("res/objects/trees/tree1/tree1.png")
				//spr.pos = Vec3{120 + float64(i)*40, -10, 0}
				spr.pos = Vec3{80, 0, 0}
				//spr.pos = Vec3{-120 + float64(i)*80, 200, 0}
				spr.spawn()
				spr.mesh.Mesh.Material().AlphaTest = false
				spr.mesh.Mesh.Material().Alpha = true
				spr.update(1.0)

				__xp(spr1)
				p("---")
				__xp(spr)
				p(spr1.mesh.Mesh.Material() == spr.mesh.Mesh.Material())
				p(spr1.mesh.Mesh == spr.mesh.Mesh)
				//pp(2)

			//__xpp(spr1, spr)
			//diff := deep.Equal(1, 2)
			//panic(diff)
		*/

		spr2 := newSprite()
		//spr2.pos = Vec3{120, 0, 0}
		spr2.SetPos(Vec3{80, -10, 0})
		spr2.load("res/buildings/building2/building2.png")
		spr2.spawn()
		spr2.update(1.0)

		for i := 0; i < 10; i++ {
			spr := newSprite()
			spr.load("res/buildings/building2/building2.png")
			//spr.pos = Vec3{120 + float64(i)*40, -10, 0}
			spr.SetPos(Vec3{-80 + float64(i)*40, -10 + float64(-i)*15, 0})
			//spr.pos = Vec3{-120 + float64(i)*80, 200, 0}
			spr.spawn()
			spr.update(1.0)
		}

		for j := 0; j < 10; j++ {
			if j%3 == 0 {
				continue
			}
			for i := 0; i < 10; i++ {
				if i%4 == 0 || i%5 == 0 {
					continue
				}
				/*
					spr := newSprite()
					spr.pos = Vec3{-400 + float64(i)*160, -800 + 200*float64(j), 0}
					spr.spawn()
					spr.update(1.0)
				*/
				//b := newBuilding("building2", ViewType_Sprite)
				//b := makeBuilding("building4")
				//b := makeBuilding("building5")
				b := makeBuilding("building6")
				b.SetPos(Vec3{-400 + float64(i)*160, -800 + 200*float64(j), 0})
				b.rotDeg = float64(randint(0, 360))
				//b.spawn()
				//pp(b)
				spawnEntity(b)
				//b.update(1.0)

				/*
					ah := rx.NewAxesHelper()
					ah.SetPos(b.Pos())
					ah.DrawOriginLink = true
					ah.Size = 20
					ah.Spawn()
				*/

				//game.scene.debugBox.SetVisible(true)
				/*
					//game.scene.debugBox.SetPos(b.Pos())
					game.scene.debugBox.SetPos(Vec3Zero)
					game.scene.debugBox.SetScale(Vec3{10000,10000,10000})
				*/
			}
		}

		for i := 0; i < 30; i++ {
			spr := newSprite()
			spr.load("res/objects/trees/tree1/tree1.png")
			//spr.pos = Vec3{120 + float64(i)*40, -10, 0}
			//spr.pos = Vec3{-160 + float64(i)*40, -10 + float64(-i)*15, 0}
			spr.SetPos(Vec3{-160 + float64(i)*20, -50 + float64(-i)*7.5, 0})
			//spr.pos = Vec3{-120 + float64(i)*80, 200, 0}
			spr.spawn()
			spr.mesh.Mesh.Material().AlphaTest = false
			spr.mesh.Mesh.Material().Alpha = true
			//spr.dim = Vec2{300, 300}
			spr.update(1.0)
			//__xpp(spr.mesh)
		}

		for i := 0; i < 30; i++ {
			spr := newSprite()
			spr.load("res/objects/trees/tree1/tree1.png")
			//spr.pos = Vec3{120 + float64(i)*40, -10, 0}
			//spr.pos = Vec3{-160 + float64(i)*40, -10 + float64(-i)*15, 0}
			spr.SetPos(Vec3{-160 + float64(i)*20, -150 + float64(-i)*7.5, 0})
			//spr.pos = Vec3{-120 + float64(i)*80, 200, 0}
			spr.spawn()
			spr.mesh.Mesh.Material().AlphaTest = false
			spr.mesh.Mesh.Material().Alpha = true
			//spr.dim = Vec2{300, 300}
			spr.update(1.0)
			//__xpp(spr.mesh)
		}

		for i := 0; i < 30; i++ {
			spr := newSprite()
			spr.load("res/objects/trees/tree2/tree2.png")
			spr.SetPos(Vec3{120 + float64(i)*40, -10, 0})
			//spr.pos = Vec3{-160 + float64(i)*40, -10 + float64(-i)*15, 0}
			//spr.pos = Vec3{-160 + float64(i)*20, -150 + float64(-i)*7.5, 0}
			//spr.pos = Vec3{-120 + float64(i)*80, 200, 0}
			spr.spawn()
			spr.mesh.Mesh.Material().AlphaTest = false
			spr.mesh.Mesh.Material().Alpha = true
			//spr.dim = Vec2{300, 300}
			spr.update(1.0)
			//__xpp(spr.mesh)
		}

		/*
			for i := 0; i < 80; i++ {
				spr := newSprite()
				spr.load("res/objects/trees/tree2/tree2.png")
				spr.pos = Vec3{-500 + float64(i)*25, -180, 0}
				//spr.pos = Vec3{-160 + float64(i)*40, -10 + float64(-i)*15, 0}
				//spr.pos = Vec3{-160 + float64(i)*20, -150 + float64(-i)*7.5, 0}
				//spr.pos = Vec3{-120 + float64(i)*80, 200, 0}
				spr.spawn()
				spr.mesh.Mesh.Material().AlphaTest = false
				spr.mesh.Mesh.Material().Alpha = true
				//spr.dim = Vec2{300, 300}
				spr.update(1.0)
				//__xpp(spr.mesh)
			}
		*/
	}

	sl := rx.NewSceneLoader()
	_ = sl

	// Generate roads
	if false {
		nodes := sl.Load("res/objects/roads/road1/road1.dae")
		sl.Spawn()
		node := nodes[0]
		node.Mesh.Material().Texture().SetMinFilter(rx.TextureFilterNearest)
		node.Mesh.Material().Texture().SetMagFilter(rx.TextureFilterNearest)

		yPos := -500.
		xPos := 100.
		roadTileLength := 10.0
		for i := 0; i < 100; i++ {
			n := node.Clone()
			n.SetPos(Vec3{xPos + float64(i)*roadTileLength, yPos, 0})
			//n.SetScale(Vec3{10, 10, 10})
			n.SetScale(Vec3{40, 40, 10})
			//node.SetScale(Vec3{1000, 1000, 1000})
			rxi.Scene.Add(n)

		}

		xPos = 400
		for i := 0; i < 100; i++ {
			n := node.Clone()
			n.SetPos(Vec3{xPos, yPos + float64(i)*roadTileLength, 0})
			n.SetRot(Vec3{0, 0, 90})
			//n.SetScale(Vec3{10, 10, 10})
			n.SetScale(Vec3{10, 10, 10})
			//node.SetScale(Vec3{1000, 1000, 1000})
			rxi.Scene.Add(n)

		}

	}

	// game block

	//sl.Load("res/test/bg/scene/model.dae")
	//sl.Load("res/test/bg/scene2/scene.dae")
	//sl.Load("res/test/bg/scene3/scene.dae")
	nodes := sl.Load("res/test/bg/kid/model.dae")
	sl.Spawn()

	nodes[0].SetPos(Vec3{2, 0.1, 0})
	//g3 = nodes[0]

	/*
		sl.Load("res/units/ground/heavytank/reds/model/model.dae")
		sl.Spawn()
	*/

	/*
		sl.Load("/home/j/dev/go/src/rx/res/test/sphere_array/spherearray.dae")
		sl.Spawn()
	*/

	pathScale := 2.0

	//path := game.pathgraph
	path := game.pgRailway
	path.beginPath()
	path.addPointPos(Vec3{0, 0, 0})
	//path.addPointPos(_newPathPoint(Vec3{10, 0, 0}))
	//path.addPointPos(_newPathPoint(Vec3{10, 10, 0}))
	//path.addPointPos(_newPathPoint(Vec3{20, 10, 0}))
	//path.addPointPos(_newPathPoint(Vec3{20, 10, 0}))
	//path.addPointPos(_newPathPoint(Vec3{27.77, 0, 0}))
	path.addPointPos(Vec3{50 * pathScale, 50 * pathScale, 0})
	path.addPointPos(Vec3{100 * pathScale, 50 * pathScale, 0})
	path.endPath()
	//path.spawn()
	spawnEntity(path)

	path2 := game.pgRoad
	spawnEntity(path2)

	/*
		path := game.pathgraph
		path.addPointPos(_newPathPoint(Vec3{0, 0, 0}))
		path.addPointPos(_newPathPoint(Vec3{28, 0, 0}))
		spawnEntity(path)
	*/

	if false {
		p("pg dump:")
		//dumpDepth(game.pathgraph, 20)
		dumpDepth(game.pgRailway.points[0], 20)
		//dump(game.pathgraph.g.Edges())
		//p(game.pathgraph.points[1].linkedPoints())
		pp(2)
	}

	//dump(path.points)

	//pp(path.points)
	p("angles ->", path.points[0].angles())
	//p("angles ->", path.points[1].angles())
	//p("angles ->", path.points[2].angles())
	//pp(2)

	var t *Train
	if true {
		t = newTrain()
		tc1 := newTrainCar()
		tc2 := newTrainCar()
		tc3 := newTrainCar()
		tc4 := newTrainCar()
		t.addCar(tc1)
		t.addCar(tc2)
		t.addCar(tc3)
		t.addCar(tc4)
		//tc1.vehicle.speed = 100
		//tc1.vehicle.speed = 1
		p("train speed:", t.getSpeed())
		spawnEntity(tc1)
		spawnEntity(tc2)
		spawnEntity(tc3)
		spawnEntity(tc4)
		//pp(t.cars)

		if false {
			for i := 0; i < 5; i++ {
				tc := newTrainCar()
				t.addCar(tc)
				spawnEntity(tc)
			}
		}

		//tc1.SetRot(Vec3{0, 0, 45})
		//p(tc1.Pos())
		//p(tc1.frontPoint())
		//p(tc1.rearPoint())
		//pp(2)

		//t.setPos(Vec3{10, 10, 0})
		t.attachToPathPoint(path.points[0])
		//t.setDPos(Vec3{10, 10, 0})
		//t.setDPos(Vec3{20, 20, 0})
		//t.setSpeed(2)
		//t.setSpeed(5)
		//t.moveToNextPathPoint()
	}

	// Make arc
	if true {
		game.pgRailway.beginPath()
		//lp := game.pathgraph.getPointByPos(Vec3{20, 10, 0})
		lp := game.pgRailway.getPointByPos(Vec3{100 * pathScale, 50 * pathScale, 0})
		game.pgRailway.lastPoint = lp
		//r := 20
		xsh := 0.0
		for i := 0; i < 5; i++ {
			//game.pathgraph.addPointPos(_newPathPoint(Vec3{30 + float64(i)*5, 10 - xsh, 0}))
			game.pgRailway.addPointPos(Vec3{lp.pos.X() + 20 + float64(i)*5, lp.pos.Y() - xsh, 0})
			xsh += 5.0 * float64(i)
		}
		game.pgRailway.endPath()
	}

	if true {
		rt := game.railtool
		rt.railwayType = railwayTypes[2]
		rt.buildRoad(game.pgRailway.lastPoint.pos, Vec3{2000, -20, 0})
		rt.railwayType = rt.defaultRailwayType() // Reset rail type
		//xpp(game.pathgraph.lastLink.railway)
		//pp(2)
	}

	if true {
		// path2
		pg := game.pgRailway
		color := Vec3{0.8, .2, .2}
		//pg.addPointPos(_newPathPoint(Vec3{10, 10, 0}))
		//pg.lastLink.color = color
		p1 := pg.addFreePointPos(Vec3{20, 20, 0})
		p2 := pg.addFreePointPos(Vec3{30, 20, 0})
		//p1.color = Vec3{.2, 0.8, 0.8}
		pg.linkPoints(p1, p2)
		//pg.addPointPos(_newPathPoint(Vec3{20, 20, 0}))
		pg.lastLink.color = color
	}

	if true {
		rt := game.railtool
		rt.buildRoad(game.pgRailway.points[2].pos, Vec3{200, 300, 0})
	}

	//game.pathgraph.deletePoint(game.pathgraph.points[3])
	//pp(2)

	game.pgRailway.saveDot()
	game.pgRoad.saveDot()
	//pp(2)

	// init game

	//initNewGame(app)

	//c.pan(20, 0)
	//c.pan(0, 20*0.5)
	//c.pan(0, 20)

	for app.Step() {
		//println("main.step")
		//rx.DebugDrawCube(Vec3Zero, 5, Vec3{1.0, .5, .5})
		dt := app.GetDt()
		game.update(dt)
	
		if t != nil {
			t.update(dt)
		}
		//spr1.update(dt)

		if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyG) {
			//pp(2)
			vars.dShowPathGraph = !vars.dShowPathGraph
		}

		{

			//pp(c.cam.Pos(), c.cam.Rot())
			camMoveSpeed := 20.0
			shX := 0.0
			shY := 0.0
			_ = shX
			_ = shY
			k := 1.0
			//k := 0.5 // 1:2 iso
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyLeft) {
				shX = -camMoveSpeed
			}
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyRight) {
				shX = camMoveSpeed
			}
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyUp) {
				shY = camMoveSpeed * k
			}
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyDown) {
				shY = -camMoveSpeed * k
			}
			c.pan(shX, shY)
		}
		
		//d := rx.NewDrawer()
		//d.DrawCube(100, Vec3{1, 1, 0})
		//d.DrawLineV(Vec3{-100, -100, 30}, Vec3{100, 100, 30}, d.ColorRed)
		//d.DrawLineV(Vec3{-100, -100, 50}, Vec3{100, 100, 50}, d.ColorGreen)


		app.Flip()
	}

	println("exiting...")
}

/////////
// Init

var (
	_log        *xlog.Logger
	overrideRes bool // FIXME: add normal app module
	fullscreen  bool
)

func initApp() {
	println("Init() begin")
	SetDefaultVars()

	/*
		var optAppRoot = flag.String("root", "", "appRoot")
		var optRes = flag.String("res", "", "resolution WxH")
		var optFs = flag.Bool("fs", false, "full screen")
		var optDisableMusic = flag.Bool("disable_music", false, "disable music")
		var optDisableSound = flag.Bool("disable_sound", false, "disable sound")
		var optLevel = flag.String("level", "", "play level")

		flag.Parse()
		fmt.Println("args:", os.Args)
		//pp(2)
		if *optAppRoot != "" {
			//pp(*optAppRoot)
			appRoot := *optAppRoot
			fmt.Println("new appRoot:", appRoot)
			os.Chdir(appRoot)
		}
		if *optRes != "" {
			ss := strings.Split(*optRes, "x")
			sx, _ := strconv.Atoi(ss[0])
			sy, _ := strconv.Atoi(ss[1])
			fmt.Printf("new res: (%d, %d)\n", sx, sy)
			//resizeWindow(sx, sy)
			vars.resX = sx
			vars.resY = sy
			overrideRes = true
		}
		if *optFs == true {
			// XXX: comment this out to get -res option working with the -fs
			sx, sy := rx.GetDesktopResolution()
			vars.resX = sx
			vars.resY = sy

			overrideRes = true
			fullscreen = true
		}
		if *optDisableMusic == true {
			conf.disableMusic = true
		}
		if *optDisableSound == true {
			conf.disableSound = true
		}
		if *optLevel != "" {
			vars.playLevel = *optLevel
		}
	*/

	var optRes = flag.String("res", "", "resolution WxH")
	flag.Parse()
	fmt.Println("args:", os.Args)

	if *optRes != "" {
		ss := strings.Split(*optRes, "x")
		sx, _ := strconv.Atoi(ss[0])
		sy, _ := strconv.Atoi(ss[1])
		fmt.Printf("new res: (%d, %d)\n", sx, sy)
		//resizeWindow(sx, sy)
		vars.resX = sx
		vars.resY = sy
		overrideRes = true
	}

	// Seed
	//rand.Seed(time.Now().UTC().UnixNano())

	// Create logger
	_log = xlog.NewLogger("mrs", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	_log.SetOutputFile("mrs.log")
	_log.SetWriteTime(true)
	_log.SetWriteFuncName(true)
	println("Init() done")
}

func initBaseGame() (*Game, *rx.App) {
	println("InitGame() begin")

	/*
		// Load settings
		defaultSettingsFile := strings.ToLower(AppName) + "_default.json"
		customSettingsFile := strings.ToLower(AppName) + ".json"

		// Pick default or custom settings file
		fp := ""
		if found, err := exists(customSettingsFile); found {
			if err != nil {
				panic(err)
			}
			fp = customSettingsFile
		} else if found, err = exists(defaultSettingsFile); found {
			if err != nil {
				panic(err)
			}
			fp = defaultSettingsFile
		} else {
			pp("Settings file not found in:", defaultSettingsFile, customSettingsFile)
		}

		// Load settings
		s := newSettings()
		s.load(fp)
		s.verify()
		//pdump(s)

		// Apply settings
		if !overrideRes {
			vars.resX = s.getInt("resX")
			vars.resY = s.getInt("resY")
		}
	*/

	/*
		// Set config
		rx.SetDefaultConf()
		//rx.ConfSetResPath("/home/j/dev/go/s/rx/res/") // Fixme
		rx.ConfSetResPath("res/") // Fixme
		// Set extra conf
		rx.Conf.EnableCulling = false
		//rx.Conf.EnableCulling = true
	*/

	xconf := rx.NewDefaultConf()
	xconf.ResPath = "res"
	xconf.EnableCulling = false

	//rx.Init()
	//win := rx.NewWindow()
	//win.Init(640, 480)
	//win.Init(1024, 576)
	//rxi := rx.Init(1024, 576)
	var rxi *rx.Rx
	if !fullscreen {
		rxi = rx.Init(vars.resX, vars.resY, xconf)
	} else {
		rxi = rx.InitFullscreen(vars.resX, vars.resY, xconf)
	}
	app := rxi.App
	game := newGame(app)
	// Set debug game options
	game.setGameOptions(newDebugGameOptions())
	println("InitGame() done")

	app.Win().SetTitle(AppName + " (rev. " + BuildRevision + ")")

	// Update globals
	_rxi = rxi

	// Finally
	//s.save("test_settings_out.json")

	return game, app
}

func initDefaultGame() {
	//game.createNewDefaultGame()
	game.start()
}
