package main

import (
	"fmt"
)

type HasId interface {
	getId() int
}

type EntitySys struct {
	*GameSystem
	//objs map[int]*Entity
}

func newEntitySys() *EntitySys {
	s := &EntitySys{}
	s.GameSystem = newGameSystem("EntitySys", "Entity system", s)
	return s
}

func (s *EntitySys) getEntityById(id int) interface{} {
	for _, oi := range s.getElems() {
		switch oi.(type) {
		case HasId:
			o := oi.(HasId)
			p("processing id:", o.getId())
			if o.getId() == id {
				return oi
			}
		}
		//fmt.Printf("--> %T\n", oi)
	}
	//s.listEntities()
	s.listEntities()
	p("Entity not found; id:", id, "len:", len(s.getElems()))
	return nil
}

func (s *EntitySys) listEntities() {
	for _, oi := range s.getElems() {
		fmt.Printf("--> %T\n", oi)
	}
}

/*
func (s *EntitySys) addEntity(o *Entity) {
	if s.hasElem(o) {
		panic("already have obj; id: " + Itoa(o.id))
	}
	s.addElem(o)
}

func (s *EntitySys) removeEntity(o *Entity) {
	s.removeElem(o)
}

func (s *EntitySys) hasEntity(o *Entity) bool {
	return s.hasElem(o)
}
*/

func (s *EntitySys) update(dt float64) {
	/*
		for _, el := range s.getElems() {
			//en := el.(UpdateableI)
			//en.update(dt)

			en := el.(EntityI)
			if en.isDead() {
				pf("destroying entity: %p %v\n", en, en)
				pv(en)
				for _, el := range s.getElems() {
					pf("before -> %p %v\n", el, el)
				}
				destroyEntity(en)
				p("YYY:", s.hasElem(en))
				p("ZZZ:")
				for _, el := range s.getElems() {
					pf("after -> %p %v\n", el, el)
				}
				//s.listElems()

			} else {
				updateEntity(en, dt)
			}
		}
	*/
	for i := 0; i < s.size(); i++ {
		//en := el.(UpdateableI)
		//en.update(dt)
		el := s.elems[i]

		en := el.(EntityI)
		if en.isDead() {
			destroyEntity(en)
			i--
		} else {
			updateEntity(en, dt)
		}

	}
}
