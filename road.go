package main

type RoadType struct {
	WayType
}

// TODO: use map with ids?
var roadTypes []*RoadType

func init() {
	rt := &RoadType{
		WayType: WayType{
			id:          0,
			name:        "rt_20kmh",
			description: "20 km/h road",
			maxSpeed:    20,
			//maxSpeed: 10,
			cost: 5,
		},
	}
	roadTypes = append(roadTypes, rt)
}
