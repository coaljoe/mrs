// Height map for field generation.
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"math"
	"os"

	"github.com/drbig/perlin"
)

type HeightMapOptions struct {
	Alpha   float64 // Alpha Factor
	Beta    float64 // Beta Factor
	Octaves int     // Number of octaves
	Seed    int64   // Random Seed
	Smooth  bool    // "smooth (continuous) gradient"

	NoiseScale float64 // Noise Scale
	//LevelWeights [8]float64 // Posterization level weights
	SeaAmt float64 // Sea/Ground amount

	SaveFixedStep bool // Save images with fixed step values rather than values from regions
}

// Default HeightMap options.
func newHeightMapOptions() HeightMapOptions {
	return HeightMapOptions{
		Alpha: 1.5,
		Beta:  2,
		//Alpha: 3.5,
		//Beta:  4,
		Octaves: 6,
		//Octaves: 2,
		Seed:   41,
		Smooth: true,
		//Smooth:  false,
		//NoiseScale: 4.0,
		//NoiseScale: 1.0,
		NoiseScale: 2.0,
		//LevelWeights: [8]float64{1.0, 1.0, 1.0, 1.0,
		//	1.0, 1.0, 1.0, 1.0},
		//SeaAmt: 0.5,
		//SeaAmt: 1.0,
		SeaAmt:        0.0,
		SaveFixedStep: true,
	}
}

type HeightMap struct {
	data        [][]int // -n.. 0.. +n; 0 - sea/ground level(?); posterized
	dataOrig    [][]int // Smooth, unchanged hm, used for debug; normalized to 0..256 range
	w, h        int
	levelStep   int // 256/8 // 32
	numLevels   int // 8
	minLevel    int // -4
	maxLevel    int // +3 (0 included)
	regions     [8]int
	opt         HeightMapOptions
	initialized bool
}

func newHeightMap() HeightMap {
	hm := HeightMap{
		levelStep:   256 / 8, // / 32,
		initialized: false,
	}
	hm.numLevels = 256 / hm.levelStep
	//hm.minLevel = -(hm.numLevels / 2)
	//hm.maxLevel = hm.numLevels / 2
	hm.minLevel = int(ZLevel_Min)
	hm.maxLevel = int(ZLevel_Max)
	// Regions
	hm.regions = [8]int{
		/*
			10, // sea3
			20, // sea2
			30, // sea1
			40, // sea0
			50, // ground0
			60, // ground1
			70, // ground2
			80, // ground3
		*/
		/*
			32,     // sea3
			32 * 2, // sea2
			32 * 3, // sea1
			32 * 4, // sea0
			32 * 5, // ground0
			32 * 6, // ground1
			32 * 7, // ground2
			32 * 8, // ground3
		*/
		/*
			int(0.125 * 256), // sea3
			int(0.25 * 256),  // sea2
			int(0.375 * 256), // sea1
			int(0.5 * 256),   // sea0
			int(0.625 * 256), // ground0
			int(0.75 * 256),  // ground1
			int(0.875 * 256), // ground2
			int(1.0 * 256),   // ground3
		*/
		/*
			int(0.125 * 256), // sea3
			int(0.25 * 256),  // sea2
			int(0.375 * 256), // sea1
			int(0.5 * 256),   // sea0
			int(0.625 * 256), // ground0
			int(0.75 * 256),  // ground1
			int(0.875 * 256), // ground2
			255,              // ground3
		*/
		32,       //32, // sea3
		72,       //64,  // sea2
		88,       //96, // sea1
		96,       //128, // sea0
		160 - 16, //160, // ground0
		176,      //192,  // ground1
		205,      // ground2
		255,      // ground3
		/*
			166,              // ground1
			168,              // ground2
			255,              // ground3
		*/
		//int(1.0 * 256),   // ground3
	}
	return hm
}

func (hm *HeightMap) generate(w, h int, opt HeightMapOptions) {
	hm.w = w
	hm.h = h
	hm.opt = opt

	// Fill data
	//data := hm.data
	hm.data = make([][]int, w)
	for i := range hm.data {
		hm.data[i] = make([]int, h)
	}
	//pp(hm.data)
	//pp(data[w][h])
	//pp(data[w-1][h-1])
	//pp(len(data))
	//hm.data = data

	// Initialize dataOrig
	hm.dataOrig = make([][]int, w)
	for i := range hm.dataOrig {
		hm.dataOrig[i] = make([]int, h)
	}

	// ./perlin -c -a 2 -b 2 -h 128 -w 128 -n 10 -s 3 -r 41 ex.png
	//g := perlin.NewGenerator(2, 2, 10, 41)
	//g := perlin.NewGenerator(1.5, 2, 6, 41)
	//g := perlin.NewGenerator(2, 2, 4, 41)
	g := perlin.NewGenerator(opt.Alpha, opt.Beta, opt.Octaves, opt.Seed)

	tileableNoise := func(x, y, w, h float64) float64 {
		return (g.Noise2D(x, y)*(w-x)*(h-y) +
			g.Noise2D(x-w, y)*(x)*(h-y) +
			g.Noise2D(x-w, y-h)*(x)*(y) +
			g.Noise2D(x, y-h)*(w-x)*(y)) / (w * h)
		/*
			(
				F(x, y) * (w - x) * (h - y) +
				F(x - w, y) * (x) * (h - y) +
				F(x - w, y - h) * (x) * (y) +
				F(x, y - h) * (w - x) * (y)
			) / (w * h)
		*/
	}

	/*
		noiseScale := opt.NoiseScale //4.0
		sx := noiseScale / float64(hm.w)
		sy := noiseScale / float64(hm.h)
	*/

	vMin := 255
	vMax := 04
	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			//rmin, rmax := -1.0, 1.0
			// Normalize fw/fh range to -1, 1
			//nx := -1.0 + float64(x)/(float64(hm.w)/2.0)
			//ny := -1.0 + float64(y)/(float64(hm.h)/2.0)
			/*
				nx := float64(x) * sx
				ny := float64(y) * sy
			*/

			/*
				local s=x/bufferwidth
				local t=y/bufferheight

				local nx=x1+s*(x2-x1)
				local ny=y1+s*(y2-y1)
			*/
			x1, x2 := 0, 1
			y1, y2 := 0, 1
			//x1, x2 := 0, 16
			//y1, y2 := 0, 16
			//x1, x2 := 0, hm.w
			//y1, y2 := 0, hm.h
			s := float64(x) / float64(hm.w)
			t := float64(y) / float64(hm.h)

			nx := float64(x1) + s*float64(x2-x1)
			//ny := float64(y1)+s*float64(y2-y1)
			ny := float64(y1) + t*float64(y2-y1)

			var nv float64
			if opt.Smooth {
				//nv = (g.Noise2D(nx, ny) + 1.0) / 2.0
				//nv = (tileableNoise(nx, ny, float64(hm.w), float64(hm.h)) + 1.0) / 2.0
				//nv = (tileableNoise(nx, ny, float64(x2-x1), float64(y2-y1)) + 1.0) / 2.0
				nv = tileableNoise(nx, ny, float64(x2-x1), float64(y2-y1))
			} else {
				//nv = g.Noise2D(nx, ny)
			}
			//nv = math.Pow(nv, 2.38)

			// Normalize nv to 0, 1 range
			nvNorm := (nv - -1.0) / (1.0 - -1.0)
			_ = nvNorm
			nvNorm = math.Pow(nvNorm, 2.4)
			//p(nv, nvNorm)
			//v := int(128.0 - (nv*256)/2)
			v := int(nvNorm * 256)
			//p(v)
			hm.dataOrig[x][y] = v

			if v < vMin {
				vMin = v
			}
			if v > vMax {
				vMax = v
			}
		}
	}
	//pp(hm.dataOrig)

	// Normalize dataOrig to 0..255 range
	//if false {
	if true {
		//range_ := vMax - vMin
		//pp(vMin, vMax, range_)
		rangeMin := 0
		rangeMax := 255
		for x := 0; x < hm.w; x++ {
			for y := 0; y < hm.h; y++ {
				v := hm.dataOrig[x][y]
				p("v1:", v)
				//v = (v-vMin)/range_
				//vNorm := (v - vMin) / (vMax - vMin)
				//vNorm := (v - 0) / (255 - 0)
				//vNorm = (255-0) * (v-min)/(max-min) + 0
				vNorm := (rangeMax-rangeMin)*(v-vMin)/(vMax-vMin) + rangeMin
				p("v2:", vNorm)
				hm.dataOrig[x][y] = vNorm
				//if v == 233 {
				//	pp(2)
				//}
			}
		}
		//pp(2)
	}

	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			v := hm.dataOrig[x][y]

			sucess := false
			for i, rv := range hm.regions {
				if v <= rv {
					//hm.data[x][y] = i
					//defaultLevel := 4
					//hm.data[x][y] = i - defaultLevel
					hm.data[x][y] = hm.ltoz(i)
					sucess = true
					break
				}
			}
			if !sucess {
				pp("hm value can't be handled by regions; v:", v, "regions:", hm.regions)
			}
			//hm.data[x][y] += 4

			// Adjust noise value with SeaAmt
			// Sea/Ground coeff.
			//sAmt := 1.0
			//sAmt := 0.0
			sAmt := opt.SeaAmt
			//sAmt = 1.5
			//gAmt = 2.0 - sAmt
			gAmt := 1.0 - sAmt
			_ = gAmt
			/*
				var nvSeaAdj float64
				if nv < 0.0 {
					nvSeaAdj = nv * sAmt
				} else {
					nvSeaAdj = nv * gAmt
				}
			*/
			//nvSeaAdj := nv*sAmt + nv*gAmt
			//nvSeaAdj := 0.5 + nv
			//nvSeaAdj := -1.0 + nv
			//nvSeaAdj := -1.0 + nv
			//nvSeaAdj := nv
			//_ = nvSeaAdj

			/*
				nvSeaAdj := nv
				k := -0.5
				if nvSeaAdj < 0.0 {
					nvSeaAdj = maxf(k+nvSeaAdj, -0.0)
				} else {
					nvSeaAdj = maxf(k+nvSeaAdj, 1.0)
				}
			*/

			// Rewrite v
			//v = int(128.0 - (nvSeaAdj*256)/2)
			// Normalize to 0, 255 range
			//v = int(((nvSeaAdj - -1.0) / (1.0 - -1.0)) * 255)

			/*
				nlevels := float64(hm.numLevels)
				v0 := sAmt * (1.0 / nlevels)
				v1 := sAmt * (1.0 / nlevels)
				v2 := sAmt * (1.0 / nlevels)
				v3 := sAmt * (1.0 / nlevels)
				//v0 = v1 = v2 = v3 = 0

				v4 := gAmt * (1.0 / nlevels)
				v5 := gAmt * (1.0 / nlevels)
				v6 := gAmt * (1.0 / nlevels)
				v7 := gAmt * (1.0 / nlevels)
			*/

			if false {
				// Posterize
				//fmt.Printf("%v ", v)
				// Map values in -4 .. +4 range, 0 is ground level 0
				//levelStep := 256/8 // 32
				//defaultLevel := 4
				defaultLevel := 4
				//defaultLevel := 3
				v = (v / hm.levelStep) - defaultLevel
			}

			// XXX upscale (for testing)
			//v += 3
			//v += 4
			//hm.data[x][y] = v
			//f.cells[x][y].z = v
		}
	}
	//pp(hm.data)
	//p(hm.data)
	//pp(hm.dataOrig)
	hm.initialized = true
	//pp(2)
}

func (hm *HeightMap) ztol(z int) int {
	return z + (hm.numLevels / 2)
}

func (hm *HeightMap) ltoz(l int) int {
	return l - (hm.numLevels / 2)
}

func (hm *HeightMap) load(path string) {
	_log.Inf("[HeightMap] load; path:", path)
	r, err := os.Open(path)
	if err != nil {
		println("cant read file: " + path)
		panic(err)
	}
	defer r.Close()

	im, _, err := image.Decode(r)
	if err != nil {
		panic(err)
	}

	tmp := im.Bounds()
	iw, ih := tmp.Dx(), tmp.Dy()
	if hm.w != iw || hm.h != ih {
		pp("image and hightmap sizes don't match")
	}

	// Convert to grayscale
	im2 := image.NewGray(im.Bounds())
	draw.Draw(im2, im.Bounds(), im, image.Pt(0, 0), draw.Src)

	// XXX fixme: should be y, x instead?
	for y := 0; y < ih; y++ {
		for x := 0; x < iw; x++ {
			v := int(im2.GrayAt(x, y).Y)
			//v := 0
			//p("v:", v)

			// XXX: fixme use value pack/reconstruct functions
			// Map values in -4 .. +4 range, 0 is ground level 0
			levelStep := 256 / 8 // 32
			defaultLevel := 4
			v = (v / levelStep) - defaultLevel
			hm.data[x][y] = v
			//f.cells[x][y].z = ZLevel(v)
		}
	}
}

func (hm *HeightMap) save(path string, saveOrig bool) {
	_log.Inf("[HightMap] save; path:", path)
	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, hm.w, hm.h)
	im := image.NewGray(rect)

	/*
		// XXX fixme: should be y, x instead?
		for y := 0; y < hm.h; y++ {
			for x := 0; x < hm.w; x++ {
				if !saveOrig {
					//p(hm.data[x][y], uint8(hm.data[x][y]))
					// Reconstructed value
					rv := 128 + (hm.data[x][y] * hm.levelStep)
					c := color.Gray{Y: uint8(rv)}
					im.SetGray(x, y, c)
				} else {
					v := hm.dataOrig[x][y]
					c := color.Gray{Y: uint8(v)}
					im.SetGray(x, y, c)
				}
			}
		}
	*/

	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			if !saveOrig {
				//p(hm.data[x][y], uint8(hm.data[x][y]))
				// Reconstructed value
				rv := -1
				if hm.opt.SaveFixedStep {
					//rv = 128 + (hm.data[x][y] * hm.levelStep)
					rv = hm.ztol(hm.data[x][y]) * hm.levelStep
				} else {
					ri := hm.ztol(hm.data[x][y])
					p("2>>>", ri, hm.data[x][y])
					rv = hm.regions[ri]
					p(">>>", hm.data[x][y], ri, rv)
				}
				c := color.Gray{Y: uint8(rv)}
				im.SetGray(x, y, c)
			} else {
				v := hm.dataOrig[x][y]
				c := color.Gray{Y: uint8(v)}
				im.SetGray(x, y, c)
			}
		}
	}
	//pp(2)

	png.Encode(f, im)

	_log.Inf("heightmap saved to: ", path)
}

// Warning: ground elevation color/value is inverted
func (hm *HeightMap) saveColor(path string) {
	_log.Inf("[HightMap] saveColor; path:", path)
	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, hm.w, hm.h)
	im := image.NewRGBA(rect)

	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			//p(hm.data[x][y], uint8(hm.data[x][y]))
			// Reconstructed value
			rv := -1
			if hm.opt.SaveFixedStep {
				//rv := 128 + (hm.data[x][y] * hm.levelStep)
				rv = hm.ztol(hm.data[x][y]) * hm.levelStep
			} else {
				ri := hm.ztol(hm.data[x][y])
				rv = hm.regions[ri]
			}
			//c := color.Gray{Y: uint8(rv)}
			//im.SetGray(x, y, c)
			var c color.RGBA
			//c := color.RGBA{uint8(rv), uint8(rv), uint8(rv), 255} // Set gray
			div := 2 // 8
			_ = div
			if ZLevel(hm.data[x][y]).isSea() {
				//c = color.RGBA{uint8(rv / div), uint8(rv / div), uint8(rv), 255}
				c = color.RGBA{0, 0, 128 + uint8(rv), 255}
			} else if ZLevel(hm.data[x][y]).isGround() {
				//rvInv := rv - int(ZLevel_GroundMax)
				//rvInv := int(ZLevel_GroundMax+ZLevel_Ground0) - rv
				rvInv := (int(ZLevel_GroundMax)*hm.levelStep + int(ZLevel_Ground0)*hm.levelStep) - rv
				rvInvAbs := iabs(rvInv)
				_ = rvInvAbs
				//p("rv:", rv, "rvInv:", rvInv, "rvInvAbs:", rvInvAbs)
				//c = color.RGBA{uint8(rvInv / div), uint8(rvInv), uint8(rvInv / div), 255}
				//c = color.RGBA{128 + uint8(rvInv/div), 128 + uint8(rvInv), 128 + uint8(rvInv/div), 255}
				c = color.RGBA{0, 128 + uint8(rvInv), 0, 255} // XXX 128 is arbitrary, not checked properly
				//c = color.RGBA{128 + uint8(rvInvAbs / div), 128 + uint8(rvInvAbs), 128 + uint8(rvInvAbs / div), 255}
			} else {
				pp("unknown ZLevel:", hm.data[x][y])
			}
			im.SetRGBA(x, y, c)
		}
	}

	png.Encode(f, im)
	//pp(2)

	_log.Inf("color heightmap saved to: ", path)
}

func (hm *HeightMap) saveTxt(path string) {
	_log.Inf("[HightMap] saveTxt; path:", path)
	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	// Write header
	f.WriteString(fmt.Sprintf("# hm.w: %d, hm.h: %d\n", hm.w, hm.h))
	f.WriteString("# 0")
	for x := 1; x < hm.w; x++ {
		f.WriteString(fmt.Sprintf("%3d", x))
	}
	f.WriteString("\n\n\n")

	// Write data
	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			f.WriteString(fmt.Sprintf(" %2d", hm.data[x][y]))
		}
		f.WriteString("\n")
	}

	_log.Inf("heightmap txt saved to: ", path)
}

func (hm *HeightMap) clear() {
	if !hm.initialized || len(hm.data) == 0 {
		pp("heightmap is not initialized;",
			hm.initialized, len(hm.data))
	}
	//pp(len(hm.data), hm.w, hm.h, hm.w*hm.h)
	//pp(hm.data)
	//pp(hm.data[0][0])
	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			//p(x, y)
			hm.data[x][y] = 0
		}
	}
}
