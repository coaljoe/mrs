package main

import (
	. "kristallos.ga/rx/math"
	. "math"
	"math/rand"
	//"github.com/StefanSchroeder/Golang-Ellipsoid/ellipsoid"
)

type RotDir int

const (
	CW  RotDir = 1
	CCW RotDir = -1
)

func (rd RotDir) String() string {
	switch rd {
	case 0:
		return "NONE"
	case CW:
		return "CW"
	case CCW:
		return "CCW"
	default:
		panic("unknown RotDir")
	}
}

func geomDegrees(r float64) float64 {
	return r * (180 / Pi)
}

func degBetween(p1, p2 Vec2) float64 {
	x1, y1 := p1.X(), p1.Y()
	x2, y2 := p2.X(), p2.Y()
	return geomDegrees(Atan2(y2-y1, x2-x1))
}

func degBetweenI(x1, y1, x2, y2 int) float64 {
	return degBetween(Vec2{float64(x1), float64(y1)},
		Vec2{float64(x2), float64(y2)})
}

func distancePos2(a, b Vec2) float64 {
	//return Sqrt(Pow(b.x - a.x, 2) - Pow(b.y - a.y, 2))
	return Hypot(b.X()-a.X(), b.Y()-a.Y())
}

/*
func Lerp(a, b, t float64) float64 {
	return a + (b-a)*t
}
*/

func inRadius(x, y, r, cx, cy float64) bool {
	/*
	   cx, cy - center of circle
	   x, y - point
	   r - radius
	*/
	if Pow(x-cy, 2)+Pow(y-cy, 2) <= Pow(r, 2) {
		return true
	} else {
		return false
	}
}

func roundPrec(x float64, prec int) float64 {
	var rounder float64
	pow := Pow(10, float64(prec))
	intermed := x * pow
	_, frac := Modf(intermed)
	if frac >= 0.5 {
		rounder = Ceil(intermed)
	} else {
		rounder = Floor(intermed)
	}

	return rounder / pow
}

// False - negative dir
// True - positive dir
func shortestRotDir(from, to float64) RotDir {
	// Special case for angles starting with 0 and negative targets (fixme?)
	if from == 0 && to < 0 {
		return CW
	}
	// (cpu_facing-player_degree+360)%360>180
	if Mod(to-from+360, 360) > 180 {
		return CW
	} else {
		return CCW
	}
}

// Normalize angle.
func unwrapAngle(a float64) float64 {
	/*
		//return Mod(a, 360)
		if a >= 0 {
			tmpa := Mod(a, 360)
			if tmpa == 360 {
				return 0
			} else {
				return tmpa
			}
		} else {
			return Mod(360-(1*a), 360)
		}
	*/

	/*
		// [0,360]
		// From: http://stackoverflow.com/questions/11498169/dealing-with-angle-wrap-in-c-code
		a = Mod(a, 360)
		if a < 0 {
			a += 360
		}
	*/

	// [-180,180]
	a = Mod(a+180, 360)
	if a < 0 {
		a += 360
	}
	return a - 180
}

// Like lerp but for angles.
func rotAngle(start, end, amount float64) float64 {
	//shortest_angle := ((((end - start) % 360) + 540) % 360) - 180;
	rotDir := shortestRotDir(start, end) // Fixme?
	if rotDir == CW {
		return start - amount
	} else {
		return start + amount
	}
}

func maxi(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func mini(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func random(min, max float64) float64 {
	return rand.Float64()*(max-min) + min
}

func randint(min, max int) int {
	return min + rand.Intn(max-min)
}

/*
// Check if from angle meets to angle
func CheckDAngle(from, to float64) bool {
  sd := ShortestRotDir(from, to)

  if (sd && from <= to) ||
    (!sd && from >= to) {
    return true
  } else {
    return false
  }
}
*/

// Doesn't work?
/*
func RotAngle(start, end, amount float64) float64 {
  //shortest_angle := ((((end - start) % 360) + 540) % 360) - 180;
  shortest_angle := Mod((Mod(end - start, 360) + 540), 360) - 180
  return shortest_angle * amount
}
*/

func iabs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func convertSphericalToCartesian(lat, lon float64) Vec3 {
	// From https://rbrundritt.wordpress.com/2008/10/14/conversion-between-spherical-and-cartesian-coordinates-systems/
	//var earthRadius = 6367.0 //radius in km
	var earthRadius = 6371.0 //radius in km
	lat = Radians(lat)
	lon = Radians(lon)
	var x = earthRadius * Cos(lat) * Cos(lon)
	var y = earthRadius * Cos(lat) * Sin(lon)
	var z = earthRadius * Sin(lat)
	return Vec3{x, y, z}
}
