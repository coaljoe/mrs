// Модуль для удобного управления направлением (ориентацией)
package main

//import "math"

//const _float_unset = math.NaN()

type Dir struct {
	// Rot angle
	rot float64
	// Src angle
	_sRot *float64
	// Dst angle
	_dRot *float64
	// Rot speed
	rotSpeed float64
	// Rot direction
	rotDir RotDir // CW, CCW
}

func (d *Dir) angle() float64     { return d.rot }
func (d *Dir) setAngle(v float64) { d.rot = unwrapAngle(v) }

// Return value should be nil-checked (optional)
func (d *Dir) dRot() *float64 { return d._dRot }
func (d *Dir) sRot() *float64 { return d._sRot }

func newDir() Dir {
	d := Dir{}
	d._unsetDRot() // Reset
	return d
}

func (d *Dir) rotTo(to float64, speed float64) {
	if d.rot == to {
		return
	}
	//pp("rotTo", to, speed)
	d._setDRot(&to)
	d.rotSpeed = speed
}

func (d *Dir) hasD() bool {
	return d._dRot != nil
}

func (d *Dir) isZero() bool {
	return d.angle() == 0.0
}

// Check if Dir is not moving, or locked on target.
func (d *Dir) locked() bool {
	if !d.hasD() {
		return true
	}
	return d.rot == *d._dRot
}

// nil to unset(?)
func (d *Dir) _setDRot(v *float64) {
	//pf("v: %#v\n", v)
	if v == nil {
		d._unsetDRot()
		pp("derp")
		return
	}
	//pf("v: %#v\n", v)
	//pf("v: %#v\n", d._sRot)
	tmp1 := unwrapAngle(d.rot)
	d._sRot = &tmp1
	tmp2 := unwrapAngle(*v)
	d._dRot = &tmp2
	d.rotDir = shortestRotDir(*d._sRot, *d._dRot)
}

func (d *Dir) _unsetDRot() {
	d._sRot = nil
	d._dRot = nil
	d.rotDir = 0 // None
}

func (d *Dir) update(dt float64) {
	//p("dir upd")
	//p(_log.LogLevelStr())
	if !d.hasD() || d.locked() {
		return
	}

	_log.Trc("d.rot", d.rot, d.rotSpeed, d.rotSpeed*dt, d.rotDir)
	d.rot = rotAngle(d.rot, *d._dRot, d.rotSpeed*dt)
	_log.Trc("d.rot", d.rot, *d._dRot, d.rotSpeed, d.rotDir)
	//p(d.rot)

	rotDone := false
	switch d.rotDir {
	case CW:
		if d.rot <= *d._dRot {
			rotDone = true
		}
	case CCW:
		if d.rot >= *d._dRot {
			rotDone = true
		}
	}

	if rotDone {
		_log.Trc("rot done")
		d.rot = *d._dRot
		d.rotSpeed = 0
		d._unsetDRot()
	}
}
