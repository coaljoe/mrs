// Позиционный компонент.
// Отвечает за позиционирование и ориентацию, а также перемещение,
// но не трансформацию (для этого есть Transform)
//
package main

import (
	. "kristallos.ga/rx/math"
	"math"
)

// component
type Position struct {
	obj *Obj // link
	//pos      Vec3
	//dPos Vec3
	//sPos Pos
	sPos  Vec3
	dPos_ *Vec3 // Fixme
	//velocity    float64
	speed float64
	//rotVelocity float64
	dir Dir
	//dDir      Dir
	//travelTime float64 // Total travel time (tt)
	stepTime  float64
	i         float64
	direction Vec3
	distance  float64
	moving    bool
	// Component
	//entityId int
}

func (p *Position) dPos() Vec3  { return *p.dPos_ }
func (p *Position) dPos2() Vec2 { return p.dPos().ToVec2() }

func newPosition(obj *Obj) *Position {
	p := &Position{
		obj: obj,
		//pos:    Vec3Zero,
		//dPos: Vec3Zero,
		dir: newDir(),
		//dDir: NewDir(),
	}

	return p
}

func (p *Position) setDPos(_p Vec3) {
	p.dPos_ = &_p
	p.sPos = p.obj.Pos()
	p.stepTime = 0 // Fixme: move to update code?
	p.distance = _p.Sub(p.sPos).Len()
	p.direction = _p.Sub(p.sPos).Norm()
	p.moving = true
}

func (p *Position) setDPos2(_p Vec2) {
	obj := p.obj
	p.setDPos(Vec3{_p.X(), _p.Y(), obj.PosZ()})
}

func (p *Position) isMoving() bool {
	if p.dPos_ != nil {
		return true
	}
	return false
}

func (p *Position) hasDPos() bool {
	return p.dPos_ != nil
}

func (p *Position) setDirTowardsD() {
}

func (p *Position) destroy() {
}

// From:
// answers.unity.com/questions/414829/any-one-know-maths-behind-this-movetowards-functio.html
func moveTowards(current, target Vec3, maxDistanceDelta float64) Vec3 {
	fmagnitude := func(v Vec3) float64 {
		return math.Sqrt(math.Pow(v[0], 2) + math.Pow(v[1], 2) + math.Pow(v[2], 2))
	}
	_ = fmagnitude
	fdiv := func(v Vec3, b float64) Vec3 {
		return Vec3{v[0] / b, v[1] / b, v[2] / b}
	}
	a := target.Sub(current)
	//magnitude := fmagnitude(a)
	magnitude := a.Len()
	if magnitude <= maxDistanceDelta || magnitude == 0.0 {
		return target
	}
	//return current + a / magnitude * maxDistanceDelta;
	r1 := fdiv(a, magnitude).MulScalar(maxDistanceDelta)
	return current.Add(r1)
}

func (p *Position) update(dt float64) {
	//_log.Dbg("%F", dt)
	//__ps()
	//pp("doneMoving:", p.dPos_ == nil, p.speed)
	obj := p.obj
	//var _ = p

	// Dir update
	p.dir.update(dt)
	obj.SetRotZ(p.dir.angle()) // Always set orientation

	//newPos := obj.Pos().Add(Vec3{1 * dt, 1 * dt, 0})

	if p.dPos_ == nil {
		return
	} else {
		// Begin move
		//p.stepTime = 0
	}

	// Check
	if p.stepTime == 0 {
		p.stepTime = game.timer.time
	}

	doneMoving := false

	//sPos := obj.pos2()
	//dPos := p.dPos2()

	//nextPos := sPos

	// Linear
	{

		if p.speed == 0.0 {
			return
		}

		var _ = `
			// From:
			//  http://stackoverflow.com/questions/33092912/moving-a-panel-on-its-x-a-certain-amount-in-unity-5-2
			// See also:
			//  https://www.reddit.com/r/Unity3D/comments/41x6zt/vector3movetowards_without_slow

						// Total time of travel A -> B [constant]
			//tt := (cell_size / p.speed) * 10
			tt := 100.0
			//tt := (float64(cell_size) / 40.0) * 10.0
			//sPos := Pos{u.x * cell_size, u.y * cell_size}
			//sPos := obj.Pos2()
			//dPos := p.dPos

			//fmt.Println(sPos, dPos, p.speed, tt)

			//distCovered := (game.timer.time - p.stepTime) * dt
			distCovered := (game.timer.time - p.stepTime) * p.speed
			step := distCovered / tt
			nextPos := Lerp2(sPos, dPos, step)
			//fmt.Println("step", fmt.Sprintf("%f", step))
			//fmt.Println(fmt.Sprintf("%f %f", game.timer.time, s.stepTime))

			if roundPrec(step, 2) < 1.0 {
				obj.setPos2(nextPos)
			} else {
				obj.setPos2(p.dPos2())
				doneMoving = true
			}
			`
		/*
			a := p.speed * dt
			if (a > 1.0) {
				a = 1.0
			}
			sPos := p.sPos
			dPos := *p.dPos_
			pos := sPos.Add(dPos.Sub(sPos)).MulScalar(a)
			obj.SetPos(pos)
		*/

		/*
			sPos := p.sPos
			dPos := *p.dPos_
			diffVec := dPos.Sub(sPos)
			diffVec = diffVec.Norm()
			diffVec = diffVec.MulScalar(p.speed)
			obj.SetPos(diffVec)
		*/
		// From:
		// https://gamedev.stackexchange.com/questions/23447/moving-from-ax-y-to-bx1-y1-with-constant-speed
		/*
			sPos := p.sPos
			dPos := *p.dPos_
			if p.moving {
				pos := obj.Pos()
				pos = pos.Add(p.direction.MulScalar(p.speed * dt))
				// distance
				if pos.Sub(sPos).Len() >= p.distance {
					pos = dPos
					p.moving = false
				}
				obj.SetPos(pos)
			}
		*/
		//sPos := p.sPos
		dPos := *p.dPos_
		if p.moving {
			pos := obj.Pos()
			pos = moveTowards(pos, dPos, p.speed*dt)
			// distance
			if pos == dPos {
				p.moving = false
			}

			obj.SetPos(pos)
		}
	}

	_ = doneMoving
}
