package main

import (
	ps "kristallos.ga/lib/pubsub"
	"kristallos.ga/rx"
	"github.com/go-gl/glfw/v3.3/glfw"
	//ps "lib/pubsub"
	//. "rx/math"
)

type GameGameState struct {
	*GameState
}

func newGameGameState() *GameGameState {
	gs := &GameGameState{
		GameState: newGameState("game"),
	}
	return gs
}

func (gs *GameGameState) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	//pp(key)
	if key == glfw.KeyEscape {
		setGameState("menu")
	}
}

func (gs *GameGameState) start() {
	//game.createNewDefaultGame()
	// XXX fixme
	if !game.gameCreated {
		game.start()
	}

	// Ship test

	/*
		sh := game.ship
		sh.health.showHealth()
		sh.health.setHealth(10)
		sh.health.showHealth()
		sh.health.takeHealth(10)
		if !sh.health.isAlive() {
			println("killed", fmt.Sprintf("(id %d)", sh.id))
		}
		spawnEntity(sh)
	*/

	// Enemy test

	/*
		//e1 := newEnemyEntity()
		//e1 := makeEnemyEntity("enemy2")
		e1 := makeEnemy("enemy1")
		e1.health.showHealth()
		//e1.spawn()
		spawnEntity(e1)
		//e1.SetPos(Vec3{0, 0, -100})
		//pp(e1.Pos())
		//pp(e1.color)

		//e2 := makeEnemy("enemy2")
		e2 := makeEnemy("enemy1")
		e2.SetPos(Vec3{10, 0, 0})
		spawnEntity(e2)
	*/

	//p(e1.getView().(*EnemyView).node.Pos())
	//p(e2.getView().(*EnemyView).node.Pos())
	//pp(e1.Pos(), e2.Pos())

	/*
		e3 := makeEnemy("enemy1")
		e3.SetPos(Vec3{18, 0, 0})
		spawnEntity(e3)
	*/

	//playMusic("res/music/test.mp3")
	//l := newLevel1()
	//l.start()
	//game.playLevel("level1")
	//game.playLevel(vars.playLevel)

	/*
		g := game.gui
		g.rgi.SheetSys.SetActiveSheet(g.mainSheet)
	*/
	sub(rx.Ev_key_press, gs.onKeyPress)
}

func (gs *GameGameState) stop() {
	sub(rx.Ev_key_press, gs.onKeyPress)
	//rx.Conf.DisableExitOnEscape = false
	game.finishLevel()
	game.stop()
}

func (gs *GameGameState) update(dt float64) {

}
