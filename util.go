package main

import (
	//"fmt"
	"os"
	"reflect"
	"time"

	"github.com/davecgh/go-spew/spew"
	//"github.com/tylerwince/godbg"

	"kristallos.ga/lib/debug"
	debug_extra "kristallos.ga/lib/debug/extra"
)

var p = debug.P
var pp = debug.Pp
var pf = debug.Pf
var pv = debug.Pv
var pz = debug.Pz
var dump = debug.Dump
var pdump = debug.Pdump
var __xp = debug_extra.Xp
var __xpp = debug_extra.Xpp
var pq = debug_extra.Pq

var __p = p
var _ = __p

var __pp = pp
var _ = __pp

var __ps = debug.Ps

func cknil(c interface{}) {
	if c == nil || reflect.ValueOf(c).IsNil() {
		panic("Check nil failed.")
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func sleepsec(t float64) {
	// Sleep float seconds
	time.Sleep(time.Duration(t*1000) * time.Millisecond)
}

func dumpDepth(s interface{}, depth int) {
	cs := spew.NewDefaultConfig()
	cs.ContinueOnMethod = true
	cs.MaxDepth = depth
	cs.Indent = "\t"
	cs.Dump(s)
	//spew.Dump(s)
}

// Exists returns whether the given file or directory exists or not
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
