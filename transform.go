package main

import (
	//"bitbucket.org/coaljoe/rx"
	. "kristallos.ga/rx/math"
	"kristallos.ga/rx/transform"
)

type Transform struct {
	// Local?
	//pos, rot Vec3
	//*gfx.Transform
	*transform.Transform
	//parent *Transform // Backlink
}

func newTransform() *Transform {
	t := &Transform{
		//pos: Vec3Zero,
		//rot: Vec3Zero,
		//Transform: gfx.NewTransform(),
		Transform: transform.NewTransform(),
		//parent: nil,
	}
	return t
}

/*
func (t *Transform) SetParent(p *Transform) {
  t.parent = p
}

func (t *Transform) AbsPos() Vec3 {
  r := t.pos
  if t.parent != nil {
    r = r.Add(t.parent.AbsPos())
    //r = r.Add(t.parentTransform.Pos())
  }
  return r
}
*/
/*
func (t *Transform) GetWorldTransformPos() Vec3 {
  t.SetPos
}
*/

//func (t *Transform) Pos() Vec3     { return t.pos } // Fixme?
//func (t *Transform) SetPos(p Vec3) { t.pos = p }
/*
func (t *Transform) SetPos(p Vec3) {
	t.pos = p
	t.Transform.SetPos(t.pos)
}
*/

/*
func (t *Transform) Pos2() Pos {
	return Pos{t.pos.X, t.pos.Y}
}
*/

/*
func (t *Transform) SetPos2(p Pos) {
	t.pos.X = p.x
	t.pos.Y = p.y
	// Update t.Transform
	t.SetPos(t.pos)
}
*/

func (t *Transform) pos2() Vec2 {
	p := t.Pos()
	return Vec2{p.X(), p.Y()}
}

func (t *Transform) setPos2(p Vec2) {
	z := t.Pos().Z()
	t.SetPos(Vec3{p.X(), p.Y(), z})
}

//func (t *Transform) Rot() float64 { return t.rot }
//func (t *Transform) SetRot(r float64) { t.rot = r }
/*
func (t Transform) Translate(v Vec3) {
	t.pos = t.pos.Add(v)
}
*/
