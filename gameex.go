// Game extras.
package main

import (
	"time"
)

// Game run-time options.
// (a particular running game options/variables)
type GameOptions struct {
	dirty           bool    // GameOptions need update/apply
	_enableFogOfWar bool    // Read-only
	_gameSpeed      float64 // Read-only
}

func (_go *GameOptions) setGameSpeed(v float64) {
	_go._gameSpeed = v
	_go.dirty = true
}

func (_go *GameOptions) setEnableFogOfWar(v bool) {
	_go._enableFogOfWar = v
	_go.dirty = true
}

func newDefaultGameOptions() GameOptions {
	return GameOptions{
		_enableFogOfWar: true,
		_gameSpeed:      1.0, // Normal speed
		//_gameSpeed: 5.0,
	}
}

func newDebugGameOptions() GameOptions {
	gOpt := newDefaultGameOptions()
	gOpt._enableFogOfWar = false
	//gOpt._gameSpeed = 0.0 // Test
	gOpt.dirty = true
	return gOpt
}

type GameTime struct {
	realTimeStart time.Time
	timeStart     time.Time
	time          time.Time
	// One game day takes 2220ms (2.22 seconds) real time.
	// A game year thus takes 810.3 seconds, or 13.505 minutes.
	dayLength       int     // in ms
	secondLength    float64 // in ms
	dayLengthSec    float64 // in float seconds
	secondLengthSec float64 // in float seconds
}

func newGameTime() *GameTime {
	t, err := time.Parse(
		time.RFC3339,
		"1970-01-01T00:00:00+00:00")
	if err != nil {
		panic(err)
	}
	const secsInDay = 86400
	gt := &GameTime{
		time:            t,
		timeStart:       t,
		realTimeStart:   time.Now(),
		dayLength:       2220, // (2.22 seconds)
		dayLengthSec:    2.22,
		secondLengthSec: 2.22 / secsInDay,
	}
	gt.secondLength = float64(gt.dayLength) / secsInDay

	return gt
}

func (gt *GameTime) update(dt float64) {
	d_ := game.timer.dt()
	var _ = `
	//d := time.Duration(d_) * time.Second
	secondLengtSecs := gt.secondLength * 1000
	_ = secondLengtSecs
	dtms := dt * 1000
	//d := time.Duration(dt*1000) * time.Millisecond
	//d := time.Duration(dt*secondLengtSecs) * time.Second
	//d := time.Duration(dtms*gt.secondLength*1000) * time.Millisecond
	d := time.Duration(dtms*gt.secondLength*1000) * time.Millisecond
	gt.time = gt.time.Add(d)
	//dx := time.Now().Sub(gt.realTimeStart).Nanoseconds() / 1000000 // milli
	dx := time.Now().Sub(gt.realTimeStart).Nanoseconds() / 1000 // micro
	millis := (dx * 25)
	gt.time = time.Unix(0, millis*int64(time.Millisecond))
	`
	// Game time in seconds
	gameSec := d_ / gt.secondLengthSec
	gameNSec := gameSec * 1000000000
	d2 := time.Duration(gameNSec) * time.Nanosecond
	gt.time = gt.timeStart.Add(d2)
	//p("XXX", "d_:", d_, "dt:", dt, d, "dx:", dx, "gameSec:", gameSec, "frame:", game.frame, "app.dt", game.app.GetDt())
	p("XXX", "d_:", d_, "dt:", dt, "gameSec:", gameSec, "frame:", game.frame, "app.dt", game.app.GetDt())

	delta := time.Now().Sub(gt.realTimeStart).Nanoseconds() / 1000000 // milli
	deltaS := time.Now().Sub(gt.realTimeStart).Seconds()
	p("XXX delta:", delta, "deltaS:", deltaS)
	if delta > 2220 {
		//game.pause(true)
		//pp(2)
	}
}
