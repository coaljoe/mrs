package main

import (
	//"gonum.org/v1/gonum/graph"
	. "kristallos.ga/rx/math"
	"fmt"
	"gonum.org/v1/gonum/graph/encoding/dot"
	"gonum.org/v1/gonum/graph/iterator"
	"gonum.org/v1/gonum/graph/simple"
	"os"
	"os/exec"
	"strings"
)

type PathType int

const (
	PathType_Point PathType = iota
	PathType_Tile
)

type LinkMode int

const (
	LinkMode_Single LinkMode = iota
	LinkMode_Path
)

/*
// XXX WayType can have 2 or more types (?)
// such as road and tram
// https://wiki.openstreetmap.org/wiki/Highway_tagging_samples/urban
// XXX use binary mask flags?
type WayType int

const (
	WayType_Unknown WayType = iota
	WayType_Railway
	WayType_Road
	WayType_Monorail
)
*/

//type SidewalkType int

// A link between two pathpoints.
// undirectional so both pathpoints can have same link.
type PathLink struct {
	simple.Edge
	id       int
	p1       *PathPoint
	p2       *PathPoint
	pathType PathType
	// Link to path itself
	//path *Path
	color Vec3
	// Link to linked path
	//linkedPath *Path
	// Linked objects like signals(?)
	linkedObjs []ObjI
	//stations []*Station
	// Registered cars on the node
	//cars []*TrainCar
	cars []*VehicleI
	// Type of railway
	railway *RailwayType
	// Type of road
	road *RoadType
	// Type of monorail
	monorail *MonorailType
	// Type of tram
	tram *TramType
	//wayType WayType // XXX Rename to WayInfo?
	//// Properties
	//sidewalk SidewalkType
	sidewalk bool
}

func newPathLink(pg *PathGraph, p1, p2 *PathPoint) *PathLink {
	pl := &PathLink{
		Edge:  simple.Edge{F: p1, T: p2},
		id:    _ider.GetNextId("PathLink"),
		p1:    p1,
		p2:    p2,
		color: Vec3One,
	}

	if pg.wayTypeId == WayTypeId_Railway {
		pl.railway = game.railtool.defaultRailwayType()
	} else if pg.wayTypeId == WayTypeId_Road {
		pl.road = game.roadtool.defaultRoadType()
	} else {
		pp("error: unknown wayTypeId:", pg.wayTypeId)
	}

	return pl
}

func (pl *PathLink) String() string {
	return fmt.Sprintf("PathLink<id: %d, p1 -> [id: %d, pos: %s], p2 -> [id: %d, pos: %s]>",
		pl.id, pl.p1.id, pl.p1.pos, pl.p2.id, pl.p2.pos)
}

type PathPoint struct {
	//  Id relative to specific pathgraph
	// XXX not unique
	id int
	// Absolute, unique id
	uid int
	pos Vec3
	// Link to pathgraph
	pg *PathGraph
	//prev       *PathPoint
	//next       *PathPoint
	// Links to connected pathpoints / edges
	// XXX can be acquired with function similar to
	// linkedPoints() either
	links map[int]*PathLink
}

func (pp *PathPoint) ID() int64 { return int64(pp.id) }

//func (pp *PathPoint) ID() int64 { return int64(pp.pid) }

// XXX cannot be created outside of pathgraph
/*
func _newPathPoint(pos Vec3) *PathPoint {
	pp := &PathPoint{
		id:    -1,
		uid:   _ider.GetNextId("PathPoint"),
		pos:   pos,
		links: make(map[int]*PathLink),
	}
	return pp
}
*/

func (pp *PathPoint) DOTID() string {
	return fmt.Sprintf("(%d)  \n%.2f %.2f %.2f", pp.id, pp.pos.X(), pp.pos.Y(), pp.pos.Z())
}

func (pp *PathPoint) angles() []float64 {
	_log.Dbg("angles")
	var _ = `
	if pp.next == nil {
		__pp("can't calculate angle")
	}

	deg := degBetween(pp.pos.ToVec2(), pp.next.pos.ToVec2())
	`
	degs := make([]float64, 0)
	var _ = `
	//nodes := game.pathgraph.g.From(pp.ID())
	//dump(nodes)
	//it := game.pathgraph.g.From(pp.ID())
	it := pp.pg.g.From(pp.ID())
	__p("it.len", it.Len())
	dump(it)
	//for node := range nodes.Iterator() {
	for it.Next() {
		pointId := int(it.Node().ID())
		__pp(pointId)
		node := game.pathgraph.points[pointId]
		deg := degBetween(pp.pos.ToVec2(), node.pos.ToVec2())		
		degs = append(degs, deg)
	}
	`
	for _, lpp := range pp.linkedPoints() {
		deg := degBetween(pp.pos.ToVec2(), lpp.pos.ToVec2())
		degs = append(degs, deg)
	}
	dump(degs)
	//__pp(2)
	return degs
}

// XXX why not use map[id]*PathPoint in PathPoint like with links?
func (pp *PathPoint) linkedPoints() []*PathPoint {
	_log.Dbg("linkedPoints from:", pp)
	if pp.pg == nil {
		__pp("pg can't be nil")
	}
	nodes := pp.pg.g.From(pp.ID()).(*iterator.OrderedNodes).NodeSlice()
	//nodes := it.NodeSlice()
	//dump(nodes)
	r := make([]*PathPoint, 0)
	for _, node := range nodes {
		r = append(r, node.(*PathPoint))
	}
	return r
}

// XXX not tested
func (pp *PathPoint) getLinkToPoint(p *PathPoint) *PathLink {
	for _, pl := range pp.links {
		if pl.p1 == p || pl.p2 == p {
			return pl
		}
	}
	__p(pp.links)
	__p(p.links)
	__pp("error: link to point not found; pp:", pp, "p:", p)
	return nil
}

func (pp *PathPoint) String() string {
	return fmt.Sprintf("PathPoint<id: %d, pos: %s>", pp.id, pp.pos.String())
}

// entity?
type PathGraph struct {
	*Obj
	points map[int]*PathPoint
	// XXX warning: hack: storing both F: T: and T: F:
	// keys -> links for proper undirectional matching. FIXME
	links     map[simple.Edge]*PathLink
	lastPoint *PathPoint
	lastLink  *PathLink
	wayTypeId WayTypeId
	linkMode  LinkMode
	//g graph.Undirect
	// XXX should use UndirectedMatrix in future?
	g *simple.UndirectedGraph
}

func newPathGraph(wayTypeId WayTypeId) *PathGraph {
	pg := &PathGraph{
		Obj:       newObj("PathGraph"),
		points:    make(map[int]*PathPoint),
		links:     make(map[simple.Edge]*PathLink),
		wayTypeId: wayTypeId,
		linkMode:  LinkMode_Single,
		//g: graph.Undirect{},
		g: simple.NewUndirectedGraph(),
	}
	pg.view = newPathGraphView(pg)
	//game.pathsys.addElem(p)
	return pg
}

func (pg *PathGraph) size() int {
	return len(pg.points)
}

func (pg *PathGraph) _newPathPoint(pos Vec3) *PathPoint {
	pp := &PathPoint{
		id:    -1,
		uid:   _ider.GetNextId("PathPoint"),
		pos:   pos,
		links: make(map[int]*PathLink),
	}

	// XXX monkeypatch point?
	pp.id = _ider.GetNextId(fmt.Sprintf("PathPoint_WayTypeId_%s", pg.wayTypeId.String()))

	return pp
}

/*
func (pg *PathGraph) lastPoint() *PathPoint {
	if len(pg.points) > 0 {
		return pg.points[len(pg.points) - 1]
	} else {
		return nil
	}
}
*/

func (pg *PathGraph) _addPoint(point *PathPoint) (*PathPoint, *PathLink) {
	println()
	//_log.Dbg("[PathGraph] _addPoint:", point, connectToLast)
	//_log.Dbg("_", point, connectToLast)
	_log.Dbg("%F;", point)
	p("pg.wayTypeId", pg.wayTypeId)
	p("pg.size", pg.size())
	p("pg.lastPoint", pg.lastPoint)

	var existingPoint *PathPoint

	connectToLast := false
	if pg.linkMode == LinkMode_Path {
		connectToLast = true
	}

	if pg.hasPoint(point) {
		existingPoint = pg.getPointByPos(point.pos)

		//pp("path already has this point (or point with same position):", point)
		p("warn: path already has this point (or point with same position):", point)
		p("warn: existingPoint ->", existingPoint)
		/*
			if connectToLast {
				existingPoint := pg.getPointByPos(point.pos)
				point = existingPoint
			}
		*/
		point = existingPoint
	}

	var ret *PathLink

	//lastPoint := pg.lastPoint()
	//pg.points = append(pg.points, point)
	prevLastPoint := pg.lastPoint
	pg.points[point.id] = point
	// Update last point
	pg.lastPoint = point
	if pg.size() > 1 {
		// Connect to last inserted point
		if connectToLast {
			//__p("prev ->", p.points[p.size() - 2])
			//__p("point ->", point)
			//p.points[p.size()-2].next = point
			//point.prev = p.points[p.size()-2]
			if prevLastPoint != nil {
				_log.Dbg("connecting points:", point, prevLastPoint)
				// XXX should edges be normalized by node.ID()? X < Y
				//pg.g.SetEdge(simple.Edge{F: prevLastPoint, T: point})
				// Test multigraph (not working: not adding new edges
				// (overriding? the order is random?))
				//pg.g.SetEdge(simple.Edge{F: point, T: prevLastPoint})
				// Set links
				var _ = `
				pl := newPathLink(pg, prevLastPoint, point)
				//pg.links[point.id] = pl
				//pg.links[prevLastPoint.id] = pl
				// XXX hack, store both directions. FIXME
				pg.links[simple.Edge{F: prevLastPoint, T: point}] = pl
				pg.links[simple.Edge{F: point, T: prevLastPoint}] = pl
				point.links[prevLastPoint.id] = pl
				prevLastPoint.links[point.id] = pl
				pg.lastLink = pl
				`
				pl := pg.linkPoints(prevLastPoint, point)
				ret = pl
			}
		}
	}

	point.pg = pg

	//game.pathsys.autoconnectPaths()
	//pg.autoconnectPoints()
	if existingPoint == nil {
		_log.Inf("added pathpoint:", point)
	} else {
		_log.Inf("XXX no new points added. returning existing point:", point)
	}
	_log.Inf("done %F")
	println("\n\n\n")
	//pg.saveDot()
	return point, ret
}

func (pg *PathGraph) addPoint(point *PathPoint) *PathPoint {
	addedPP, _ := pg._addPoint(point)
	return addedPP
}

// Pos version of addPointPos
func (pg *PathGraph) addPointPos(pos Vec3) *PathPoint {
	point := pg._newPathPoint(pos)
	pg._addPoint(point)
	return point
}

func (pg *PathGraph) addFreePoint(point *PathPoint) *PathPoint {
	addedPP, _ := pg._addPoint(point)
	return addedPP
}

// Pos version of addFreePoint
func (pg *PathGraph) addFreePointPos(pos Vec3) *PathPoint {
	point := pg._newPathPoint(pos)
	pg._addPoint(point)
	return point
}

func (pg *PathGraph) addPointFromPos(fromPos Vec3, point *PathPoint) *PathPoint {
	fromPP := pg.getPointByPos(fromPos)
	//pp(fromPP)
	pg.lastPoint = fromPP
	addedPP, _ := pg._addPoint(point)
	return addedPP
}

func (pg *PathGraph) hasPoint(point *PathPoint) bool {
	if pg.points[point.id] != nil {
		return true
	}

	for _, p := range pg.points {
		if p.pos == point.pos {
			__p("XXX warn: path has point with same position:", point.pos)
			return true
		}
	}

	return false
}

func (pg *PathGraph) createPathLink(from, to Vec3) *PathLink {
	_log.Dbg("%F")
	// Reset linkmode
	prevLinkMode := pg.linkMode
	pg.resetLinkMode()

	var fromPP *PathPoint
	existingFromPP := pg.getPointByPosCheck(from)
	if existingFromPP != nil {
		fromPP = existingFromPP
	} else {
		_log.Inf("%F warning: no from point, adding one")
		fromPP = game.pgRoad._newPathPoint(from)
		pg.addPoint(fromPP)
		//pp(game.pgRoad.points)
	}

	var toPP *PathPoint
	existingToPP := pg.getPointByPosCheck(to)
	if existingToPP != nil {
		toPP = existingToPP
	} else {
		//game.pathgraph.lastPoint = from
		toPP = pg._newPathPoint(to)
		pg.addPoint(toPP)
	}

	pl := pg.linkPoints(fromPP, toPP)

	var _ = `
	//toPP.color = Vec3{1, 0, 1}
	//game.pgRoad.addPointFromPos(from, toPP)
	game.pgRoad.addPointFromPos(from, toPP)
	//pp(game.pgRoad.points)
	//fromPP := game.pgRoad.getPointByPos(from)
	//pp(game.pgRoad.links)

	fromPP := game.pgRoad.getPointByPos(from)
	//pp(game.pgRoad.links)
	pl := toPP.getLinkToPoint(fromPP)
	`

	// Restore linkmode
	pg.linkMode = prevLinkMode

	_log.Dbg("done %F")
	return pl
}

func (pg *PathGraph) linkPoints(p1, p2 *PathPoint) *PathLink {
	_log.Dbg("%F", p1, p2)
	//pg.g.SetEdge(simple.Edge{F: p1, T: p2})
	pl := newPathLink(pg, p1, p2)
	pg.g.SetEdge(pl)
	//pg.links[point.id] = pl
	//pg.links[prevLastPoint.id] = pl
	// XXX hack, store both directions. FIXME
	pg.links[simple.Edge{F: p1, T: p2}] = pl
	pg.links[simple.Edge{F: p2, T: p1}] = pl
	p1.links[p2.id] = pl
	p2.links[p1.id] = pl
	pg.lastLink = pl

	var _ = `
	// Retrieve
	xi := pg.g.Edge(p1.ID(), p2.ID())
	x := xi.(*PathLink)
	p(x.color)
	pp(x)
	`

	pubS("ev_pathgraph.link_points", pg, p1, p2, pl)

	return pl
}

func (pg *PathGraph) unlinkPoints(p1, p2 *PathPoint) {
	_log.Dbg("%F", p1, p2)
	pg.g.RemoveEdge(p1.ID(), p2.ID())
	pg.g.RemoveEdge(p2.ID(), p1.ID())
	delete(p1.links, p2.id)
	delete(p2.links, p1.id)
}

func (pg *PathGraph) deletePoint(point *PathPoint) {
	_log.Dbg("%F", point)
	if !pg.hasPoint(point) {
		pp("no such point", point)
	}
	// XXX buggy? not well tested
	for _, pl := range point.links {
		pg.unlinkPoints(pl.p1, pl.p2)
	}
	pg.g.RemoveNode(point.ID())
	delete(pg.points, point.id)
}

func (pg *PathGraph) getPointByPosCheck(pos Vec3) *PathPoint {
	//_log.Dbg("%F")
	for _, p := range pg.points {
		if p.pos == pos {
			return p
		}
	}
	//p(pg.wayTypeId)
	//p(pg.points)
	//pp("error: pathpoint with such position not found:", pos)
	return nil
}

func (pg *PathGraph) getPointByPos(pos Vec3) *PathPoint {
	//_log.Dbg("%F")
	p := pg.getPointByPosCheck(pos)
	//__p("XXX", p)
	if p == nil {
		pp("error: pathpoint with such position not found:", pos)
		return nil
	}
	//__p("YYY")
	return p
}

func (pg *PathGraph) getLinkByPos(pos1, pos2 Vec3) *PathLink {
	//_log.Dbg(">", pos1, pos2)
	_log.Dbg("%F;", pos1, pos2)
	p1 := pg.getPointByPos(pos1)
	p2 := pg.getPointByPos(pos2)
	pl := pg.links[simple.Edge{F: p1, T: p2}]
	if pl == nil {
		x := pg.links[simple.Edge{F: p1, T: p2}]
		y := pg.links[simple.Edge{F: p2, T: p1}]
		p(x, y)
		pp("link not found")
	}
	return pl
}

func (pg *PathGraph) beginPath() {
	pg.linkMode = LinkMode_Path
}

func (pg *PathGraph) endPath() {
	pg.linkMode = LinkMode_Single
}

func (pg *PathGraph) resetLinkMode() {
	pg.linkMode = LinkMode_Single
}

func (pg *PathGraph) traceLine(headPP *PathPoint, carPP *PathPoint, pos Vec3, dir float64, length float64) Vec3 {
	if headPP == carPP {

	} else {
		pp(2)
	}
	return Vec3Zero
}

func (pg *PathGraph) autoconnectPoints() {
	_log.Dbg("autoconnectPoints")
	for _, pp := range pg.points {
		p1 := pp.pos
		_ = p1
		for _, otherPP := range pg.points {
			if otherPP == pp {
				continue
			}
			p2 := otherPP.pos
			_ = p2

			/*
				// Same position, make links
				if p1 == p2 && (pp.prev == nil || pp.next == nil) && (otherPP.prev == nil || otherPP.next == nil) {
					p("[PathGraph] added links between:", p1, p2, pp, otherPP)
					pp.linkedPath = otherPP.path
					otherPP.linkedPath = pp.path
				}
			*/
		}
	}
}

func (pg *PathGraph) saveDot() {
	_log.Dbg("%F")
	result, _ := dot.Marshal(pg.g, "", "", "  ")
	s := string(result)
	//fmt.Print(string(result))
	//path := "/tmp/mrs_pg.dot"
	wayTypeIdStr := strings.ToLower(pg.wayTypeId.String())
	path := vars.tmpDir + "/mrs_pg_" + wayTypeIdStr + ".dot"
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	f.WriteString(s)
	f.Close()
	_log.Inf("saved .dot file to:", path)
	_, err2 := exec.Command("dot", "-Tpng", path, "-o"+path+".png").Output()
	//pp(out)
	if err2 != nil {
		_log.Err(err2) // XXX doesn't really work
	}
	_log.Inf("saved .dot.png file to:", path+".png")
}

func (pg *PathGraph) update(dt float64) {
	pg.Obj.update(dt)
}
