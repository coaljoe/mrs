// Gamestate manager
package main

//var gameStates map[string]*GameState = make(map[string]*GameState)
var gameStates map[string]GameStateI = make(map[string]GameStateI)

// Current gamestate
var gameState GameStateI

type GameStateI interface {
	//enterState()
	//exitState()
	xname() string
	start()
	stop()
	update(dt float64)
}

type GameState struct {
	name   string
	parent *GameState
	child  *GameState
}

func (gs *GameState) xname() string { return gs.name }

func newGameState(name string) *GameState {
	gs := &GameState{
		name: name,
	}
	return gs
}

func initGameStates() {
	gameGS := newGameGameState()
	gameStates["game"] = gameGS
	/*
		menuGS := newMenuGameState()
		gameStates["menu"] = menuGS
		gameOverGS := newGameOverGameState()
		gameStates["game over"] = gameOverGS
	*/

	// Connect gamestates
	//mainGS.child
}

func setGameState(name string) {
	p("setGameState name:", name)
	if gameState != nil {
		p("unsetting gamestate, name:", gameState.xname())
		unsetGameState()
	}
	if gs, ok := gameStates[name]; ok != false {
		p("starting gamestate, name:", name)
		gs.start()
		gameState = gs
	} else {
		pp("no such gamestate:", name)
	}

}

func unsetGameState() {
	gameState.stop()
	gameState = nil
}

func updateGameState(dt float64) {
	if gameState != nil {
		gameState.update(dt)
	}
}
