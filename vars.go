package main

import (
	"os"
	//. "rx/math"
)

const (
	Debug       = true
	AppName     = "mrs"
	gameWindowW = 50.0
	gameWindowH = 100.0

	world_size = 1024 / 4
	//cell_size = world_size / 128 // Warning, must be int?
	// Visual cell dimensions in units
	cell_size      = 28 //8 //6
	half_cell_size = cell_size / 2
	//water_zlevel   = -1.0 // Fixme: move to Water?
	//zelev_sea0    = 0.0
	//zelev_ground0 = 1.0 // Land0
)

var (
	BuildRevision string = "unknown"
)

var vars struct {
	scrollSpeed float64
	// Screen resolution
	resX int
	resY int
	// Native screen resolution for fixed-size elements
	nativeResX int
	nativeResY int
	// Resolution scale coeff. for fixed-size elements
	resScaleX float64
	resScaleY float64
	// Wrapped world space is used
	wrapped bool
	// For temporary files
	tmpDir    string
	gameSpeed float64
	// Time Delta
	dt float64
	// Game speed-independant Time Delta
	dtGameSpeed float64
	playLevel   string
	// Debug
	dShowPathGraph bool
}

func SetDefaultVars() {
	//vars.gameWindow = Vec4{0, 0, 10, 50} // x, y, w, h
	vars.scrollSpeed = 25.0
	//vars.scrollSpeedFast = 100.0
	vars.resX = 1024
	vars.resY = 576
	vars.nativeResX = 1024
	vars.nativeResY = 576
	vars.resScaleX = 1
	vars.resScaleY = 1
	vars.wrapped = false
	vars.gameSpeed = 1
	vars.dt = 0
	vars.dtGameSpeed = 0
	vars.playLevel = ""
	vars.dShowPathGraph = true
	//vars.appRoot = ""

	vars.tmpDir = os.TempDir()
}
