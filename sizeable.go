package main

var _ = `
type Sizeable struct {
	// Together with dim[X,Y] forms dimRect
	dimOffsetX int
	dimOffsetY int
	// Original (type/data) dimensions
	origDimX int
	origDimY int
	dimRect  Rect
	// Real (actual) dimensions
	dimX  int
	dimY  int
	areaX int
	areaY int
}

func newSizeable() Sizeable {
	s := Sizeable{
		dimX:  1,
		dimY:  1,
		areaX: 1,
		areaY: 1,
	}
	s.dimRect = Rect{0, 0, s.dimX, s.dimY}
	return s
}

func (s *Sizeable) origDim() (int, int) {
	return s.origDimX, s.origDimY
}

func (s *Sizeable) dim() (int, int) {
	return s.dimX, s.dimY
}

func (s *Sizeable) area() (int, int) {
	return s.areaX, s.areaY
}

func (s *Sizeable) centerOffsetPos() (int, int) {
	// By default its for dims
	return s.dimOffsetX + (s.dimX * cell_size / 2), s.dimOffsetY + (s.dimY * cell_size / 2)
}

func (s *Sizeable) centerOffsetPosX() int {
	x, _ := s.centerOffsetPos()
	return x
}

func (s *Sizeable) centerOffsetPosY() int {
	_, y := s.centerOffsetPos()
	return y
}
`
