package main

import "fmt"

var maxObjId = 0

type ObjI interface {
	getId() int
	getObjType() string
}

// component?
// game object
type Obj struct {
	*Transform
	//colobj  *ColObj
	id      int
	objtype string
	dead    bool
	view    ViewI
}

func newObj(objtype string) *Obj {
	o := &Obj{
		id:        maxObjId,
		Transform: newTransform(),
		objtype:   objtype,
		dead:      false,
	}
	maxObjId++
	game.objsys.addElem(o)
	return o
}

func (o *Obj) getId() int {
	return o.id
}

func (o *Obj) getObjType() string {
	return o.objtype
}

func (o *Obj) isDead() bool {
	return o.dead
}

func (o *Obj) start() {
}

func (o *Obj) destroy() {
	game.objsys.removeElem(o)
}

func (o *Obj) getView() ViewI {
	cknil(o.view)
	return o.view
}

func (o *Obj) String() string {
	return fmt.Sprintf("Obj<id: %d, objtype: %s, pos: %f %f %f>",
		o.id, o.objtype, o.Transform.Pos().X(), o.Transform.Pos().Y(), o.Transform.Pos().Z())
}

func (o *Obj) update(dt float64) {
	/*
		if o.colobj != nil {
			o.colobj.update(dt)
		}
	*/
}
