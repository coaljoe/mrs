package main

import (
	//ps "lib/pubsub"
	ps "kristallos.ga/lib/pubsub"
)

const (
	ev_entity_destroy ps.EventType = iota
	ev_unit_spawn
	ev_unit_destroy
	//ev_unit_move
	ev_unit_step
	ev_weaponhost_fire
	ev_projectile_hit
	ev_building_spawn
	ev_building_destroy
)

var pubS = ps.PublishS
var subS = ps.SubscribeS
var unsubS = ps.UnsubscribeS

//type EventS ps.EventS
type EventS = ps.EventS

func pub(eventType ps.EventType, data interface{}) {
	_log.Inf("Event: pub; type:", eventType, "data:", data)
	ps.Publish(eventType, data)
}

func sub(eventType ps.EventType, fn ps.Callback) {
	_log.Inf("Event: sub; type:", eventType, "fn:", fn)
	ps.Subscribe(eventType, fn)
}

func unsub(eventType ps.EventType, fn ps.Callback) {
	ps.Unsubscribe(eventType, fn)
}
