package main

import (
	//"bytes"
	//"fmt"
	"io/ioutil"

	"github.com/google/go-jsonnet"
	"github.com/hjson/hjson-go"
)

func unmarshalJsonnetFromFile(path string) map[string]interface{} {

	content, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	contentS := string(content)

	vm := jsonnet.MakeVM()
	vm.Importer(&jsonnet.FileImporter{
		//JPaths: []string{vars.appRoot},
		JPaths: []string{"."},
	})

	out, err := vm.EvaluateSnippet(path, contentS)
	if err != nil {
		panic(err)
	}
	outB := []byte(out)

	//pp(out)

	debugOutPath := vars.tmpDir + "/mrs_jsonnet_out.json"
	err = ioutil.WriteFile(debugOutPath, outB, 0644)
	if err != nil {
		panic(err)
	}
	_log.Dbg("jsonnet's debug out saved to:", debugOutPath)

	var dat map[string]interface{}

	// Decode and a check for errors.
	if err := hjson.Unmarshal(outB, &dat); err != nil {
		panic(err)
	}
	//fmt.Println(dat)

	return dat
}
