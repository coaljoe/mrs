package main

import (
	"kristallos.ga/rx"
	//. "bitbucket.org/coaljoe/rx/math"
	//. "rx/math"
)

type PathGraphView struct {
	*View
	links map[*PathLink]bool
	m     *PathGraph
}

func newPathGraphView(m *PathGraph) *PathGraphView {
	v := &PathGraphView{
		View:  newView(),
		links: make(map[*PathLink]bool),
		m:     m,
	}
	subS("ev_pathgraph.link_points", v.onPathgraphLinkPoints)
	return v
}

func (v *PathGraphView) onPathgraphLinkPoints(ev *EventS) {
	callerPG := ev.Data[0].(*PathGraph)
	p1 := ev.Data[1].(*PathPoint)
	p2 := ev.Data[2].(*PathPoint)
	_ = p1
	_ = p2

	if callerPG != v.m {
		println("XXX warn: skip event from different pathgraph")
		return
	} else {
		/*
			pos1 := p1.pos
			pos2 := p2.pos

			// XXX color is altered _after_ linking
			pl := v.m.getLinkByPos(pos1, pos2)
			pdump(pl)
			c := pl.color
		*/
		//v.links[[3]Vec3{pos1, pos2, c}] = true

		//pos1 := p1.pos
		//pos2 := p2.pos
		//pl := v.m.getLinkByPos(pos1, pos2)
		pl := ev.Data[3].(*PathLink)
		v.links[pl] = true
	}
}

func (v *PathGraphView) load() {
}

func (v *PathGraphView) spawn() {
}

func (v *PathGraphView) destroy() {
}

func (v *PathGraphView) update(dt float64) {
	var _ = `
	//pp(2)
	//for i, p := range len(v.m.points) - 1 {
	for i := 0; i < len(v.m.points)-1; i++ {
		rx.DrawSetColorV(v.m.points[i].color)
		p1 := v.m.points[i].pos
		p2 := v.m.points[i+1].pos
		p1[2] += 0.1
		p2[2] += 0.1
		rx.DrawLineV(p1, p2)
		//p("PathView.update: draw line:", p1, p2)
		rx.DrawResetColor()
	}
	`

	var _ = `
	// Prevents links from redrawing
	// XXX can be drawn with g.Edges() instead
	var haveLinks map[[2]Vec3]bool
	haveLinks = make(map[[2]Vec3]bool)
	//for i, p := range len(v.m.points) - 1 {
	//p(v.m.points)
	//xpp(v.m.points)
	//for i := 0; i < len(v.m.points); i++ {
	for _, point := range v.m.points {
		//p("-> i", i)
		//rx.DrawSetColorV(v.m.points[i].color)
		links := point.linkedPoints()
		p1 := point.pos
		for _, link := range links {
			p2 := link.pos

			pl := v.m.getLinkByPos(p1, p2)
			rx.DrawSetColorV(pl.color)

			//p1[2] += 0.01
			//p2[2] += 0.01
			if haveLinks[[2]Vec3{p1, p2}] == true {
				continue
			}
			haveLinks[[2]Vec3{p1, p2}] = true
			// Post draw
			//p1[2] = 0.01
			//p2[2] = 0.01
			//rx.DrawLineV(p1, p2)
			drawP1 := p1
			drawP2 := p2
			drawP1[2] = 0.01
			drawP2[2] = 0.01
			rx.DrawLineV(drawP1, drawP2)
		}
		//p("PathView.update: draw line:", p1, p2)
		rx.DrawResetColor()
	}
	//pdump(haveLinks)

	`

	if vars.dShowPathGraph {
		for r, _ := range v.links {

			d := rx.NewDrawer()
		
			/*
				p1 := r[0]
				p2 := r[1]
				c := r[2]
				rx.DrawSetColorV(c)
			*/
			pl := r
			p1 := pl.p1.pos
			p2 := pl.p2.pos
			//rx.DrawSetColorV(pl.color)

			drawP1 := p1
			drawP2 := p2
			drawP1[2] = 0.01
			drawP2[2] = 0.01
			//rx.DrawLineV(drawP1, drawP2)
			//rx.DrawResetColor()
			d.DrawLineV(drawP1, drawP2, pl.color)
		}
	}

}
