package main

import (
	"kristallos.ga/rx"
	//. "bitbucket.org/coaljoe/rx/math"
)

type BuildingView struct {
	*View
	m      *Building
	spr    *Sprite
	rotDeg float64
}

func newBuildingView(m *Building) *BuildingView {
	v := &BuildingView{
		View: newView(),
		m:    m,
		spr:  newSprite(),
	}
	return v
}

func (v *BuildingView) load() {
	path := "res/buildings/" + v.m.name + "/" + "image.png"
	if v.m.rotRes == 0 {
		v.spr.load(path)
	} else {
		numTiles := 360 / v.m.rotRes
		v.spr.loadTiles(path, numTiles)
		//pp(numTiles)
	}
	v.loaded = true
}

func (v *BuildingView) spawn() {
	if !v.loaded {
		v.load()
	}
	v.spr.spawn()

	if true {
		ah := rx.NewAxesHelper()
		ah.SetPos(v.m.Pos())
		//ah.DrawOriginLink = true
		ah.NoDepthTest = true
		ah.Size = 20
		ah.Spawn()
	}
}

func (v *BuildingView) destroy() {

}

func (v *BuildingView) update(dt float64) {
	newPos := v.m.Pos()
	v.spr.SetPos(newPos)

	var _ = `
	yCorr := v.spr.tileSize / 2
	yCorr = 85
	//yCorr = 0
	//yCorr = 1000
	yCorr = 106 - 85
	//newZ := newPos.Z() + float64(-yCorr)
	newZ := newPos.Z() + float64(-yCorr)
	_ = newZ
	//newPos.SetZ(newZ)

	//newPos = newPos.MoveTowards(game.scene.camera.cam.Pos(), 100)
	// XXX arbitrary: FIXME
	//newPos = newPos.MoveTowards(game.scene.camera.cam.Pos(), 1.6*float64(yCorr))
	//newPos = newPos.MoveTowards(game.scene.camera.cam.Pos(), 2.0*float64(yCorr))

	v.spr.SetPos(newPos)
	//v.spr.MoveByVec(Vec3{0, 0, float64(-yCorr)})
	
	yCorrUnits := float64(yCorr) * _rxi.Renderer().GetPh()
	_ = yCorrUnits
	//pp(yCorrUnits)
	//v.spr.MoveByVec(Vec3{0, 0, yCorrUnits})
	//v.spr.MoveByVec(Vec3{0, 0, -20.0})
	dimX := v.spr.dim.X() // Scale
	dimY := v.spr.dim.Y()
	_ = dimX
	_ = dimY
	p(dimX, dimY)
	//pp(2)
	shY := 1.0 * dimX
	_ = shY
	
	p(v.spr.spawned)
	p(v.spr.Rot())
	p("X:", v.spr.Pos())
	v.spr.MoveByVec(Vec3{0, 0, shY})
	p("Y:", v.spr.Pos())
	//pp(3)
	`

	v.spr.update(dt)
	// Update uniforms

	if v.rotDeg != v.m.rotDeg {
		rot := int(v.m.rotDeg)
		tileIdx := rot / v.m.rotRes
		tileX := tileIdx % v.spr.numTilesX
		tileY := tileIdx / v.spr.numTilesY
		p("rot:", rot, "tileIdx:", tileIdx, "tileX:", tileX, "tileY:", tileY)
		//pp(2)
		v.spr.mesh.Mesh.Material().AddUniform("shift_texCoord", "2f", false, float64(tileX), float64(tileY))
		v.rotDeg = v.m.rotDeg
	}
}

// Mesh View (3d)
//

type BuildingViewMesh struct {
	*View
	m    *Building
	node *rx.Node
}

func newBuildingViewMesh(m *Building) *BuildingViewMesh {
	v := &BuildingViewMesh{
		View: newView(),
		m:    m,
	}
	return v
}

func (v *BuildingViewMesh) load() {
	//path := "res/models/traincar/traincar.dae"
	path := "res/buildings/" + v.m.name + "/" + v.m.name + ".dae"
	v.model.Load(path)
	v.node = v.model.Nodes()[0]
	//v.node.SetScale(Vec3{4, 4, 4})
	if v.node == nil {
		panic("no v.node")
	}
	v.loaded = true
}

func (v *BuildingViewMesh) spawn() {
	if !v.loaded {
		v.load()
	}
	rx.Rxi().Scene.Add(v.node)
}

func (v *BuildingViewMesh) destroy() {
	rx.Rxi().Scene.RemoveMeshNode(v.node)
}

func (v *BuildingViewMesh) update(dt float64) {
	v.node.SetPos(v.m.Pos())
	v.node.SetRot(v.m.Rot())
}
