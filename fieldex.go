// Field extras.
package main

import (
	"fmt"
)

// Cell Z level.
type ZLevel int

const (
	/*
		ZLevel_Sea0    ZLevel = 0  // l0
		ZLevel_Sea1    ZLevel = -1 // l1
		ZLevel_Sea2    ZLevel = -2 // l2
		ZLevel_Sea3    ZLevel = -3 // l3
		ZLevel_Ground0 ZLevel = 1 // l4
		ZLevel_Ground1 ZLevel = 2 // l5
		ZLevel_Ground2 ZLevel = 3 // l6
		ZLevel_Ground3 ZLevel = 4 // l7
	*/

	ZLevel_Sea0    ZLevel = -1 // l0 // Level of terrain/depth, not level of sea surface
	ZLevel_Sea1    ZLevel = -2 // l1
	ZLevel_Sea2    ZLevel = -3 // l2
	ZLevel_Sea3    ZLevel = -4 // l3
	ZLevel_Ground0 ZLevel = 0  // l4
	ZLevel_Ground1 ZLevel = 1  // l5
	ZLevel_Ground2 ZLevel = 2  // l6
	ZLevel_Ground3 ZLevel = 3  // l7

	ZLevel_SeaMax    = ZLevel_Sea3    // Deeper
	ZLevel_GroundMax = ZLevel_Ground3 // Higher
	ZLevel_Min       = ZLevel_SeaMax
	ZLevel_Max       = ZLevel_GroundMax
)

func (zl ZLevel) isSea() bool {
	return zl < 0
}

func (zl ZLevel) isGround() bool {
	return zl >= 0
}

func (zl ZLevel) lower(oth ZLevel) bool {
	return zl < oth
}

func (zl ZLevel) higher(oth ZLevel) bool {
	return zl > oth
}

//func (zl ZLevel) linearize() ZLevel {}

func (zl ZLevel) String() string {
	switch zl {
	case ZLevel_Sea0:
		return "Sea0 (-1)"
	case ZLevel_Sea1:
		return "Sea1 (-2)"
	case ZLevel_Sea2:
		return "Sea2 (-3)"
	case ZLevel_Sea3:
		return "Sea3 (-4)"
	case ZLevel_Ground0:
		return "Ground0 (0)"
	case ZLevel_Ground1:
		return "Ground1 (1)"
	case ZLevel_Ground2:
		return "Ground2 (2)"
	case ZLevel_Ground3:
		return "Ground3 (3)"
	default:
		panic(fmt.Sprintf("error: unknown ZLevel zl=%d", zl))
	}
}

// River State
type RiverState int

const (
	RiverState_None  RiverState = 0
	RiverState_River RiverState = 1
	RiverState_Ford  RiverState = 2
)
