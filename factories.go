package main

import (
	"fmt"
)

func makeBuilding(name string) *Building {
	b := newBuilding(name, ViewType_Sprite)
	b.name = name
	readBuildingDef(name, b)

	// Attach view
	b.view = makeBuildingView(b)

	return b
}

//func readBuildingDef(name string) *Building {
//	b := &Building{}

// Read building's definition without creating it
func readBuildingDef(name string, to *Building) {
	b := to

	buildingsdir := "res/buildings"
	path := buildingsdir + "/" + name + "/" + "/def.jsonnet"

	found, err := exists(path)
	if err != nil {
		panic(err)
	}

	// Finally
	if !found {
		panic(fmt.Sprintf(
			"cannot make building from data, unknown settings: name=%s \npath=%s",
			name, path))
	} else {
		_log.Inf("Loading building from:", path)
	}

	dat := unmarshalJsonnetFromFile(path)
	_ = dat

	// Building
	{
		def := hjGetDef(dat, "building")

		b.dimX = hjGetInt(def, "dimX")
		b.dimY = hjGetInt(def, "dimY")
		//b.areaX = hjGetInt(def, "areaX")
		//b.areaY = hjGetInt(def, "areaY")

		b.guiName = hjGetString(def, "guiName")

		b.rotRes = hjGetInt(def, "rotRes")
		//pp(u.guiName)
	}

	// Combat
	{
	}
}

func makeBuildingView(b *Building) *BuildingView {
	v := newBuildingView(b)
	name := b.name
	_ = name

	return v
}
