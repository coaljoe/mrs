package main

// Object view type
type ViewType int

const (
	ViewType_None ViewType = iota
	ViewType_Sprite
	ViewType_Mesh
	//ViewType_ImageSprite
	//ViewType_SpriteMesh
)

// Builging area/ground
type BuildingArea struct {
	areaX  int
	areaY  int
	noArea bool
}

// entity
type Building struct {
	*Obj
	name    string
	rotRes  int
	dimX    int
	dimY    int
	guiName string
	// XXX buildings face south by default?
	rotDeg float64 // XXX use Dir?
	//rotDeg int

	// Builging area/ground
	areaX  int
	areaY  int
	noArea bool

	viewType ViewType
}

func newBuilding(name string, vt ViewType) *Building {
	b := &Building{
		Obj: newObj("Building"),
		//typename: "building1",
		name: name,
		//ViewType: ViewType_Sprite,
		viewType: vt,
	}
	if b.viewType == ViewType_Sprite {
		b.view = newBuildingView(b)
	} else if b.viewType == ViewType_Mesh {
		b.view = newBuildingViewMesh(b)
	} else {
		pp("error")
	}
	return b
}
