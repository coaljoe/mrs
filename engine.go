package main

type Engine struct {
	// props:
	// maximum power, kw/h
	info_power int
	// state:
	enabled bool
	// current power
	power int
}

func newEngine() *Engine {
	e := &Engine{
		info_power: 100,
	}
	return e
}

func (e *Engine) enable() {
	e.enabled = true
}

func (e *Engine) disable() {
	e.enabled = false
}

func (e *Engine) update(dt float64) {
	if !e.enabled {
		return
	}

	e.power = e.info_power * 1
}
