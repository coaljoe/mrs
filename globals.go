package main

import (
	//"lib/ider"
	"kristallos.ga/lib/ider"
	"kristallos.ga/rx"
)

type ObjType int

const (
	ObjType_Unknown = iota
	ObjType_Unit
	ObjType_Building
)

// Rxi instance
var _rxi *rx.Rx

// Ider
var _ider = ider.NewIder()

// Shortcuts
//var ObjT = &Obj{}
//var UnitT = &Unit{}
//var BuildingT = &Building{}
//var PlayerRecordT = &PlayerRecord{}
//var HealthT = &Health{}
