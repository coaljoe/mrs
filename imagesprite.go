package main

import (
	"kristallos.ga/rx"
)

type ImageList struct {
	//frame       int
	//maxFrame    int
	//frameTime   int // ms
	//frameInc    int
	//loop        bool
	//playing     bool
	//removeAfter bool
	//needRemove       bool
	numTiles    int
	tileSize    int
	tileWidth   int
	tileHeight  int
	imageWidth  int
	imageHeight int
	//startTime        float64
	//lastTime         float64
	posX, posY       int
	originX, originY int
	//posType          SpritePositionType
	//tex              *sdl.Texture
	tex *rx.Texture
}

func newImageList() *ImageList {
	il := &ImageList{
		//frameInc:    1,
		//loop:        true,
		//playing:     true,
		//posType:     SpritePositionType_WorldSpace,
		//removeAfter: false,
	}
	//game.spritesys.addElem(as)
	return il
}

func (il *ImageList) load(path string, numTiles int) {

	il.tex = rx.NewTexture(path)

	il.imageWidth = il.tex.Width()
	il.imageHeight = il.tex.Height()

	tileSize := il.imageWidth / numTiles
	il.tileSize = tileSize
	il.tileWidth = il.tileSize
	il.tileHeight = il.imageHeight
	//as.maxFrame = (as.imageWidth / as.tileSize) - 1
	//il.maxFrame = (il.imageWidth / il.tileSize)
}

var _ = `
func (as *AnimatedSprite) animate() {
	// Init playing
	if as.startTime == 0 {
		as.startTime = game.timer.time
		as.lastTime = as.startTime
		return
	}

	frameTimeSec := float64(as.frameTime) / 1000.0
	if as.lastTime+frameTimeSec > game.timer.time {
		return
	}

	/*
		if as.lastTime+frameTimeSec > game.timer.time {
			return
		}
	*/

	as.lastTime = game.timer.time

	as.frame += 1

	if as.frame >= as.maxFrame {
		if !as.loop {
			as.frame = as.maxFrame
		} else {
			as.frame = 0
		}
	}

	if !as.loop && as.removeAfter && as.frame == as.maxFrame {
		//as.needRemove = true
		//pp(as.frame, as.maxFrame)
		as.destroy()
	}

}
`

func (il *ImageList) destroy() {
	//game.spritesys.removeElem(as)
	pp("not implemented")
}

var _ = `
func (il *ImageList) draw() {
	if as.playing {
		as.animate()
	}
	//p("YYY frame:", as.frame)

	var dst sdl.Rect
	src := sdl.Rect{int32(as.frame * as.tileSize), 0, int32(as.tileSize), int32(as.imageHeight)}
	if as.posType == SpritePositionType_WorldSpace {
		sx, sy := worldToScreenPos(as.posX, as.posY)
		dst = sdl.Rect{int32(sx - as.originX), int32(sy - as.originY),
			int32(as.tileSize), int32(as.imageHeight)}
	} else if as.posType == SpritePositionType_ScreenSpace {
		dst = sdl.Rect{int32(as.posX - as.originX), int32(as.posY - as.originY),
			int32(as.tileSize), int32(as.imageHeight)}
	} else {
		pp("error: unknown posType:", as.posType)
	}
	renderer.Copy(as.tex, &src, &dst)
}
`
