package main

var _ = `

import (
	//. "math"
	"bitbucket.org/coaljoe/rx"
	//. "bitbucket.org/coaljoe/rx/math"
	rg "bitbucket.org/coaljoe/rx/rxgui"
)

type GuiMenuSheet struct {
	*rg.Sheet
	topText    *rg.Label
	debugText  *rg.Label
	statusText *rg.Label
}

func newGuiMenuSheet(g *Gui) *GuiMenuSheet {
	s := &GuiMenuSheet{
		Sheet: rg.NewSheet("Menu sheet"),
	}
	//g := _Gui
	g.rgi.SheetSys.AddSheet(s)
	//g.rgi.SetEnabled(g.enabled)

	// Panel test
	//p := rg.NewPanel(rg.Rect{0, 0, 100, 100})
	//p := rg.NewPanel(rg.Rect{0, 0, 10, 10})
	//p := rg.NewPanel(rg.Rect{0, 0, .2, .2})
	p := rg.NewPanel(rg.Rect{.5, .5, .2, .2})
	p.SetName("blue panel")
	//p.SetColor(&rg.ColorBlue)
	p.Label().SetText("NEW GAME")
	//s.AddWidget(p)
	s.Root().AddChild(p)
	//g.mainSheet.Root().AddChild(p)

	pCb := func() {
		setGameState("game")
	}
	p.SetCallback(pCb)

	return s
}

func (s *GuiMenuSheet) Render(r *rx.Renderer) {
	// Call base
	s.Sheet.Render(r)

	//println("MYSHEET RENDER")
	//r := rxi.Renderer()

	/*
		r.RenderQuad(0.0, 0.97, 0.2, 0.1, 10000)
		r.Set2DMode()
		rx.DrawLine(0, 0, 0, .5, .5, 0)
		//rx.DrawLine(0, 0, 0, 1, 1, 0)
		//rx.DrawLine(0, 0, 0, 100, 100, 0)
		rx.DrawQuad(.3,0,0.1, 0.1)
		r.Unset2DMode()
	*/

	//pp(2)

	/*
		r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
		r.Set2DMode()
		rx.DrawLine(0, 0, 0, .5, .5, 0)
		//rx.DrawLine(0, 0, 0, 1, 1, 0)
		//rx.DrawLine(0, 0, 0, 100, 100, 0)
		r.Unset2DMode()
	*/
}

func (s *GuiMenuSheet) Update(dt float64) {
	// Call base
	s.Sheet.Update(dt)

	//pp(1)
}
`