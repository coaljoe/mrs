package main

import (
	. "kristallos.ga/rx/math"
)

type TrainCar struct {
	*Obj
	id         int
	vehicle    *Vehicle
	position   *Position
	locomotive bool
	headCar    bool
	// Pathpoint on which the car is
	pp *PathPoint
	// Target pathpoint
	nextPP *PathPoint
	// From head to tail
	parent *TrainCar
	child  *TrainCar
}

func newTrainCar() *TrainCar {
	tc := &TrainCar{
		Obj:        newObj("train car"),
		id:         _ider.GetNextId("TrainCar"),
		vehicle:    newVehicle(),
		locomotive: false,
	}
	tc.position = newPosition(tc.Obj)
	tc.view = newTrainCarView(tc)
	return tc
}

// Link lenght? or car length?
// XXX add padding?
func (tc *TrainCar) length() float64 {
	//return 1.5
	//return 24
	return 24 * 4
}

func (tc *TrainCar) frontPoint() Vec3 {
	// XXX fixme? use translate?
	return tc.Forward().MulScalar(tc.length() / 2)
}

func (tc *TrainCar) rearPoint() Vec3 {
	// XXX fixme? use translate?
	//return tc.Forward().MulScalar(-1).MulScalar(tc.length() / 2)
	return tc.Back().MulScalar(tc.length() / 2)
}

// Get current pathlink.
func (tc *TrainCar) pl() *PathLink {
	//p("->", tc.pp, tc.nextPP)
	if tc.pp != nil && tc.nextPP != nil {
		linki := game.pgRailway.g.Edge(tc.pp.ID(), tc.nextPP.ID())
		if linki != nil {
			return linki.(*PathLink)
		}
	}
	return nil
}

func (tc *TrainCar) update(dt float64) {
	// Update link speed
	if tc.pl() != nil {
		maxLinkSpeed := tc.pl().railway.maxSpeed
		//pp(maxLinkSpeed)

		//const msK = 5.0 / 18.0
		//const msK = 3.6
		//maxLinkSpeedMS := float64(maxLinkSpeed) * msK
		//maxLinkSpeedMS := float64(maxLinkSpeed) / msK
		//maxLinkSpeedMS := kmhToMs(maxLinkSpeed)
		maxLinkSpeedMS := kmhToMs(maxLinkSpeed) * 4 // x4 train scale
		tc.vehicle.targetSpeed = maxLinkSpeedMS
		//pp(maxLinkSpeed, maxLinkSpeedMS)
		//tc.vehicle.targetSpeed = float64(maxLinkSpeed) * 0.1
	}

	// Update components
	tc.vehicle.update(dt)
	tc.position.speed = tc.vehicle.speed
	tc.position.update(dt)
	p("XXX", tc.position.speed, tc.vehicle.speed)
}

type Train struct {
	cars []*TrainCar
	// Current pathpoint
	//pp *PathPoint
	// Next pathpoint
	nextPP *PathPoint
	// Previous (last) pathpoint
	prevPP *PathPoint
	// Stopped state in gui
	stopped bool
}

func newTrain() *Train {
	t := &Train{
		cars: make([]*TrainCar, 0),
	}
	return t
}

func (t *Train) head() *TrainCar {
	return t.cars[0]
}

func (t *Train) pos() Vec3 {
	return t.cars[0].Pos()
}

func (t *Train) pp() *PathPoint {
	return t.cars[0].pp
}

func (t *Train) setPP(pp *PathPoint) {
	t.cars[0].pp = pp
}

func (t *Train) length() float64 {
	r := 0.0
	for _, car := range t.cars {
		r += car.length()
	}
	return r
}

func (t *Train) addCar(c *TrainCar) {
	t.cars = append(t.cars, c)
	if len(t.cars) == 1 {
		t.cars[0].headCar = true
	} else {
		c.parent = t.cars[len(t.cars)-2]
		c.parent.child = c
		//p(c, c.parent.child) // Must be same
		//p(c, c.parent, c.child)
	}
	var _ = `
	// Align position
	newPos := t.pos()
	//newPos.SetX(newPos.X() - car.length())
	c.SetPos(newPos)
	c.MoveByVec(Vec3{-c.length() * float64(len(t.cars)), 0, 0})
	c.position.sPos = c.Pos()
	c.position.dir.setAngle(c.RotZ())
	`
}

func (t *Train) getPos() Vec3 {
	return t.head().Pos()
}

func (t *Train) getSpeed() float64 {
	if len(t.cars) < 1 {
		return 0
	} else {
		return t.cars[0].vehicle.speed
	}
}

func (t *Train) setSpeed(s float64) {
	t.head().vehicle.speed = s
	/*
		for _, car := range t.cars {
			car.vehicle.speed = s
		}
	*/
}

func (t *Train) setPos(p Vec3) {
	t.cars[0].SetPos(p)
	for _, car := range t.cars {
		// Skip head car
		if car.headCar {
			continue
		}
		newPos := t.pos()
		//newPos.SetX(newPos.X() - car.length())
		car.SetPos(newPos)
		car.MoveByVec(Vec3{-car.length(), 0, 0})
	}
}

func (t *Train) setRot(r Vec3) {
	t.head().SetRot(r)
	/*
		for _, car := range t.cars {
			// Skip head car
			if car.headCar {
				continue
			}
			car.SetRot(r)
		}
	*/
}

func (t *Train) setDir(a float64) {
	t.head().position.dir.setAngle(a)
	/*
		for _, car := range t.cars {
			// Skip head car
			if car.headCar {
				continue
			}
			car.position.dir.setAngle(a)
		}
	*/
}

func (t *Train) attachToPathPoint(pp *PathPoint) {
	//t.pp = pp
	t.setPP(pp)

	// Fixme: maybe cars should be on different pathpoints?
	for _, car := range t.cars {
		if car.headCar {
			continue
		}

		car.pp = pp
	}
	/*
		//t.cars[0].SetPos(Vec3{10, 0, 0})
		t.cars[0].SetPos(Vec3{0, 0, 0})
		__p("rot before -> ", t.cars[0].Rot())
		t.cars[0].LookAt2d(Vec3{10, 10, 0})
		//t.cars[0].LookAt(Vec3{10, 10, 0})
		__p("rot after -> ", t.cars[0].Rot())
	*/
	dir := pp.angles()[0]
	t.setRot(Vec3{0, 0, dir})
	t.setPos(pp.pos)
	//pp(t.path.points[0].pos)
	t.setDir(dir)
	//t.moveForward(t.length())
}

/*
func (t *Train) followPath(p *Path) {

}
*/

func (t *Train) moveForward(v float64) {
	if t.pp() == nil {
		pp("no train pathpoint")
	}

	p("v ->", v)

	for _, c := range t.cars {
		//c.AdjPos(Vec3{v, v, 0})
		p("rot -> ", c.Rot())
		c.MoveByVec(Vec3{v, 0, 0})
	}
}

func (t *Train) moveToPathPoint(pp *PathPoint) {
	_log.Dbg("[Train] moveToPathPoint:", pp)
	t.setDPos(pp.pos)
	t.nextPP = pp

	// Set head's nextPP
	t.head().nextPP = pp

	// Set angle
	ppDeg := degBetween(t.pp().pos.ToVec2(), t.nextPP.pos.ToVec2())
	t.head().position.dir.setAngle(ppDeg)
}

func (t *Train) moveToNextPathPoint() {
	linkedPoints := t.pp().linkedPoints()
	nextPP := linkedPoints[0]
	p("nextPP:", nextPP)
	p("linkedPoints:", linkedPoints)
	t.moveToPathPoint(nextPP)
}

func (t *Train) setDPos(p Vec3) {
	t.head().position.setDPos(p)
	/*
		for _, car := range t.cars {
			car.position.setDPos(p)
		}
	*/
}

func (t *Train) hasDPos() bool {
	return t.head().position.hasDPos()
}

func (t *Train) update(dt float64) {
	//_log.Dbg("%F", dt)
	speed := t.getSpeed()
	_ = speed
	//pp(speed)
	//t.moveForward(speed * dt)
	var _ = `
	pp := t.head().nextPathPoint
	if pp != nil {
		t.head().position.setDPos(pp.pos)
			linkedPoints := t.pp().linkedPoints()
			// Set next pathpoint
			if pp.next != nil {
				t.head().nextPathPoint = pp.next
			} else {
				p("[Train] no next pathpoint")
				//__pp(2)
				t.prevPP = t.pp()
				t.setPP(t.nextPP)
	}
	`

	// Update moving
	if speed > 0 {
		if !t.hasDPos() {
			/*
				linkedPoints := t.pp().linkedPoints()
				nextPP := linkedPoints[0]
				p("nextPP:", nextPP)
				p("linkedPoints:", linkedPoints)
				t.moveToPathPoint(nextPP)
			*/
			t.moveToNextPathPoint()
			//pp(2)
		} else {
			if t.getPos() == t.nextPP.pos {
				// Update current pathpoint
				t.prevPP = t.pp()
				t.setPP(t.nextPP)
				//pp(2)
				linkedPoints := t.pp().linkedPoints()
				var nextPP *PathPoint
				for _, lpp := range linkedPoints {
					if lpp == t.prevPP {
						p("skip prevPP")
						continue
					}
					nextPP = lpp
					break
				}
				//nextPP := linkedPoints[0]
				p("nextPP:", nextPP)
				p("linkedPoints:", linkedPoints)
				if nextPP != nil {
					t.moveToPathPoint(nextPP)
				} else {
					p("no nextPP")
					t.setSpeed(0)
				}
			}
			//pp(2)
		}
		// Todo: Calculate overshot
	}

	// Update cars positions
	if true {
		for i, car := range t.cars {
			if car.headCar {
				continue
			}

			//carPos := game.pathgraph.traceLine(t.pp(), car.pp, t.head().Pos(), t.head().position.dir.angle(), -car.length())
			//p("carPos:", carPos)
			carPos := car.Pos()

			// traceLine back
			headPP := t.pp()
			carPP := car.pp
			carNextPP := car.nextPP
			//linelength := car.length() * float64(i)
			linelength := car.length()
			_ = linelength
			_ = i
			// Same PP
			//if headPP == carPP {
			if carNextPP == carPP {
				//carPos = t.head().Back().MulScalar(length)
				//carPos = t.head().Back()
				//carPos = t.head().Forward()
				//adj := t.head().Back().MulScalar(linelength)
				//carPos = t.head().Pos().Add(adj)
				adj := car.parent.Back().MulScalar(linelength)
				carPos = car.parent.Pos().Add(adj)
				p("back:", t.head().Back())
				p("forward:", t.head().Forward())
				p("adj:", adj)
			} else {
				// Different PP / corner
				adj := t.head().Back().MulScalar(linelength)
				//carPos = t.head().Pos().Add(adj)
				p("adj", adj, "carPos", carPos, "headPP", headPP, "head.pos", t.head().Pos())
				// Traceback line
				//headTraveledDistanceFromPP := t.head().Pos().Sub(headPP.pos)
				//headTraveledDistanceFromPP := distancePos2(t.head().Pos().ToVec2(), headPP.pos.ToVec2())
				//p("headTraveledDistanceFromPP:", headTraveledDistanceFromPP)
				//la := headTraveledDistanceFromPP
				//nextCarTraveledDistanceFromPP := distancePos2(car.parent.Pos().ToVec2(), headPP.pos.ToVec2())
				nextCarTraveledDistanceFromPP := distancePos2(car.parent.Pos().ToVec2(), car.parent.pp.pos.ToVec2())
				p("nextCarTraveledDistanceFromPP:", nextCarTraveledDistanceFromPP)
				// Line part a
				la := nextCarTraveledDistanceFromPP
				// Line part b
				//lb := car.Pos().Sub(headPP.pos) - la
				//lb := distancePos2(car.Pos().ToVec2(), headPP.pos.ToVec2()) - la
				lb := linelength - la
				p("la:", la, "lb:", lb)
				//pp(2)
				// Transform point
				//tp := t.head().Transform.Copy()
				tp := car.parent.Transform.Copy()
				p(tp.Rot())
				tp.SetRotZ(tp.RotZ() + 180.0)
				tp.MoveByVec(Vec3{la, 0, 0})
				p(tp.Rot())
				tp.SetRotZ(car.RotZ() + 180.0)
				tp.MoveByVec(Vec3{lb, 0, 0})
				/*
					if lb < 0 {
						pp(3)
					}
				*/

				if la > car.length() {
					//pp(2)
					car.pp = car.parent.pp
					car.nextPP = car.parent.nextPP
					car.SetRotZ(car.parent.RotZ())
					car.position.dir.setAngle(car.parent.position.dir.angle())
				}

				carPos = tp.Pos()
				//pp(2)
			}
			p("head pos:", t.head().Pos(), "rot:", t.head().Rot())
			p("carPos:", carPos)
			car.SetPos(carPos)
			//pp(2)
		}
	}

	// XXX cars already updated in updateEntity. FIXME?
	// XXX actually before the train.update, so its wrong?
	/*
		for _, car := range t.cars {
			car.update(dt)
		}
	*/
}
