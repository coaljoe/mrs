package main

import (
//"bitbucket.org/coaljoe/rx"
//. "bitbucket.org/coaljoe/rx/math"
)

type Tree struct {
	spr *Sprite
}

func newTree() *Tree {
	t := &Tree{
		spr: newSprite(),
	}
	return t
}

func (t *Tree) spawn() {
	t.spr.load("res/objects/trees/tree2/tree2.png")
	t.spr.spawn()
	t.spr.mesh.Mesh.Material().AlphaTest = false
	t.spr.mesh.Mesh.Material().Alpha = true
	//spr.dim = Vec2{300, 300}
	t.spr.update(1.0)
}
