package main

type VehicleI interface{}

type Vehicle struct {
	// props:
	/*
		info struct {
			maxSpeed float64
		}
	*/
	maxSpeed float64 // m/s (?)
	// state:
	speed float64 // m/s
	// Target speed
	targetSpeed float64 // m/s
	// Speed gear
	gear   float64
	weight int
	engine *Engine
	parent *VehicleI
	child  *VehicleI
}

func newVehicle() *Vehicle {
	v := &Vehicle{
		maxSpeed: 10,
		weight:   1000,
	}
	//v.info.maxSpeed = 100
	return v
}

func (v *Vehicle) hasEngine() bool {
	return v.engine != nil
}

func (v *Vehicle) hasChild() bool {
	return v.child != nil
}

func (v *Vehicle) hasParent() bool {
	return v.parent != nil
}

func (v *Vehicle) update(dt float64) {
	if v.targetSpeed > 0 {
		v.speed = v.targetSpeed
	}
}
