package main

type WayTypeId int

const (
	WayTypeId_Unknown WayTypeId = iota
	WayTypeId_Railway
	WayTypeId_Road
	WayTypeId_Monorail
	WayTypeId_Tram
)

func (wti WayTypeId) String() string {
	switch wti {
	case WayTypeId_Unknown:
		return "Unknown"
	case WayTypeId_Railway:
		return "Railway"
	case WayTypeId_Road:
		return "Road"
	case WayTypeId_Monorail:
		return "Monorail"
	case WayTypeId_Tram:
		return "Tram"
	default:
		pp("unknown WayTypeId:", wti)
		return ""
	}
}

type WayType struct {
	id              int
	name            string
	description     string
	maxSpeed        int
	maxSpeedFreight int  // XXX use global freight speed limit?
	electrified     bool // XXX contact_line / rail?
	//abandoned   bool
	availableFrom   int
	availableTo     int
	cost            int // XXX per tile?
	maintenanceCost int
}

func newWayType() WayType {
	wt := WayType{}
	return wt
}
