#!/bin/bash

src=$1
dst=`realpath $2`
tmpf=/tmp/t.png

yOffset=""

rm -f $tmpf
mkdir $dst
cd $src

for f in 0*.png; do

	# Calculate top extent from center
	convert -crop 100%x50%+0+0 $f $tmpf
	convert -trim $tmpf $tmpf
	top_ext=`identify -format "%h" $tmpf`
	echo "top_ext: $top_ext"
	#exit

	# Calculate bottom extent from center
	magick $f -crop "100%x50%+0+%[fx:h*0.5]" $tmpf
	convert -trim $tmpf $tmpf
	bottom_ext=`identify -format "%h" $tmpf`
	echo "bottom_ext: $bottom_ext"

	max_ext=""
	if (( $top_ext > $bottom_ext )); then
		max_ext=$top_ext
	else
		max_ext=$bottom_ext
	fi
	echo "max_ext: $max_ext"	

	# Center image
	#magick $f -crop "%[fx:$max_ext*2]x%[fx:$max_ext*2]+%[fx:w/2 - $max_ext]+%[fx:h/2 - $max_ext]" $tmpf
	#cp $tmpf $dst/$f
	#continue
	#exit

	convert -trim $f $dst/$f
	img_w=`identify -format "%w" $dst/$f`
	img_h=`identify -format "%h" $dst/$f`
	echo "img_w: $img_w"
	echo "img_h: $img_h"

	#if (( $img_h > $img_w )); then
	#	
	#fi

	if [[ "$yOffset" -eq "" ]]; then
		yOffset=$top_ext
		#echo $yOffset
		#exit
	fi
	
done

cd $dst
montage -mode concatenate 0*.png -background none out_concat.png
#montage -geometry +0+0 0*.png -background none out.png

max_w=`identify -format "%w %h %fn\n" 0*.png | sort -n -r -k 1 |  cut -d ' ' -f 1 | head -n 1`
max_h=`identify -format "%w %h %fn\n" 0*.png | sort -n -r -k 2 |  cut -d ' ' -f 2 | head -n 1`

echo "max_w: $max_w"
echo "max_h: $max_h"

max_val=""

if (( $max_w > $max_h )); then
	max_val=$max_w
else
	max_val=$max_h
fi

echo "max_val: $max_val"

#montage -geometry ${max_w}x${max_h}+0+0 0*.png -background none out.png
montage -geometry ${max_val}x${max_val}\>+0+0 0*.png -background none -depth 8 out.png
montage -geometry ${max_val}x${max_val}\>+0+0 0*.png -background none -depth 16 out_16.png

# Optimize 16 bit (depth)
convert -background white -alpha Remove -depth 11 out_16.png out_16_opt.png
#optipng -o7 out_16_opt.png
zopflipng -m out_16_opt.png -y out_16_opt.png

echo ".info:"
echo ""
echo "originOffsetY=$yOffset"
echo ""
echo "max_w=$max_w"
echo "max_h=$max_h"
echo ""

echo "done"
