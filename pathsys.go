package main

type PathSys struct {
	*GameSystem
}

func newPathSys() *PathSys {
	s := &PathSys{}
	s.GameSystem = newGameSystem("PathSys", "Path system", s)
	return s
}

var _ = `
func (s *PathSys) getAllPathPoints() []*PathPoint {
	r := make([]*PathPoint, 0)
	for _, pathI := range s.getElems() {
		path := pathI.(*Path)
		r = append(r, path.points...)
	}
	return r
}


func (s *PathSys) autoconnectPaths() {
	allPathPoints := s.getAllPathPoints()
	for _, pp := range allPathPoints {
		p1 := pp.pos
		for _, otherPP := range allPathPoints {
			if otherPP == pp {
				continue
			}
			p2 := otherPP.pos

			// Same position, make links
			if p1 == p2 && pp.linkedPath == nil && otherPP.linkedPath == nil {
				p("[PathSys] added links between:", p1, p2, pp, otherPP)
				pp.linkedPath = otherPP.path
				otherPP.linkedPath = pp.path
			}
		}
	}
}
`

func (s *PathSys) update(dt float64) {
}
