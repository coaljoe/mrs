package main

import (
	//"fmt"
	"github.com/go-gl/glfw/v3.3/glfw"
	//"lib/ecs"
	//ps "lib/pubsub"
	ps "kristallos.ga/lib/pubsub"
	"kristallos.ga/rx"
	//. "math"
)

var (
	//_InputSys *rx.InputSys
	//_ObjSys      *ObjSys
	//_ShedSys *ShedSys
	//_Scene   *Scene
	//_Keymap  *Keymap
	// State
	//_Ship        *ShipEntity
	game *Game = nil
)

type Game struct {
	id       int
	app      *rx.App
	gopt     GameOptions // Game Options
	systems  map[string]GameSystemI
	timer    *Timer
	frame    int
	keymap   *Keymap
	scene    *Scene
	player   *Player
	inputsys *rx.InputSys
	timersys *TimerSys
	shedsys  *ShedSys
	objsys   *ObjSys
	field    *Field
	//collisionsys *CollisionSys
	entitysys *EntitySys
	pathsys   *PathSys
	//pathgraph *PathGraph
	pgRailway *PathGraph
	pgRoad    *PathGraph
	railtool  *RailTool
	roadtool  *RoadTool
	//fxsys        *FxSys
	//gui  *Gui
	time *GameTime
	// State
	//bg    *Background
	//ship  *Ship
	//level LevelI
	// A game was created
	gameCreated bool
	paused      bool
	initialized bool
}

func newGame(app *rx.App) *Game {
	if game != nil {
		return game
	}

	println("game.new")
	//SetDefaultVars()
	g := &Game{
		frame: 0,
		app:   app,
		//gopt:    newDefaultGameOptions(),
		timer:       newTimer(true),
		systems:     make(map[string]GameSystemI),
		paused:      false,
		initialized: false,
	}
	game = g // Early linking

	// Set Game Options
	g.setGameOptions(newDefaultGameOptions())

	g.objsys = newObjSys()
	g.field = newField()
	//g.collisionsys = newCollisionSys()
	g.entitysys = newEntitySys()
	g.pathsys = newPathSys()
	//g.pathgraph = newPathGraph()
	g.pgRailway = newPathGraph(WayTypeId_Railway)
	g.pgRoad = newPathGraph(WayTypeId_Road)
	g.railtool = newRailTool()
	g.roadtool = newRoadTool()
	//g.fxsys = newFxSys()
	g.inputsys = rx.NewInputSys(g.app)
	g.scene = newScene(g)
	g.player = newPlayer(PlayerType_Human)
	g.timersys = newTimerSys()
	g.shedsys = newShedSys()
	g.keymap = newKeymap()
	//g.gui = newGui()
	g.time = newGameTime()

	// XXX FIXME reapply game options for later systems (ugly)
	//g._updateGameOptions()

	// Set access vars
	//_InputSys = g.inputsys
	//_ShedSys = g.shedsys
	//_Scene = g.scene

	// Init complete
	g.initialized = true

	// Automatically start the game after creation (fixme)
	//g.start()

	sub(rx.Ev_key_press, g.onKeyPress)

	return game
}

func (g *Game) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == g.keymap.getKey1("pause") ||
		key == g.keymap.getKey2("pause") {
		// Toggle pause
		game.pause(!game.paused)
	}
}

func (g *Game) playLevel(name string) {
	/*
		if name == "level1" {
			g.level = newLevel1()
			g.level.start()
		} else if name == "level2" {
			g.level = newLevel2()
			g.level.start()
		} else {
			pp("unknown level:", name)
		}
	*/
}

func (g *Game) finishLevel() {
	//g.level.finish()
}

func (g *Game) setGameOptions(gopt GameOptions) {
	_log.Inf("[Game] setGameOptions")

	g.gopt = gopt
	g._updateGameOptions()
}

func (g *Game) _updateGameOptions() {
	_log.Inf("[Game] _updateGameOptions")

	gOpt := &g.gopt // Shortcut
	// Update game speed
	if gOpt._gameSpeed >= 0.1 {
		g.timer.speed = gOpt._gameSpeed
	} else {
		pp("gameSpeed is too low:", gOpt._gameSpeed)
	}

	// Finally
	gOpt.dirty = false
}

func (g *Game) addSystem(name string, s GameSystemI) {
	//g.systems = append(g.systems, s)
	g.systems[name] = s
}

func (g *Game) getSystem(name string) GameSystemI {
	return g.systems[name]
}

// XXX rename to startNewGame?
// Start new game
func (g *Game) start() {
	game.id = _ider.GetNextId("game")
	//g._setDefaultGame()
	game.createNewDefaultGame()
	//g.hud.start()
	if !g.gameCreated {
		pp("no game was created")
	}

	// Start registered subsystems
	for _, si := range g.systems {
		if s, ok := si.(StartableI); ok {
			_log.Inf("[Game] start subsystem:", si.Name())
			s.start()
		}
	}
}

// Stop the game
func (g *Game) stop() {
	// Stop registered subsystems
	for _, si := range g.systems {
		if s, ok := si.(StopableI); ok {
			_log.Inf("[Game] stop subsystem:", si.Name())
			s.stop()
		}
	}

	g._resetGame()
}

func (g *Game) pause(v bool) {
	//pp("not implemented")
	g.paused = v
}

func (g *Game) clearGame() {
	/*
		for _, u := range _UnitSys.getUnits() {
			//_UnitSys.removeUnit(u)
			deleteEntity(u)
		}
		for _, ei := range _BuildingSys.getElems() {
			deleteEntity(ei.(*ecs.Entity))
		}
	*/
	sce := _rxi.Scene
	_ = sce
	/*
		sce.RemoveMeshNode(sce.GetNodeById(1030))
		sce.RemoveMeshNode(sce.GetNodeById(4))
		sce.RemoveMeshNode(sce.GetNodeById(1029))
		sce.RemoveMeshNode(sce.GetNodeById(5))
		sce.RemoveMeshNode(sce.GetNodeById(3))
		sce.RemoveMeshNode(sce.GetNodeById(0))
		sce.RemoveMeshNode(sce.GetNodeById(2))
	*/
	/*
		fow := sce.GetNodeByName("fow_plane")
		sce.RemoveMeshNode(fow)
	*/
	sce.ClearScene()
	//dump(_rxi.Scene())
	//pp(_rxi.Scene().GetNodes())
	//pp(_rxi.Renderer().CurCam())
	//pp(_rxi.Scene().GetNodes())
}

//func game *Game { return gamei }

// Reset the game.
func (g *Game) _resetGame() {
	g.gameCreated = false
	//g.level = nil
	g.clearGame()
	//g.entitysys.listEntities()
	g.entitysys.clearElems()
	//g.shedsys.clear()
	//pp(2)
}

// Create new default empty game.
func (g *Game) createNewDefaultGame() {
	g._resetGame()
	g._initBaseGame()
	g._initDefaultGame()
	// Finally
	g.gameCreated = true
}

// Create new test empty game.
func (g *Game) createNewTestGame() {
	g._resetGame()
	g._initBaseGame()
	g._initTestGame()
	// Finally
	g.gameCreated = true
}

// Init base game structure.
func (g *Game) _initBaseGame() {

	/*
		// Add player
		p := newPlayer(PlayerType_Human)
		p.name = "player1"
		g.player = p

		// Add ship entity
		g.ship = newShip()

		// Add backround
		g.bg = newBackground()
	*/
}

func (g *Game) _initDefaultGame() {
	// TODO: move field here from main
}

func (g *Game) _initTestGame() {
}

func (g *Game) zero() bool {
	return g.frame == 0
}

func (g *Game) update(dt float64) {
	if game.paused {
		return
	}
	/*
		if game.frame == 0 {
			game.timer.start()
		}
	*/
	dt = dt * g.timer.speed

	// Update vars
	vars.dt = dt
	vars.dtGameSpeed = dt / g.gopt._gameSpeed

	// Update game options
	if g.gopt.dirty {
		g._updateGameOptions()
	}

	g.timersys.update(dt)
	//g.shedsys.update(dt)
	g.time.update(dt)
	/*
		g.field.update(dt)
		g.unitsys.update(dt)
		g.playersys.update(dt)
		g.fxsys.update(dt)
		g.scene.update(dt)
		//g.hud.update(dt)
		g.gui.update(dt)
	*/
	//g.player.update(dt)
	//g.gui.update(dt)
	//g.bg.update(dt)

	// Update registered systems
	for _, s := range g.systems {
		s.update(dt)
		//fmt.Println("Update", n, s)
		//p("Update", s)
	}
	//fmt.Println(g.systems)
	//g.hud.update(dt)
	updateGameState(dt)

	//g.timer.update(dt) // XXX updated by timersys
	g.frame += 1
}
