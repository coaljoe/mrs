package main

import (
	"fmt"
)

type Player struct {
	id       int
	ptype    PlayerType
	name     string
	disabled bool
}

func newPlayer(ptype PlayerType) *Player {
	p := &Player{
		id:       _ider.GetNextId("Player"),
		ptype:    ptype,
		name:     "Unknown",
		disabled: false,
	}
	if ptype == PlayerType_AI {
		//p.ai = newPlayerAi(p)
	}

	// Automatically add to PlayerSys
	//game.playersys.addPlayer(p) // Fixme: remove?
	return p
}

func (p *Player) isHuman() bool {
	if p.ptype == PlayerType_Human {
		return true
	}
	return false
}

func (p *Player) isAi() bool {
	return !p.isHuman()
}

func (p *Player) String() string {
	return fmt.Sprintf("Player<name: %s>", p.name)
}

func (p *Player) update(dt float64) {
	if p.disabled {
		return
	}
}

///////////////
// PlayerType

type PlayerType int

const (
	PlayerType_Human PlayerType = iota
	PlayerType_AI
)

func (p PlayerType) String() string {
	switch p {
	case PlayerType_Human:
		return "Human"
	case PlayerType_AI:
		return "AI"
	default:
		panic("unknown PlayerType")
	}
}
