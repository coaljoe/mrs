package main

func kmhToMs(kmh int) float64 {
	const msK = 5.0 / 18.0
	//const msK2 = 3.6
	return float64(kmh) * msK
}

func msToKmh(ms float64) float64 {
	const msK = 5.0 / 18.0
	//const msK2 = 3.6
	return float64(ms) / msK
}
