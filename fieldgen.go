package main

import (
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
	//"github.com/y0ssar1an/q"
)

//"github.com/bpicode/fritzctl/assert"

type FieldGenerateOptions struct {
	checkZlevels bool
	// Generate anything from these options at all
	disableEverything bool
	// HM times bigger than the field (fixme: can be calculated?)
	hmScale              int
	generateFieldObjects bool
	generateOilRigs      bool
	generateRoads        bool
	amountFieldObjects   float64
	amountOilRigs        float64
}

func newFieldGenerateOptions() FieldGenerateOptions {
	return FieldGenerateOptions{
		checkZlevels:      true,
		disableEverything: false,
		// Default HM is X times larger than field size
		hmScale:              4,
		generateFieldObjects: true,
		generateOilRigs:      false,
		generateRoads:        false,
		amountFieldObjects:   1.0,
		amountOilRigs:        1.0,
	}
}

// Verify options for correctness.
func (fgo FieldGenerateOptions) verify() {
	// Skip test
	if fgo.disableEverything {
		return
	}
	//if fgo.hmScale < 1 {
	//	pp("hmScale is less than 1;", fgo.hmScale)
	//}
	//assert.IsTrue(fgo.hmScale > 1,
	//	"hmScale is less than 1;", fgo.hmScale)
}

var _ = `
func generateFieldObjects(f *Field, fgOpt FieldGenerateOptions) {
	_log.Inf("[Fieldgen] generate field objects...")
	if fgOpt.disableEverything {
		return
	}

	fw := f.w / 4 //2
	fh := f.h / 4 //2

	// Add field objects
	if fgOpt.generateFieldObjects {
		for x := 0; x < fw; x++ {
			for y := 0; y < fh; y++ {
				cell := &f.cells[x][y]
				if !cell.z.isGround() {
					continue
				}
				if random(0, 1) > 0.9 {
					fo := newFieldObject("tree")
					fo.SetPosX(float64(x*cell_size + half_cell_size))
					fo.SetPosY(float64(y*cell_size + half_cell_size))
					// Set random orientation
					fo.SetRotZ(random(0, 360))
					f.objects = append(f.objects, fo)
				}
			}
		}
	}

	// Add oil rigs
	if fgOpt.generateOilRigs {
		oilRigCells := make([]Cell, 0)
		oilRigLocs := make([]CPos, 0)
		_ = oilRigCells
		_ = oilRigLocs

		// Props
		siteW, siteH := 3, 3
		//zlev := ZLevel_Sea1
		zlev := ZLevel_SeaMax + 1

		//pp(f.getCellsAtZLevel(zlev))
		//pp(f.getCellsAtZLevel(ZLevel_Ground0))
		//pp(f.getCellsAtZLevel(ZLevel_Sea1))
		//pp(f.getCellsAtZLevel(ZLevel(-1)))
		//pp(f.getCellsAtZLevels(ZLevel_Sea0, ZLevel_SeaMax))
		//pp(f.getCellsAtZLevels(ZLevel_SeaMax, ZLevel_Sea0))
		//pp(f.getCells())

		// Find suitable places for oilrigs
		for x := 0; x < f.w-siteW; x++ {
			for y := 0; y < f.h-siteH; y++ {
				//oilRigLocs = append(
				cell := &f.cells[x][y]
				if cell.z != zlev {
					continue
				}
				p(cell)

				suitable := true

				nb := f.getRectSlice(x, y, siteW, siteH)

				for _, n := range nb {
					if n.z != zlev {
						suitable = false
					}
				}

				if suitable {
					oilRigLocs = append(oilRigLocs, CPos{x, y})
				}
			}
		}

		//pp(oilRigLocs)

		for _, p := range oilRigLocs {
			x, y := p.x, p.y
			fo := newFieldObject("tree")
			fo.SetPosX(float64(x*cell_size/2 + half_cell_size/2))
			fo.SetPosY(float64(y*cell_size/2 + half_cell_size/2))
			//fo.setXRot(10)
			f.objects = append(f.objects, fo)
		}
	}

	_log.Inf("[Fieldgen] generate field objects complete")
}

func generateFieldZones(f *Field, fgOpt FieldGenerateOptions) {
	_log.Inf("[Fieldgen] generate field zones...")
	if fgOpt.disableEverything {
		return
	}

	// Generate camp sites
	for _, pl := range game.playersys.players {
		_log.Inf("[Fieldgen] generate camp site for player:", pl.name)
		zlev := ZLevel_Ground0
		minCells := 40 // Continuous cells
		_, _ = zlev, minCells
	}

	_log.Inf("[Fieldgen] generate field zones complete")
}
`

type FieldGen struct {
	roadNode    *rx.Node
	roadCapNode *rx.Node // Right road cap
	sgNode      *rx.Node
	f           *Field
}

func newFieldGen(f *Field) *FieldGen {
	fg := &FieldGen{f: f}
	return fg
}

func (fg *FieldGen) init() {
	sl := rx.NewSceneLoader()
	//nodes := sl.Load("res/objects/roads/road1/road1.dae")
	//nodes := sl.Load("res/objects/roads/road2/road2.dae")
	nodes := sl.Load("res/objects/roads/road3/road3.dae")
	sl.Spawn()
	node := nodes[0]
	node.Mesh.Material().Texture().SetMinFilter(rx.TextureFilterNearest)
	node.Mesh.Material().Texture().SetMagFilter(rx.TextureFilterNearest)
	fg.roadNode = node

	{
		nodes := sl.Load("res/objects/roads/road3/road3_cap.dae")
		sl.Spawn()
		node := nodes[0]
		node.Mesh.Material().Texture().SetMinFilter(rx.TextureFilterNearest)
		node.Mesh.Material().Texture().SetMagFilter(rx.TextureFilterNearest)
		fg.roadCapNode = node
	}

	fg.sgNode = rx.NewStaticGeometryNode()
	fg.sgNode.Mesh.SetMaterial(node.Mesh.Material())
	rx.Rxi().Scene.Add(fg.sgNode)
}

func (fg *FieldGen) generateRoad(from, to Vec3, roadClass string) {
	deg := degBetween(from.ToVec2(), to.ToVec2())
	dist := distancePos2(from.ToVec2(), to.ToVec2())
	//roadTileLength := 10.0
	roadTileLength := 100.0

	//pp(deg, dist, roadTileLength)
	p(deg, dist, roadTileLength)

	var _ = `
	coveredDist := 0.0
	i := 0
	for {
		n := fg.roadNode.Clone()
		n.SetPos(from)
		n.SetRot(Vec3{0, 0, deg})

		//n.SetPos(Vec3{xPos, yPos + float64(i)*roadTileLength, 0})
		//n.SetScale(Vec3{10, 10, 10})
		//n.SetScale(Vec3{10, 10, 10})
		//n.SetScale(Vec3{4, 4, 1})
		//node.SetScale(Vec3{1000, 1000, 1000})
		rx.Rxi().Scene().Add(n)

		n.MoveByVec(Vec3{float64(i) * roadTileLength, 0, 0})
		//n.MoveByVec(Vec3{float64(i), 0, 0})
		newPos := n.Pos()
		coveredDist = distancePos2(from.ToVec2(), newPos.ToVec2())

		if coveredDist > dist {
			break
		}

		i++

		//break
	}
	`

	/*
		n := fg.roadNode.Clone()
		n.SetPos(from)
		n.SetRot(Vec3{0, 0, deg})

		//n.SetPos(Vec3{xPos, yPos + float64(i)*roadTileLength, 0})
		//n.SetScale(Vec3{10, 10, 10})
		//n.SetScale(Vec3{10, 10, 10})
		//n.SetScale(Vec3{4, 4, 1})
		//node.SetScale(Vec3{1000, 1000, 1000})
		rx.Rxi().Scene().Add(n)

		// Scale road
		roadWidth := 4.0
		n.SetScale(Vec3{dist / roadTileLength, roadWidth, 1})

		// Move road
		n.MoveByVec(Vec3{roadTileLength / 2, 0, 0})

		// Road caps
		{
			nr := fg.roadCapNode.Clone()
			//nr.SetPos(from)
			nr.SetPos(to)
			nr.SetRot(Vec3{0, 0, deg})
			rx.Rxi().Scene().Add(nr)

			nr.SetScale(Vec3{roadWidth, roadWidth, 1})
			//nr.MoveByVec(Vec3{dist / 2, 0, 0})
			//pp(nr.Pos(), from, to)

			nl := fg.roadCapNode.Clone()
			nl.SetPos(from)
			nl.SetRot(Vec3{0, 0, 180})
			nl.AdjRot(Vec3{0, 0, deg})
			rx.Rxi().Scene().Add(nl)

			nl.SetScale(Vec3{roadWidth, roadWidth, 1})
		}
	*/

	//// Static geometry version
	//n := fg.roadNode.Clone()
	nMesh := fg.roadNode.Mesh
	n := fg.roadNode.Transform.Copy()
	n.SetPos(from)
	n.SetRot(Vec3{0, 0, deg})

	//rx.Rxi().Scene().Add(n)

	// Scale road

	//roadWidth := 4.0
	//roadWidth := 2.0
	//roadWidth := 1.0
	roadWidth := 1.5
	if roadClass == "motorway" || roadClass == "trunk" || roadClass == "primary" ||
		roadClass == "secondary" || roadClass == "-1" {
		//roadWidth = 4.0
		roadWidth = 4.5
	}

	n.SetScale(Vec3{dist / roadTileLength, roadWidth, 1})

	// Move road
	n.MoveByVec(Vec3{roadTileLength / 2, 0, 0})

	//fg.sgNode.Mesh.StaticGeometry.AddMeshAt(n.Mesh, n.WorldMat())
	fg.sgNode.Mesh.StaticGeometry.AddMeshAt(nMesh, n.WorldMat())

	// Road caps
	if true {
		//nr := fg.roadCapNode.Clone()
		nrMesh := fg.roadCapNode.Mesh
		nr := fg.roadCapNode.Transform.Copy()
		//nr.SetPos(from)
		nr.SetPos(to)
		nr.SetRot(Vec3{0, 0, deg})
		//rx.Rxi().Scene().Add(nr)

		nr.SetScale(Vec3{roadWidth, roadWidth, 1})
		//nr.MoveByVec(Vec3{dist / 2, 0, 0})
		//pp(nr.Pos(), from, to)

		fg.sgNode.Mesh.StaticGeometry.AddMeshAt(nrMesh, nr.WorldMat())

		//nl := fg.roadCapNode.Clone()
		nlMesh := fg.roadCapNode.Mesh
		nl := fg.roadCapNode.Transform.Copy()
		nl.SetPos(from)
		nl.SetRot(Vec3{0, 0, 180})
		nl.AdjRot(Vec3{0, 0, deg})
		//rx.Rxi().Scene().Add(nl)

		nl.SetScale(Vec3{roadWidth, roadWidth, 1})

		fg.sgNode.Mesh.StaticGeometry.AddMeshAt(nlMesh, nl.WorldMat())
	}

	game.roadtool.buildRoad(from, to)
	//game.pgRoad.lastPoint = nil // XXX fixme
}

func (fg *FieldGen) generateRoads() {
	//fg.generateRoad(Vec3{0, 0, 0}, Vec3{1000, 0, 0})
	//fg.generateRoad(Vec3{0, 0, 0}, Vec3{1000, 1000, 0})
	if true {
		//xPos := -2000.0
		//yPos := -2000.0
		xPos := -500.0
		yPos := -500.0
		//xStep := 200
		xStep := 400
		xSize := 1500
		//yStep := 200
		yStep := 400
		ySize := 1500
		_ = ySize
		_ = xStep
		_ = xSize
		//for x := 0; x < 1000; x += xStep {
		for y := 0; y < ySize; y += yStep {
			//p("yStep:", y)
			fg.generateRoad(Vec3{xPos, yPos + float64(y) + 1000, 0},
				//fg.generateRoad(Vec3{xPos, yPos + float64(y), 0},
				Vec3{xPos + float64(xSize), yPos + float64(y), 0}, "-1")
			break
		}
		//pp(2)
		/*
		for x := 0; x < xSize; x += xStep {
			break
			fg.generateRoad(Vec3{xPos + float64(x), yPos, 0},
				Vec3{xPos + float64(x), yPos + float64(ySize), 0}, "-1")
		}
		*/
	}

	//fg.generateRoad(Vec3{0, 0, 0}, Vec3{1000, 0, 0})
	//fg.generateRoad(Vec3{0, 200, 0}, Vec3{1000, 200, 0})
	//fg.generateRoad(Vec3{0, 400, 0}, Vec3{1000, 400, 0})

	// Place trees alongside the roads
	if true {
		//treeStep := 100.0
		//treeStep := 50.0
		treeStep := 40.0
		//roadWidth := 100.0
		roadWidth := 80.0
		_ = treeStep
		for e, pl := range game.pgRoad.links {
			p(e)
			p(pl)
			deg := degBetween(pl.p1.pos.ToVec2(), pl.p2.pos.ToVec2())
			length := distancePos2(pl.p1.pos.ToVec2(), pl.p2.pos.ToVec2())
			p("deg:", deg)
			p("length:", length)
			nSteps := int(length / treeStep)
			for i := 0; i < nSteps; i++ {
				// XXX use lerp?
				tr := newTransform()
				tr.SetPos(pl.p1.pos)
				tr.SetRotZ(deg)
				tr.MoveByVec(Vec3{treeStep * float64(i), 0, 0})
				//tr.Forward()

				p("place tree at:", tr.Pos())
				centerPos := tr.Pos()

				tr.MoveByVec(Vec3{0, roadWidth / 2, 0})
				t := newTree()
				t.spr.SetPos(tr.Pos())
				t.spawn()

				tr.SetPos(centerPos)
				tr.MoveByVec(Vec3{0, -(roadWidth / 2), 0})
				t2 := newTree()
				t2.spr.SetPos(tr.Pos())
				t2.spawn()
			}
		}
	}

	game.roadtool.buildRoadsFromOsmFile("res/data/osm/planet_28.141,45.314_28.157,45.323.osm", Vec3{0, 0, 0}, 2.0)

	// Place buildings alongside the roads
	if true {
		//treeStep := 100.0
		//treeStep := 50.0
		treeStep := 100.0
		//treeStep := 200.0
		//roadWidth := 100.0
		//roadWidth := 80.0
		roadWidth := 200.0
		_ = treeStep
		for e, pl := range game.pgRoad.links {
			p(e)
			p(pl)
			deg := degBetween(pl.p1.pos.ToVec2(), pl.p2.pos.ToVec2())
			length := distancePos2(pl.p1.pos.ToVec2(), pl.p2.pos.ToVec2())
			p("deg:", deg)
			p("length:", length)
			//pp(2)
			nSteps := int(length / treeStep)
			for i := 0; i < nSteps; i++ {
				// XXX use lerp?
				tr := newTransform()
				tr.SetPos(pl.p1.pos)
				tr.SetRotZ(deg)
				tr.MoveByVec(Vec3{treeStep * float64(i), 0, 0})
				//tr.Forward()

				p("place tree at:", tr.Pos())
				centerPos := tr.Pos()
				_ = centerPos

				tr.MoveByVec(Vec3{0, roadWidth / 2, 0})

				/*
					// Move manually to the roadside
					//tr.RotateZ(90)
					q.Q(tr.Pos(), tr.Rot())
					tr.AdjRot(Vec3{0, 0, 90})
					tr.MoveByVec(Vec3{roadWidth / 2, 0, 0})
					q.Q(tr.Pos(), tr.Rot())
					tr.AdjRot(Vec3{0, 0, -180})
					q.Q(tr.Pos(), tr.Rot())
				*/

				if false {

					//b := newBuilding("building2", ViewType_Sprite)
					//b := newBuilding("building3", ViewType_Mesh)
					b := makeBuilding("building4")
					// XXX buildings face south by default?
					b.rotDeg = tr.RotZ()
					//b.rotDeg = 270
					b.SetPos(tr.Pos())
					b.SetRot(tr.Rot())
					//p(b.Rot())
					b.LookAt2d(centerPos)
					//b.LookAt(centerPos)
					//pp(b.Rot())
					//pq(b.Pos(), centerPos, b.Rot())
					pq(b.Pos(), centerPos, b.Rot())
					//pp(2)
					spawnEntity(b)
					//t.spawn()

					ah := rx.NewAxesHelper()
					ah.SetPos(b.Pos())
					ah.DrawOriginLink = true
					ah.DrawOriginLinkColor = Vec3{1, 1, 0}
					ah.Size = 20
					//ah.Spawn()

					if true {
						//rotRes := 15
						rotRes := 10
						//rotRes := 5

						nowRot := b.RotZ()
						newRot := (int(nowRot/float64(rotRes)) * rotRes)
						b.SetRotZ(float64(newRot))
					}

				}

				/*
					tr.SetPos(centerPos)
					tr.MoveByVec(Vec3{0, -(roadWidth / 2), 0})
					t2 := newTree()
					t2.spr.pos = tr.Pos()
					t2.spawn()
				*/
			}
		}
	}
	//pp(2)

	// Build roads from osm file
	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/dn22_a_planet_27.915,45.267_27.924,45.272.osm", Vec3{0, 0, 0}, 10.0)
	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/dn22_a_planet_27.915,45.267_27.924,45.272.osm", Vec3{0, 0, 0}, 4.0)

	//planet_28.141,45.314_28.157,45.323.osm
	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/planet_28.141,45.314_28.157,45.323.osm", Vec3{0, 0, 0}, 4.0)
	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/planet_28.141,45.314_28.157,45.323.osm", Vec3{0, 0, 0}, 8.0)
	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/dn22_a_planet_27.915,45.267_27.924,45.272.osm", Vec3{0, 0, 0}, 8.0)

	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/braila_overpass_extract.osm", Vec3{0, 0, 0}, 8.0)
	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/braila_overpass_extract.osm", Vec3{0, 0, 0}, 4.0)
	//game.roadtool.buildRoadsFromOsmFile("res/data/osm/braila_overpass_extract.osm", Vec3{0, 0, 0}, 2.0)

	// Finally
	fg.sgNode.Mesh.StaticGeometry.Build()
}
