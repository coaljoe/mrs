package main

import . "strconv"

var _timersysi *TimerSys

func timersys() *TimerSys { return newTimerSys() }

type TimerSys struct {
	timers map[int]*Timer
}

func newTimerSys() *TimerSys {
	if _timersysi != nil {
		return _timersysi
	}
	s := &TimerSys{
		timers: make(map[int]*Timer),
	}
	_timersysi = s
	return _timersysi
}

func (ts *TimerSys) addTimer(t *Timer) {
	if ts.hasTimer(t) {
		panic("already have timer; id: " + Itoa(t.id))
	}
	ts.timers[t.id] = t
}

func (ts *TimerSys) removeTimer(t *Timer) {
	delete(ts.timers, t.id)
}

func (ts *TimerSys) hasTimer(t *Timer) bool {
	return ts.timers[t.id] != nil
}

func (ts *TimerSys) update(dt float64) {
	for _, t := range ts.timers {
		t.update(dt)
	}
}
