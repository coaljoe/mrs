package main

import (
	ps "kristallos.ga/lib/pubsub"
	"fmt"
)

type Field struct {
	w, h      int
	cells     [][]Cell
	hm        HeightMap
	generated bool
	fg        *FieldGen
	//layers    []*FieldLayer
	view *FieldView
}

func newField() *Field {
	f := &Field{
		hm: newHeightMap(),
		//layers: make([]*FieldLayer, 0),
	}
	f.fg = newFieldGen(f)
	//sub(ev_mouse_button_event, f.onMouseButtonEvent)
	return f
}

func (f *Field) onMouseButtonEvent(ev *ps.Event) {
	/*
		t := ev.Data.(*sdl.MouseButtonEvent)

		if t.Button == sdl.BUTTON_LEFT && t.State == sdl.PRESSED {
			cx, cy := vars.mx/cell_size, vars.my/cell_size
			f.view.removeTile(cx, cy)
		}
	*/
}

func (f *Field) spawn() {
	if !f.generated {
		pp("field wasn't generated")
	}

	f.view = newFieldView(f)
	f.view.spawn()
}

// General field generate function.
func (f *Field) generate(w, h int, hmW, hmH int,
	hmOpt HeightMapOptions, fgOpt FieldGenerateOptions) {
	f.w = w
	f.h = h

	// Generate cells
	f._generateCells()

	// Generate heightmap
	f.hm.generate(hmW, hmH, hmOpt)

	//f.hmScaled.generate(f.w, f.h, hmOpt)
	//f.hmScaled.clear()

	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			//z := f.hm.data[x*2][y*2] // Fixme: use average from nearest cells
			z := f.hm.data[x][y]
			//z := 0

			/*
				zVals := make([]int, 0)
				for i := 0; i < f.hmScale; i++ {
					for j := 0; j < f.hmScale; j++ {
						s := f.hmScale
						zVals = append(zVals, f.hm.data[x*s+i][y*s+j])
					}
				}
				zSum := 0
				for _, v := range zVals {
					zSum += v
				}
				zAvg := zSum / len(zVals)

				p(zVals, zSum, zAvg, f.hm.data[x][y])

				z := zAvg
			*/

			//p(z)
			cell := &f.cells[x][y]
			cell.z = ZLevel(z)
			//cell.z = ZLevel_Sea0
			//p("cell.z=", cell.z)

			//f.hmScaled.data[x][y] = z
		}
	}
	//f.cells[f.w/2][f.h/2].z = 1
	//f.cells[f.w/2][f.h/2].tileId = 0
	//f.cells[f.w/2][f.h/2].tileId = 15
	//pp(2)

	//f.generateRivers()
	f.fg.init()
	f.fg.generateRoads()

	f.generated = true

	// Generate extra objects and such
	//generateFieldObjects(f, fgOpt)
	//generateFieldTerrainFeatures()
	//generateFieldCampSites()
	//generateFieldZones(f, fgOpt)

	//_FowSys.generateFows(w, h)
	//game.fowsys.generateFows(w, h)

	// Debug
	f.hm.save(vars.tmpDir+"/hm.png", false)
	f.hm.save(vars.tmpDir+"/hm_orig.png", true)
	f.hm.saveTxt(vars.tmpDir + "/hm.txt")
	f.hm.saveColor(vars.tmpDir + "/hm_color.png")
	//pp(2)

	//f.hmScaled.save(vars.tmpDir + "/hm_scaled.png")
	//f.hmScaled.saveTxt(vars.tmpDir + "/hm_scaled.txt")

	//f.fixMalformedZLevels()

	// Attempt to update old hm.data with updated cell.z values
	//p(f.hm.data)

	for y := 0; y < f.hm.h; y++ {
		for x := 0; x < f.hm.w; x++ {
			z := f.cells[x][y].z
			//l := f.hm.ztol(int(z))
			//f.hm.data[x][y] = l
			f.hm.data[x][y] = int(z)
		}
	}

	//println()
	//pp(f.hm.data)

	// Debug
	f.hm.save(vars.tmpDir+"/hm_fixed.png", false)
	f.hm.saveTxt(vars.tmpDir + "/hm_fixed.txt")

	if fgOpt.checkZlevels {
		f.check()
	}

	// Add field layers
	//f.layers = append(
}

var _ = `
func (f *Field) generateRivers() {
	//for x := 0;
	startPoint := CPos{0, 5}
	endPoint := CPos{10, 5}
	//endPoint := CPos{10, 10}
	curPoint := startPoint
	path := pathfind(startPoint, endPoint)
	p(path)
	//node := path.popLastNode()
	//p(path)
	//p(node)
	//pp(2)
	//pp(path)
	for curPoint != endPoint {
		/*
			deg := degBetweenI(curPoint.x, curPoint.y, endPoint.x, endPoint.y)
			p(deg, curPoint)
			//pp(3)
			if deg == 0 {
				curPoint.x += 1
			} else {
				pp("unknown deg:", deg)
			}
		*/
		p(curPoint)
		node := path.popLastNode()
		curPoint = CPos{node.x, node.y}

		// Mark tile as river
		c := f.getCell(curPoint.x, curPoint.y)
		c.river = RiverState_River
		c.tileId = -1
	}
	//pp(2)
}
`

// Check field manually
func (f *Field) check() {
	//zlevel := ZLevel_Ground3
	//ok, x, y, dir := f.checkMalformedZLevels(zlevel)
	ok, x, y, dir := f.checkMalformedZLevels(1453, true)
	if !ok {
		f.dumpZLevels()
		pp("error: malformed z levels at x:", x, "y:", y, "dir:", dir)
	} else {
		p("field check ok")
	}
}

func (f *Field) getCells() []Cell {
	ret := make([]Cell, f.w*f.h)
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			ret[x*f.w+y] = f.cells[x][y]
			//pp("herp")
			//if f.cells[x][y].z > 0 {
			//	pp("derp")
			//}
		}
	}
	return ret
}

// Get cell
func (f *Field) getCell(x, y int) *Cell {
	//p("Field.getCell", x, y)
	xw := x
	yw := y
	//p(game.field.w, game.field.h)
	//p(xw, yw)
	cell := &f.cells[xw][yw]
	return cell
}

// Cell height at x, y.
func (f *Field) cellHeightAt(x, y int) ZLevel {
	return f.cells[x][y].z
}

// Heightmap height at x, y.
func (f *Field) heightMapHeightAt(x, y int) int {
	return f.hm.data[x][y]
}

func (f *Field) getCellNeighbours(p CPos, includeCentralCell bool) []Cell {
	if !includeCentralCell {
		return f.getCellNeighboursDepth(p, 1)
	} else {
		cells := f.getCellNeighboursDepth(p, 1)
		centralCell := f.getCell(p.x, p.y)
		// Insert
		//p_("cells before:", cells)
		i := 4
		cells = append(cells[:i], append([]Cell{*centralCell}, cells[i:]...)...)
		//p_("cells after:", cells)
		return cells
	}
	/*
		r := make([]Cell, 0)

		cx, cy := p.x, p.y
		if cx < 0 || cy < 0 {
			pp("cx or cy is less than zero;", cx, cy)
		}

		next := func(x, y int) {
			// Filter negative
			if x < 0 || y < 0 {
				return
			}
			c := f.cells[x][y]
			r = append(r, c)
		}

		next(cx-1, cy+1) // Top Left
		next(cx, cy+1)   // Top Center
		next(cx+1, cy+1) // Top Right

		next(cx-1, cy) // Center Left
		next(cx+1, cy) // Center Right

		next(cx-1, cy-1) // Bottom Left
		next(cx, cy-1)   // Bottom Center
		next(cx+1, cy-1) // Bottom Right

		return r
	*/
}

// Get rectangular slice of cells.
func (f *Field) getRectSlice(cx, cy, w, h int) []Cell {
	p("field.getRectSlice: cx:", cx, "cy:", cy, "w:", w, "h:", h)
	debug := false
	//ret := make([]Cell, w*h)
	ret := make([]Cell, 0)
	if debug {
		p(len(ret), ret)
	}
	for y := cy; y < h+cy; y++ {
		for x := cx; x < w+cx; x++ {
			if debug {
				p("->", x, y)
			}
			/*
				if x < 0 || y < 0 {
					// Skip
					continue
				}
			*/
			//ret = append(ret, f.cells[x][y])
			ret = append(ret, *f.getCell(x, y))
			//ret[x*w+y] = f.cells[cx+x][cy+y]
		}
	}
	if debug {
		p(len(ret), ret)
	}
	/*
		// Will work only with *nil version
		if Debug && len(ret) != w*h {
			pp("bad len:", len(ret))
		}
	*/
	return ret
}

// Get rectangular cell's neighbours
func (f *Field) getCellNeighboursDepth(p CPos, depth int) []Cell {
	//ret := make([]Cell, 0)
	/*
		ns := f.getCellNeighbours(p)
		for _, n := range ns {
			_ = n
			//nns := f.get
		}
	*/
	if depth < 1 {
		pp("depth should be > 0;", depth)
	}
	/*
		if p.x < 0 || p.y < 0 {
			pp("cell pos should be zero or positive;", p)
		}
	*/
	size := 3 + ((depth - 1) * 2)
	//println("size:", size)
	ret := f.getRectSlice(p.x-1, p.y-1, size, size)
	//p_("ret before:", ret)
	// Remove central cell
	//i := size / 2 // XXX BAD
	i := len(ret) / 2
	//p_("i:", i, "size:", size)
	ret = append(ret[:i], ret[i+1:]...)
	//p_("ret after:", ret)

	return ret
}

func (f *Field) isCellOpen(x, y int) bool {
	if f.cells[x][y].open {
		return true
	} else {
		return false
	}
}

func (f *Field) _generateCells() {
	w, h := f.w, f.h

	cells := make([][]Cell, w) // Rows
	for i := range cells {
		cells[i] = make([]Cell, h) // Cols
	}

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			cells[x][y] = Cell{
				open:   true,
				z:      0,
				tileId: -1,
				//tileId: 4,
				pos: CPos{x, y}, // Redundant
			}
		}
	}

	f.cells = cells
}

func (f *Field) cleanZLevels() {
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			f.cells[x][y].z = 0
		}
	}
}

func (f *Field) checkMalformedZLevels(zlevel ZLevel, checkall bool) (bool, int, int, string) {
	//for y := 1; y < f.h-1; y++ {
	//	for x := 1; x < f.w-1; x++ {
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {

			//cell := &f.cells[x][y]
			cell := f.getCell(x, y)

			if !checkall && cell.z != zlevel {
				continue
			}

			// topLeft
			if iabs(int(f.getCell(x-1, y-1).z)-int(cell.z)) > 1 {
				return false, x, y, "topLeft"
			}
			// topRight
			if iabs(int(f.getCell(x+1, y-1).z)-int(cell.z)) > 1 {
				return false, x, y, "topRight"
			}
			// bottomLeft
			if iabs(int(f.getCell(x-1, y+1).z)-int(cell.z)) > 1 {
				return false, x, y, "bottomLeft"
			}
			// bottomRight
			if iabs(int(f.getCell(x+1, y+1).z)-int(cell.z)) > 1 {
				return false, x, y, "bottomRight"
			}

			// top
			if iabs(int(f.getCell(x, y-1).z)-int(cell.z)) > 1 {
				return false, x, y, "top"
			}
			// right
			if iabs(int(f.getCell(x+1, y).z)-int(cell.z)) > 1 {
				return false, x, y, "right"
			}
			// bottom
			if iabs(int(f.getCell(x, y+1).z)-int(cell.z)) > 1 {
				return false, x, y, "bottom"
			}
			// left
			if iabs(int(f.getCell(x-1, y).z)-int(cell.z)) > 1 {
				return false, x, y, "left"
			}
		}
	}
	return true, -1, -1, "-"
}

func (f *Field) fixMalformedZLevels() {
	p("Field.fixMalformedZLevels")
	p("zlevels before fixing:")
	f.dumpZLevels()
	//zlevel := ZLevel_Ground3
	for zlevel := ZLevel_Min; zlevel <= ZLevel_Max; zlevel++ {
		for {
			p("fixMalformedZLevels iter")
			ok, x, y, dir := f.checkMalformedZLevels(zlevel, false)
			if !ok {
				//f.cells[x][y].printNeighbours()
				p("bad val:", x, y, dir)
			} else {
				break
			}
			centerZVal := f.cells[x][y].z
			var badZVal *ZLevel
			var badCell *Cell
			if dir == "topLeft" {
				badZVal = &f.getCell(x-1, y-1).z
				badCell = f.getCell(x-1, y-1)
			} else if dir == "topRight" {
				badZVal = &f.getCell(x+1, y-1).z
				badCell = f.getCell(x+1, y-1)
			} else if dir == "bottomLeft" {
				badZVal = &f.getCell(x-1, y+1).z
				badCell = f.getCell(x-1, y+1)
			} else if dir == "bottomRight" {
				badZVal = &f.getCell(x+1, y+1).z
				badCell = f.getCell(x+1, y+1)
			} else if dir == "top" {
				badZVal = &f.getCell(x, y-1).z
				badCell = f.getCell(x, y-1)
			} else if dir == "right" {
				badZVal = &f.getCell(x+1, y).z
				badCell = f.getCell(x+1, y)
			} else if dir == "bottom" {
				badZVal = &f.getCell(x, y+1).z
				badCell = f.getCell(x, y+1)
			} else if dir == "left" {
				badZVal = &f.getCell(x-1, y).z
				badCell = f.getCell(x-1, y)
			} else {
				pp("bad dir:", dir)
			}
			p("badCell before ->", int(badCell.z), badCell)
			p("current bad val:", int(*badZVal))
			//var fixedZVal ZLevel
			if centerZVal > *badZVal {
				*badZVal = centerZVal - 1
			} else {
				*badZVal = centerZVal + 1
			}
			p("new fixed val:", int(*badZVal))
			p("check vals:")
			p("current bad val:", int(*badZVal))
			//var fixedZVal ZLevel
			if centerZVal > *badZVal {
				*badZVal = centerZVal - 1
			} else {
				*badZVal = centerZVal + 1
			}
			p("new fixed val:", int(*badZVal))
			p("check vals:")
			p("current bad val:", int(*badZVal))
			//var fixedZVal ZLevel
			if centerZVal > *badZVal {
				*badZVal = centerZVal - 1
			} else {
				*badZVal = centerZVal + 1
			}
			p("new fixed val:", int(*badZVal))
			p("check vals:")
			p("badCell after ->", int(badCell.z), badCell)
			//println()
			//p("after:")
			//f.cells[x][y].printNeighbours()
			//println()
			//p(f.getCellNeighbours(CPos{x, y}, true))
		}
		f.dumpZLevels()
		p("fixed; zlevel:", zlevel)
		//pp(2)
	}
	p("zlevels after fixing:")
	f.dumpZLevels()
}

func (f *Field) dumpZLevels() {
	p("zs:")
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			fmt.Printf("%4d", f.cells[x][y].z)
		}
		println()
	}
}

func (f *Field) update(dt float64) {
	//p("field.update")
	//f.view.draw()
}

/////////
// Cell

type Cell struct {
	// Redundant but useful. use with caution.
	pos  CPos
	z    ZLevel
	open bool
	//	river  RiverState
	tileId int
	// Cell holder, default nil (no-one)
	//playerId int
	//holder *Player
}

func (c Cell) String() string {
	return fmt.Sprintf("Cell<%d,%d>", c.pos.x, c.pos.y)
}

func (c Cell) printNeighbours() {
	p("Cell.printNeighbours;", c)
	neighbours := game.field.getCellNeighbours(c.pos, true)
	for y := 0; y < 3; y++ {
		for x := 0; x < 3; x++ {
			fmt.Printf("%#v", neighbours[y*3+x])
			if x < 2 {
				print(" | ")
			}
		}
		println()
	}
}

// Cell pos
type CPos struct {
	x, y int
}
