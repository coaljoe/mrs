#!/bin/bash

src=$1
dst=`realpath $2`

mkdir $dst
cd $src

for f in 0*.png; do

	convert -trim $f $dst/$f

done

cd $dst
montage -mode concatenate 0*.png -background none out_concat.png
#montage -geometry +0+0 0*.png -background none out.png

max_w=`identify -format "%w %h %fn\n" 0*.png | sort -n -r -k 1 |  cut -d ' ' -f 1 | head -n 1`
max_h=`identify -format "%w %h %fn\n" 0*.png | sort -n -r -k 2 |  cut -d ' ' -f 1 | head -n 1`

echo "max_w: $max_w"
echo "max_h: $max_h"

max_val=""

if (( $max_w > $max_h )); then
	max_val=$max_w
else
	max_val=$max_h
fi

echo "max_val: $max_val"

#montage -geometry ${max_w}x${max_h}+0+0 0*.png -background none out.png
montage -geometry ${max_val}x${max_val}\>+0+0 0*.png -background none -depth 8 out.png
