package main

type EntityI interface {
	//SpawnableI
	DestroyableI
	getView() ViewI
	getId() int
	isDead() bool
	start()
	update(dt float64)
}

func spawnEntity(e EntityI) {
	game.systems["EntitySys"].addElem(e)
	//e.spawn()
	e.start()
	e.getView().spawn()
}

func destroyEntity(e EntityI) {
	//p("YYY destroyEntity id:", e.getId())
	game.systems["EntitySys"].removeElem(e)
	e.destroy()
	e.getView().destroy()
}

func tryDestroyEntity(e EntityI) {
	if game.systems["EntitySys"].hasElem(e) {
		game.systems["EntitySys"].removeElem(e)
		e.destroy()
		e.getView().destroy()
	}
}

func updateEntity(e EntityI, dt float64) {
	//p("XXX updateEntity id:", e.getId())
	e.update(dt)
	e.getView().update(dt)
}
