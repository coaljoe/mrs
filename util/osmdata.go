package util

import (
	"fmt"
	"github.com/paulmach/osm"
	//"github.com/paulmach/osm/osmxml"
	"encoding/xml"
	"io/ioutil"
	//"os"
	"log"
)

// OSM node tag
type OsmPathNode struct {
	Id  int64
	Lat float64
	Lon float64
	CustomData map[string]interface{}
}

// OSM way tag
type OsmPathInfo struct {
	Id    int64
	Type  string
	//https://wiki.openstreetmap.org/wiki/Key:highway#Roads
	RoadClass string
	Nodes []OsmPathNode
}

func GetOsmPathDataFromFile(path string) []OsmPathInfo {
	println("util.GetOsmPathDataFromFile;", path)
	d, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	o := &osm.OSM{}
	err = xml.Unmarshal(d, &o)
	if err != nil {
		panic(err)
	}

	ret := make([]OsmPathInfo, 0)

	println("objects:")
	for _, obj := range o.Objects() {
		fmt.Println(obj)
		fmt.Println("ObjectID ->", obj.ObjectID())
		fmt.Println("ObjectID.Ref ->", obj.ObjectID().Ref())
		if obj.ObjectID().Type() == osm.TypeWay {
			way := obj.(*osm.Way)
			if way.Tags.Find("highway") == "" {
				continue
			}

			roadClasses := []string{"motorway", "trunk", "primary", "secondary", "tertiary",
				"unclassified", "residential"}
			roadClass := ""
			/*
			//log.Panicln(way.Tags.Map())
			if way.Tags.Map()["highway"] == "residential" {
				//log.Println("XXX skip residential road")
				//panic(2)
				//continue

			}
			*/

			tm := way.Tags.Map()
			for _, c := range roadClasses {
				if tm["highway"] == c {
					roadClass = c
				}
			}

			_ = way
			pi := OsmPathInfo{
				Id:    int64(way.ID),
				Type:  "highway",
				RoadClass: roadClass,
				Nodes: make([]OsmPathNode, 0),
			}
			_ = pi

			//for _, ndRef := range way.Nodes {
			for _, ndRef := range way.Nodes {
				// Lookup for Node
				var nd *osm.Node
				for _, obj2 := range o.Objects() {
					if obj2.ObjectID().Ref() == ndRef.ElementID().Ref() {
						nd = obj2.(*osm.Node)
					}
				}
				//nd = nil
				if nd == nil {
					//panic("error: node lookup failed: no node found")
					log.Panic("error: node lookup failed: no node found; ref id: ", ndRef.ElementID().Ref())
				}
				fmt.Println("nd ->", nd)
				pn := OsmPathNode{
					Id:  int64(nd.ID),
					Lat: nd.Lat,
					Lon: nd.Lon,
					CustomData: make(map[string]interface{}),
				}

				pi.Nodes = append(pi.Nodes, pn)
			}

			ret = append(ret, pi)
		}
	}
	println("end objects")

	println("done")

	return ret
}
