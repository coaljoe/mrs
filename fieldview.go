package main

import (
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
)

type FieldView struct {
	m               *Field
	tileSize        int
	terrainTileNode *rx.Node
}

func newFieldView(m *Field) *FieldView {
	v := &FieldView{
		m:        m,
		tileSize: cell_size,
	}

	return v
}

func (v *FieldView) load() {
	sl := rx.NewSceneLoader()
	//nodes := sl.Load("res/objects/roads/road1/road1.dae")
	//nodes := sl.Load("res/objects/roads/road2/road2.dae")
	nodes := sl.Load("res/objects/field/terraintile/road2.dae")
	sl.Spawn()
	node := nodes[0]
	//node.Mesh.Material().Texture().SetMinFilter(rx.TextureFilterNearest)
	//node.Mesh.Material().Texture().SetMagFilter(rx.TextureFilterNearest)
	v.terrainTileNode = node
}

func (v *FieldView) spawn() {
	v.load()
	//v.terrainTileNode.(Vec3{4, 4, 1})
	//v.terrainTileNode.SetScale(Vec3{1000, 1000, 1})
	//v.terrainTileNode.SetScale(Vec3{100, 100, 1})
	v.terrainTileNode.SetScale(Vec3{50, 50, 1})
	//node.SetScale(Vec3{1000, 1000, 1000})
	v.terrainTileNode.SetPosZ(-0.02)
	rx.Rxi().Scene.Add(v.terrainTileNode)
}
