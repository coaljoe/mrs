package main

import (
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
	"github.com/pelletier/go-toml"
	"math"
)

// Cache
var cSpriteMesh *rx.Node

type Sprite struct {
	*rx.Node
	dim Vec2
	//pos  Vec3
	mesh      *rx.Node
	tex       *rx.Texture
	numTilesX int
	numTilesY int
	numTiles  int
	tileSize  int
	// Y shift (yCorr) for sprite (px)
	shiftY     int     // px
	shiftYReal float64 // units
	spawned    bool
	//card *rx.Card
}

func newSprite() *Sprite {
	s := &Sprite{
		Node: rx.NewCustomNode(),
		//dim:  Vec2{50, 50},
		//dim: Vec2{60, 60 * (220.0 / 161.0)},
		//dim: Vec2{60, 60 * (220.0 / 161.0)},
		//dim: Vec2{85.0 / 2.0, 142 / 2.0},
		//dim: Vec2{150, 150 * (220.0 / 161.0)},
		//dim:  Vec2{161.0 * _rxi.Renderer().GetPw(), 220.0 * _rxi.Renderer().GetPh()},
		//dim: Vec2{1, 1},
		//dim: Vec2{float64(vars.resX), float64(vars.resY)},
		//dim: Vec2{_rxi.Camera.Camera.Fov(), _rxi.Camera.Camera.Fov()},
		numTilesX: 1,
		numTilesY: 1,
		numTiles:  1,
		//pos:  Vec3Zero,
		mesh: rx.NewMeshNode(),
		//card: rx.NewCard(),
	}

	var _ = `
	//w := 161.0
	//h := 220.0
	w = 161.0 * _rxi.Renderer().GetPw()
	h = 220.0 * _rxi.Renderer().GetPh()
	p(w, h)
	//rm := _rxi.Camera.Camera.GetViewMatrix()
	//game.scene.camera.cam.SetPos(Vec3{0, 0, 0})
	rm := game.scene.camera.cam.WorldMat()
	v := Vec3{w, h, 1}
	v = rm.MulVec3(v)
	p(game.scene.camera.cam.Pos())
	p(game.scene.camera.cam.Rot())
	//pp(v)
	`

	return s
}

func (s *Sprite) load(path string) {
	s.loadTiles(path, 1)
}

func (s *Sprite) loadTiles(path string, numTiles int) {
	_log.Dbg("%F path:", path, "numTiles:", numTiles)

	// Read .info
	hasInfo := false
	var d *toml.Tree
	if ok, err2 := exists(path + ".info"); ok {
		if err2 != nil {
			panic(err2)
		}
		var err error
		d, err = toml.LoadFile(path + ".info")
		if err != nil {
			panic(err)
		}

		hasInfo = true
	}

	if false && hasInfo {
		p(d)
		pp(hasInfo)
	}

	//s.tex = rx.NewTexture("res/buildings/building1/building1.png")
	//s.tex = rx.NewTexture("res/buildings/building2/building2.png")
	s.tex = rx.NewTexture(path)
	s.tex.SetMinFilter(rx.TextureFilterNearest)
	s.tex.SetMagFilter(rx.TextureFilterNearest)

	tw := s.tex.Width()
	th := s.tex.Height()
	var tileSizeX, tileSizeY int
	_ = tileSizeX
	_ = tileSizeY
	if numTiles == 1 {
		tileSizeX = tw
		tileSizeY = th
	} else {
		tileSize := int(math.Sqrt(float64((tw * th) / numTiles)))
		tileSizeX = tileSize
		tileSizeY = tileSize
		//pp(tileSizeX)
	}

	s.numTilesX = tw / tileSizeX
	s.numTilesY = th / tileSizeY
	s.numTiles = numTiles

	// XXX only square tiles supported
	s.tileSize = tileSizeX

	//w := 161.0
	//h := 220.0
	w := float64(s.tex.Width())
	h := float64(s.tex.Height())

	if numTiles > 1 {
		w /= float64(s.numTilesX)
		h /= float64(s.numTilesY)
	}

	defaultScaleDown := 2
	if true {
		if numTiles > 1 { // XXX FIXME
			w /= float64(defaultScaleDown)
			h /= float64(defaultScaleDown)
		}
	}

	camScaleX := _rxi.Camera.Camera.Fov()
	camScaleY := camScaleX
	//kw := float64(vars.resX) / camScaleX
	//kh := float64(vars.resY) / camScaleY
	kw := float64(vars.nativeResX) / camScaleX
	kh := float64(vars.nativeResY) / camScaleY
	aspect := float64(vars.resX) / float64(vars.resY)
	//sxOfRes := float64(vars.resX) / w
	//dx := float64(vars.resX) / w / camScaleX
	//dy := float64(vars.resY) / h / camScaleY
	//dx := camScaleX
	//dy := camScaleY
	dx := w / kw
	dy := h / kh
	//pp(dx, dy)
	s.dim = Vec2{dx, dy / aspect}

	// shiftY
	if hasInfo && d.Has("originOffsetY") {
		// shiftY
		offsetY := int(d.Get("originOffsetY").(int64))
		s.shiftY = tileSizeX - offsetY

		//s.shiftY = 106
		//s.shiftYReal = 1.0 * s.dim.Y()
		//p(s.shiftYReal)
		s.shiftYReal = float64(s.shiftY) * (s.dim.Y() / float64(s.tileSize))
		//p(s.shiftYReal)
		//pp(3)
		//pp(s.shiftY, offsetY, tileSizeX)
	}
}

func (s *Sprite) spawn() {
	if cSpriteMesh == nil {
		sl := rx.NewSceneLoader()
		//nodes := sl.Load("res/models/primitives/box/box.dae")
		//nodes := sl.Load("res/models/primitives/sprite/sprite.dae")
		nodes := sl.LoadNoCache("res/models/primitives/sprite/sprite.dae")
		s.mesh = nodes[0]

		mat := rx.NewMaterial("res/materials/sprite.json")
		s.mesh.Mesh.SetMaterial(mat)

		s.mesh.Mesh.Material().SetTexture(s.tex)
		//s.mesh.Mesh.Material().Alpha = true
		s.mesh.Mesh.Material().AlphaTest = true

		cSpriteMesh = nodes[0]
	} else {
		s.mesh = cSpriteMesh.Clone()
		//s.mesh.Mesh.Material().Clone()
		//mat := rx.NewMaterial("res/materials/static.json")
		mat := rx.NewMaterial("res/materials/sprite.json")
		s.mesh.Mesh.SetMaterial(mat)
		s.mesh.Mesh.Material().SetTexture(s.tex)
		s.mesh.Mesh.Material().AlphaTest = true
	}

	s.mesh.Mesh.Material().AlphaTest = false
	s.mesh.Mesh.Material().Alpha = true
	// XXX use alpha hack to override write depth
	// TODO: doesn't work properly without depth sorting(?)
	s.mesh.Mesh.Material().AlphaWriteDepth = true

	s.mesh.Mesh.Material().AddUniform("fill_color", "4f", false, 1.0, 0.0, 0.0, 1.0)

	if s.numTiles > 1 {
		sx := 1.0 / float64(s.numTilesX)
		sy := 1.0 / float64(s.numTilesY)
		s.mesh.Mesh.Material().AddUniform("scale_texCoord", "2f", false, sx, sy)
		// XXX TODO add shift_texCoord
	}

	ps := s.mesh.Mesh.MeshBuffer().GetPositions()
	p(ps)
	p()
	tc := s.mesh.Mesh.MeshBuffer().GetTexCoords()
	_ = tc
	//pp(tc)

	dim3 := Vec3{s.dim.X(), 1, s.dim.Y()}
	//dim3.SetZ(1)
	s.mesh.SetScale(dim3)
	//s.mesh.LookAt(_rxi.Camera.Pos())
	s.mesh.SetRotZ(45)
	s.mesh.SetRotX(-30) // Same as 60 for unrotated plane (in blender)
	// Update sprite rot
	s.SetRot(s.mesh.Rot())

	_rxi.Scene.Add(s.mesh)
	//_rxi.Scene().AddRenderable(s)

	s.spawned = true
}

func (s *Sprite) update(dt float64) {
	pos := s.Pos()
	s.mesh.SetPos(pos)

	// shiftY
	if s.shiftY != 0 {
		//pp(s.shiftY)
		shY := float64(s.shiftYReal)
		_ = shY
		// XXX FIXME Work-around scale-translate bug in rx
		oldScale := s.mesh.Scale()
		s.mesh.SetScale(Vec3One)
		//shY = 1.0
		s.mesh.MoveByVec(Vec3{0, 0, -shY})
		s.mesh.SetScale(oldScale)

		yCorr := s.shiftY
		newPos := s.mesh.Pos()
		//p(newPos)
		newPos = newPos.MoveTowards(game.scene.camera.cam.Pos(), 2.0*float64(yCorr))
		//p(newPos)
		//pp(3)
		s.mesh.SetPos(newPos)
	}

	/*
		dim3 := Vec3{s.dim.X(), 1, s.dim.Y()}
		//dim3.SetZ(1)
		s.mesh.SetScale(dim3)
	*/

	r := random(0, 1)
	s.mesh.Mesh.Material().AddUniform("fill_color", "4f", false, r, 0.0, 0.0, 1.0)
}

func (s *Sprite) Render(r *rx.Renderer) {
	pp(2)
}
