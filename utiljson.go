// Json utilities.
package main

import ()

/*
func _getObject2(
	data map[string]interface{}, name string) map[string]interface{} {
	return data[name].(map[string]interface{})
}
*/
// Convert json5 data to json5 object.
func _getObject(data interface{}) map[string]interface{} {
	return data.(map[string]interface{})
}

func _getObjectCheck(data interface{}) (map[string]interface{}, bool) {
	if data == nil {
		return nil, false
	}
	return data.(map[string]interface{}), true
}

// Get string value from json5 def.
func _getString(def map[string]interface{}, name string) string {
	if v := def[name]; v != nil {
		return v.(string)
	} else {
		panic("read fail key=" + name)
	}
}

// Get float value from json5 def.
func _getFloat(def map[string]interface{}, name string) float64 {
	if v := def[name]; v != nil {

		return v.(float64)
	} else {
		panic("read fail key=" + name)
	}
}

// Get int value from json5 def.
func _getInt(def map[string]interface{}, name string) int {
	vf := _getFloat(def, name)
	// Check if value is integer
	if vf != float64(int(vf)) {
		pp("error: json value is not integer:", vf)
	}
	return int(vf)
}
