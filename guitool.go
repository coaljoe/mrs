package main

import (
	//"./util"
	//"main/util"
	. "kristallos.ga/rx/math"
	"github.com/go-spatial/tegola/maths/webmercator"
	"gitlab.com/coaljoe/mrs/util"
	"math"
)

type GuiToolType int

const (
	GuiToolType_None = iota
	GuiToolType_Rail
	GuiToolType_Road
	GuiToolType_Tram
	GuiToolType_Monorail
)

type GuiTool struct {
	name string
	typ  GuiToolType
}

func newGuiTool(name string, typ GuiToolType) *GuiTool {
	gt := &GuiTool{
		name: name,
		typ:  typ,
	}
	return gt
}

type RoadBuildMode int

const (
	RoadBuildMode_Freeform = iota
	RoadBuildMode_Tiles
)

type RailTool struct {
	*GuiTool
	// Active rail type
	railwayType *RailwayType
	buildMode   RoadBuildMode
}

func newRailTool() *RailTool {
	rt := &RailTool{
		GuiTool: newGuiTool("Rail tool", GuiToolType_Rail),
		//railway:  railwayTypes[0], // Default rail type
		buildMode: RoadBuildMode_Freeform,
	}
	rt.railwayType = rt.defaultRailwayType()
	return rt
}

func (rt *RailTool) defaultRailwayType() *RailwayType {
	return railwayTypes[0]
}

func (rt *RailTool) buildRoad(from, to Vec3) {
	if rt.buildMode == RoadBuildMode_Freeform {
		/*
			//game.pathgraph.lastPoint = from
			toPP := game.pgRailway._newPathPoint(to)
			//toPP.color = Vec3{1, 0, 1}
			game.pgRailway.addPointFromPos(from, toPP)
			fromPP := game.pgRailway.getPointByPos(from)
			pl := toPP.getLinkToPoint(fromPP)
			pl.color = Vec3{1, 0, 1}
			pl.railway = rt.railwayType
		*/

		pl := game.pgRailway.createPathLink(from, to)
		pl.color = Vec3{1, 0, 1}
		pl.railway = rt.railwayType
	} else {
		pp("unsupported")
	}
}

type RoadTool struct {
	*GuiTool
	// Active rail type
	roadType  *RoadType
	buildMode RoadBuildMode
}

func newRoadTool() *RoadTool {
	rt := &RoadTool{
		GuiTool: newGuiTool("Road tool", GuiToolType_Road),
		//railway:  railwayTypes[0], // Default rail type
		buildMode: RoadBuildMode_Freeform,
	}
	rt.roadType = rt.defaultRoadType()
	return rt
}

func (rt *RoadTool) defaultRoadType() *RoadType {
	return roadTypes[0]
}

func (rt *RoadTool) buildRoad(from, to Vec3) {
	_log.Inf("%F from:", from, "to:", to)
	if rt.buildMode == RoadBuildMode_Freeform {

		var _ = `
		if game.pgRoad.getPointByPosCheck(from) == nil {
			_log.Inf("%F warning: no from point, adding one")
			fromPP := game.pgRoad._newPathPoint(from)
			game.pgRoad.addPoint(fromPP)
			//pp(game.pgRoad.points)
		}

		//game.pathgraph.lastPoint = from
		toPP := game.pgRoad._newPathPoint(to)
		//toPP.color = Vec3{1, 0, 1}
		//game.pgRoad.addPointFromPos(from, toPP)
		game.pgRoad.addPointFromPos(from, toPP)
		//pp(game.pgRoad.points)
		fromPP := game.pgRoad.getPointByPos(from)
		//pp(game.pgRoad.links)
		pl := toPP.getLinkToPoint(fromPP)
		pl.color = Vec3{1, 1, 0}
		pl.road = rt.roadType
		`

		pl := game.pgRoad.createPathLink(from, to)
		pl.color = Vec3{1, 1, 0}
		pl.road = rt.roadType
		//game.pgRoad.lastPoint = nil // XXX fixme

	} else {
		pp("unsupported")
	}
}

func (rt *RoadTool) buildRoadsFromOsmFile(path string, pos Vec3, scale float64) {
	// extra info:
	// https://godoc.org/github.com/paulmach/go.geo#pkg-variables (ScalarMercator)
	// https://www.netzwolf.info/osm/tilebrowser.html?lat=51.157800&lon=6.865500&zoom=14
	// https://wiki.openstreetmap.org/wiki/Mercator
	// https://en.wikipedia.org/wiki/Web_Mercator_projection
	// https://dominoc925-pages.appspot.com/mapplets/cs_ecef.html

	pis := util.GetOsmPathDataFromFile(path)
	//pp(pis)
	maxLat, minLat := 0.0, math.MaxFloat64
	maxLon, minLon := 0.0, math.MaxFloat64
	for _, pi := range pis {
		for _, nd := range pi.Nodes {
			if nd.Lat > maxLat {
				maxLat = nd.Lat
			}
			if nd.Lat < minLat {
				minLat = nd.Lat
			}

			if nd.Lon > maxLon {
				maxLon = nd.Lon
			}
			if nd.Lon < minLon {
				minLon = nd.Lon
			}

			x := webmercator.LonToX(nd.Lon)
			y := webmercator.LatToY(nd.Lat)
			webmercatorCoord := Vec3{x, y, 0}
			nd.CustomData["webmercatorCoord"] = webmercatorCoord
		}
	}

	cartMin := convertSphericalToCartesian(minLat, minLon)
	cartMax := convertSphericalToCartesian(maxLat, maxLon)
	wmMin := Vec3{webmercator.LonToX(minLon), webmercator.LatToY(minLat), 0}
	wmMax := Vec3{webmercator.LonToX(maxLon), webmercator.LatToY(maxLat), 0}

	p(minLat, maxLat)
	p(minLon, maxLon)
	p(cartMin, cartMax)
	p(wmMin, wmMax)
	p(wmMax.Sub(wmMin))

	// "Normalize" coords
	for _, pi := range pis {
		for _, nd := range pi.Nodes {
			wmCoord := nd.CustomData["webmercatorCoord"].(Vec3)
			nx := wmCoord.X() - wmMin.X()
			ny := wmCoord.Y() - wmMin.Y()
			nCoord := Vec3{nx, ny, 0}
			p("nCoord:", nCoord, "coord:", nd.Lat, nd.Lon, nd.Id)
			nd.CustomData["normalizedWmCoord"] = nCoord
		}
	}

	//pp(pis)

	// Place roads
	oldWidth := wmMax.X() - wmMin.X()
	newWidth := float64(oldWidth) * scale
	_ = newWidth
	//pp(oldWidth, newWidth)
	for _, pi := range pis {
		//for i := 0; i < len(pi.Nodes)-1; i += 2 {
		for i := 0; i < len(pi.Nodes)-1; i++ {
			n1 := pi.Nodes[i]
			n2 := pi.Nodes[i+1]
			p1 := n1.CustomData["normalizedWmCoord"].(Vec3)
			p2 := n2.CustomData["normalizedWmCoord"].(Vec3)

			p("b:", p1, p2)

			if true {
				p1 = p1.MulScalar(scale)
				p2 = p2.MulScalar(scale)

				p1 = p1.SubScalar(newWidth / 2.0)
				p2 = p2.SubScalar(newWidth / 2.0)

				p1.SetZ(0)
				p2.SetZ(0)
			}
			p("a:", p1, p2)

			// XXX FIXME doesn't create views
			//rt.buildRoad(p1, p2)

			// XXX temporary?
			game.field.fg.generateRoad(p1, p2, pi.RoadClass)
		}
	}
	//pp(2)
}
