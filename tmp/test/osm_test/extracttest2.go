package main

import (
	//"fmt"
	"github.com/paulmach/osm"
	//"github.com/paulmach/osm/osmxml"
	"os"
)

func main() {
	fmt.Println("main()")

	path := os.Args[1]

	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	enc := xml.NewEncoder(f)
	startEl := xml.StartElement{
		Name: "osm",
	}
	o := osm.OSM{}
	o.MarschalXML(enc, startEl)

	println("done")
}
