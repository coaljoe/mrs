package main

import (
	"fmt"
	"github.com/paulmach/osm"
	//"github.com/paulmach/osm/osmxml"
	"encoding/xml"
	"io/ioutil"
	"os"
)

func main() {
	fmt.Println("main()")

	path := os.Args[1]

	/*
		//f, err := os.Open(path)
		f, err := os.Create(path)
		if err != nil {
			panic(err)
		}

		enc := xml.NewEncoder(f)
		startEl := xml.StartElement{
			Name: xml.Name{
				Space: "",
				Local: "osm"},
		}
		o := osm.OSM{}
		err = o.MarshalXML(enc, startEl)
		if err != nil {
			panic(err)
		}
	*/
	d, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	/*
		var o *osm.OSM
		o, err = osm.UnmarshalOSM(d)
		if err != nil {
			panic(err)
		}
	*/
	o := &osm.OSM{}
	err = xml.Unmarshal(d, &o)
	if err != nil {
		panic(err)
	}

	println("objects:")
	for _, obj := range o.Objects() {
		fmt.Println(obj)
	}
	println("end objects")

	println("done")
}
