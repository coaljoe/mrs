package main

import (
	"context"
	"fmt"
	"github.com/paulmach/osm"
	"github.com/paulmach/osm/osmxml"
	"os"
)

func main() {
	fmt.Println("main()")

	scanner := osmxml.New(context.Background(), os.Stdin)
	for scanner.Scan() {
		//fmt.Println(scanner.Object().(*osm.Changeset))
		//fmt.Println(scanner.Object().(*osm.Node))
		switch x := scanner.Object().(type) {
		case *osm.Node:
			node := scanner.Object().(*osm.Node)
			fmt.Println(node)
			fmt.Println("tags:", node.Tags)
		case *osm.Way:
			way := x
			fmt.Println(way)
			fmt.Println("tags:", way.Tags)
			if way.Tags.Find("highway") != "" {
				println("highway!")
			}
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}

	println("done")
}
