//attribute vec3 position;
attribute vec3 v_position;
attribute vec3 v_normal;
attribute vec2 v_texCoord;
// extra
//attribute vec2 v_position2d;

uniform mat4 m, v, p;
uniform mat4 mvp;
uniform mat4 m_3x3_inv_transp; // XXX warn: actually mat4
uniform vec3 lightPosition;

varying vec3 f_position;
varying vec3 f_normal;
varying vec2 f_texCoord;

/*
varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec3 H;
*/

#ifdef _TEXTURE_MAP
//vec2 vertexUV;
//vec2 UV;
#endif

#define _SHADOWS 0
#ifdef _SHADOWS
/*
uniform mat4 shadowMatrix;
varying vec4 shadowCoord;
*/
#endif
