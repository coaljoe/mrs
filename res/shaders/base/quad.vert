//#define _TEXTURE_MAP 1
#include "uniforms.vert"
varying vec3 f_color;

void main(void) {
  //f_color = v_color;
  f_color = vec3(0.0, 1.0, 1.0);
  f_texCoord = v_texCoord;
  
  //gl_Position = vec4(v_position2d, 0.0, 1.0);
  
  //vec3 screen_position = v_position;
  //screen_position.xy = 0.5 * screen_position.xy + vec2(0.5);
  //screen_position.x = 0.5 * screen_position.x + 0.5;
  //screen_position.y = 0.5 * screen_position.y + 0.5;
  
  // From screen space (viewport) [0, 1] to ndc [-1, 1]
  // XXX move to an util func?
  vec3 ndc_position = v_position;
  ndc_position.x = -1 + ndc_position.x * 2;
  ndc_position.y = -1 + ndc_position.y * 2;
  
  //gl_Position = vec4(v_position, 1.0);
  gl_Position = vec4(ndc_position, 1.0);
}

