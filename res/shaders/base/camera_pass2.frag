#include "uniforms.frag"

uniform sampler2D fboTex;
uniform bool use_sharpen_pass = false;
uniform int effect_number = 0;

//vec2 buffersize = textureSize(fboTex, 0).xy;
float step_w = 1.0/buffersize.x;
float step_h = 1.0/buffersize.y;
//const float step_w = 1.0/800;
//const float step_h = 1.0/600;

vec2 offset[9] = vec2[9](
    vec2(-step_w, -step_h),
    vec2(0.0, -step_h),
    vec2(step_w, -step_h),
    vec2(-step_w, 0.0),
    vec2(0.0, 0.0),
    vec2(step_w, 0.0),
    vec2(-step_w, step_h),
    vec2(0.0, step_h),
    vec2(step_w, step_h));

/* SHARPEN KERNEL
 0 -1  0
-1  5 -1
 0 -1  0
*/

const float kernel[9] = float[9](
    float(0.0), float(-0.1), float(0.0),
    float(-0.1), float(1.4), float(-0.1),
    float(0.0), float(-0.1), float(0.0));

void main()
{
    vec3 color = vec3(1.0, 0.0, 1.0);

    // XXX cannot  use sharppen pass with effect_number, need another fbo/rendertarget for sampling
    if (use_sharpen_pass && effect_number == 0)
    {
        vec3 sum = vec3(0.0);
        int i;
        for (i=0; i<9; i++) {
            vec3 _color = texture2D(fboTex, gl_TexCoord[0].st + offset[i]).rgb;
            sum += _color * kernel[i];
        }

        color = sum;
    }
    /*** post processing effects ***/
    // night vision
    else if(effect_number == 1)
    {
        color *= 0.3;
        //color *= 0.05;

        const float brightness = 4;
        float lum = dot(vec3(0.30, 0.59, 0.11), color);
        color = vec3(0.5, 0.6, 0.5) * max(.1, 1.0 - (lum*brightness));
    }
    else if(effect_number == 2)
    {
        float oExtinction = 0.4;
        color.r = mix(16.0/255.0, gl_FragColor.r ,oExtinction);
    }
    // sharpen
    else if(effect_number == 3)
    {
        float step_w = 1.0/buffersize.x;
        float step_h = 1.0/buffersize.y;

        vec2 offset[9] = vec2[9](
            vec2(-step_w, -step_h),
            vec2(0.0, -step_h),
            vec2(step_w, -step_h),
            vec2(-step_w, 0.0),
            vec2(0.0, 0.0),
            vec2(step_w, 0.0),
            vec2(-step_w, step_h),
            vec2(0.0, step_h),
            vec2(step_w, step_h));

        /* SHARPEN KERNEL
         0 -1  0
        -1  5 -1
         0 -1  0
        */

        const float kernel[9] = float[9](
            float(0.), float(-.1), float(0.),
            float(-.1), float(1.4), float(-.1),
            float(0.), float(-.1), float(0.));

        vec3 sum = vec3(0.0);
        int i;
        for (i=0; i<9; i++) {
            // fixme chain post-processing
            vec3 _color = texture2D(fboTex, gl_TexCoord[0].st + offset[i]).rgb;
            sum += _color * kernel[i];
        }

        color = sum;
    }
    // blur
    else if(effect_number == 4)
    {
        float step_w = 1.0/buffersize.x;
        float step_h = 1.0/buffersize.y;

        vec2 offset[9] = vec2[9](
            vec2(-step_w, -step_h),
            vec2(0.0, -step_h),
            vec2(step_w, -step_h),
            vec2(-step_w, 0.0),
            vec2(0.0, 0.0),
            vec2(step_w, 0.0),
            vec2(-step_w, step_h),
            vec2(0.0, step_h),
            vec2(step_w, step_h));

        const float kernel[9] = float[9](
            float(1), float(.25), float(1),
            float(.25), float(1), float(.25),
            float(1), float(.25), float(1));
        const float conWeight = 1.0/6.0;

        vec3 sum = vec3(0.0);
        int i;
        for (i=0; i<9; i++) {
            // fixme chain post-processing
            vec3 _color = texture2D(fboTex, gl_TexCoord[0].st + offset[i]).rgb;
            sum += _color * kernel[i];
        }

        color = 0.3 + (sum * conWeight) * 0.9;
    }
    else if(effect_number == 5)
    {
        //u_Scale is vec2(0, 1.0/height ) for vertical blur and vec2(1.0/width, 0 ) for horizontal blur. 
        //const vec2 u_Scale = vec2(1.0/1024, 0);
        vec2 u_Scale = vec2(4.0/buffersize.x, 0);
        const vec2 gaussFilter[7] = vec2[7](
                    vec2(-3.0,	0.015625),
                    vec2(-2.0,	0.09375),
                    vec2(-1.0,	0.234375),
                    vec2(0.0,	0.3125),
                    vec2(1.0,	0.234375),
                    vec2(2.0,	0.09375),
                    vec2(3.0,	0.015625));
 
	vec4 c = vec4(0.0);
	for(int i=0; i<7; i++)
	{
	    c += texture2D(fboTex, vec2( gl_TexCoord[0].x+gaussFilter[i].x*u_Scale.x, gl_TexCoord[0].y+gaussFilter[i].x*u_Scale.y ) )*gaussFilter[i].y;
	}
 
	color = c.rgb;
    }
    else {
        color = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    }


    gl_FragColor.rgb = color;
}
