#if 0
vec3 calcLambertLight(vec3 L, vec3 N, vec3 mat_diffuse, float light_intensity)
{
    vec3 l = normalize(L); // light vec
    vec3 n = normalize(N); // normal vec

    // out
    vec3 color;
    vec3 diffuse = vec3(0);

#ifdef _NORMAL_MAP
    n = texture2D(normalMap, gl_TexCoord[2].st).rgb * 2.0 - 1.0;
#endif

    float NdotL = max(dot(n,l), 0.0);

    // if(NdotL)  > 0.0
    diffuse = mat_diffuse * light_intensity * NdotL;
    //diffuse = clamp(diffuse, 0.0, 1.0);
    color = diffuse * diffuse_intensity;

    return color;
}
#endif

// XXX FIXME: `light` is not used
vec3 calcLambertLight2(vec3 light, vec3 f_normal, vec3 mat_diffuse)
{
    vec3 normal = normalize(f_normal); // normal vec

	bool directional = false;

	if (directional) {
		//light = -normalize(lights[i].target - lights[i].position);
	} else {
		light = normalize(lightPosition - f_position);
	}

    // out
    vec3 color;
    vec3 diffuse = vec3(0);

    float NdotL = max(dot(normal, light), 0.0);

    // if(NdotL)  > 0.0
    diffuse = mat_diffuse * NdotL;
    //diffuse = clamp(diffuse, 0.0, 1.0);
    //color = diffuse * diffuse_intensity; 

	color = diffuse;

    return color;
}

#if 0
vec3 calcPhongLight(vec3 L, vec3 N, vec3 V,
                    vec3 mat_diffuse, vec3 mat_specular, float mat_hardness,
                    float light_intensity)
{
    vec3 l = normalize(L); // light vec
    vec3 n = normalize(N); // normal vec
    vec3 v = normalize(-V); // view vec (eyeVec)
    vec3 h = normalize(L + V);  // half vec
    //vec3 h = normalize(H);

    // out
    vec3 color;
    vec3 diffuse = vec3(0);
    vec3 specular = vec3(1);

#ifdef _NORMAL_MAP
    /*
    float contrast = 1.0; // bumpness
    vec3 avg = vec3(0.5, 0.5, 1.0); // mid color
    n = mix(avg, texture2D(normalMap, gl_TexCoord[2].st).rgb, contrast) * 2.0 - 1.0;
    */
    n = texture2D(normalMap, gl_TexCoord[2].st).rgb * 2.0 - 1.0;
    //n = normalize(n);
#endif

    float NdotL = max(dot(n,l), 0.0);
    float NdotH = max(dot(n,h), 0.0);

    // if(NdotL)  > 0.0
    diffuse = mat_diffuse * light_intensity * NdotL;
    //diffuse = clamp(diffuse, 0.0, 1.0);
    specular = mat_specular * pow(NdotH, mat_hardness); // 1.0
    //float sharpness = 0.1;
    //specular = vec3(1) * sharpness * pow(NdotH, mat_hardness); // 1.0
    //specular = mat.specular * pow(NdotH, mat.hardness) * (NdotL > 0.0 ? 1.0 : 0.0);
    //specular = clamp(specular, 0.0, 1.0);


    color = diffuse * diffuse_intensity + specular * specular_intensity;

    return color;
}
#endif

#if 0
float calcShadow(float intensity) {
/*
    vec3 l = normalize(L); // light vec
    vec3 n = normalize(N); // normal vec
#ifdef _NORMAL_MAP
    //n = texture2D(normalMap, gl_TexCoord[2].st).rgb * 2.0 - 1.0;
    float contrast = 1.0; // bumpness
    vec3 avg = vec3(0.5, 0.5, 1.0); // mid color
    n = mix(avg, texture2D(normalMap, gl_TexCoord[2].st).rgb, contrast) * 2.0 - 1.0;
    //n = normalize(n);
#endif
    float cosTheta = clamp(dot(n,l), 0.0, 1.0);
*/

    float shadow = 1.0;
    if (use_shadows) {
        //float bias  = 0.005*tan(acos(cosTheta));
        //bias  = clamp(bias, 0, 0.01);
        //float bias = 0.00042;
        float bias = 0.001;
        if (texture2D( shadowMap, shadowCoord.xy).z  <  shadowCoord.z-bias) {
            shadow = 1.0 - intensity;
        }
    }
    return shadow;
}

float calcShadow() {
    return calcShadow(shadow_intensity); // default
}
#endif
