#include "uniforms.frag"
#include "lighting.frag"


void main() {
    vec3 color;
	float color_a = 1.0;

    //color = ambient + calcLambertLight(L, N, mat.diffuse, light.intensity);
    color = ambient + calcLambertLight2(lightPosition, f_normal, mat.diffuse);

#ifdef _TEXTURE_MAP
    vec4 tex = texture2D(texMap, f_texCoord);
	//if(tex.a < 0.5)
	//	discard;
    color *= tex.rgb;
	color_a = tex.a;
#endif

#ifdef _SHADOWS
    //color *= calcShadow();
#endif

    //gl_FragColor.rgb = color;
    gl_FragColor = vec4(color, color_a);
}
