//#define _TEXTURE_MAP 1
#include "uniforms.frag"
//uniform vec4 fill_color;
varying vec3 f_color;


void main() {
    //vec4 color = fill_color;
    //vec3 color = f_color;
    //vec3 color = vec3(1.0, 0.0, 0.0);
    vec3 color = mat.diffuse;
    
    //vec4 tex = texture2D(texMap, f_texCoord);
    //color.rgb *= tex.rgb;
    //color.rgb = color.rgb * 0.5 + tex.rgb * 0.5;
    //color.rgb += tex.rgb * 0.1;
    //color.rgb = tex.rgb;
    
    vec4 c;
    //c.rgb = tex.rgb;
    //c.a = tex.a;
    
    c = vec4(color, 1.0);
    //c += tex * 0.0001;
    

    //gl_FragColor = color;
    //gl_FragColor = vec4(color, 1.0);
   	gl_FragColor = c;
}
