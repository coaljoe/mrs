#include "uniforms.frag"
#include "lighting.frag"
varying vec4 wpos;

void main() {
    vec3 color;

//    color = ambient + calcLambertLight(L, N, mat.diffuse, light.intensity);

#ifdef _TEXTURE_MAP
    color *= texture2D(texMap, gl_TexCoord[0].st).rgb;
#endif

//    color *= calcShadow();
    //float c = wpos.y / 32.0;

    //float c = 0.5 + (0.1 * (wpos.y / 8.0));
    //color = vec3(c, c, c);

    const float hstep = 8.0; // horizontal scale
    //const float numLevs = 8.0;
    //float lev = wpos.y / numLevs;
    //float lev = wpos.y / hstep; // nice effect with the old y
    float lev = wpos.z / hstep;
    float cstep = 0.1;
    if (lev < 0.0 && lev >= -0.4) {
        //color = vec3(0.5, 0.5, 0.8);
		
		float v = (1.0 - 0.0)/(0.0 - -0.4)*(lev - 0.0) + 1.0;
        color = mix(vec3(0.95, 0.95, 0.95),
				    //vec3(0.5, 0.5, 0.8),
				    vec3(0.1, 0.1, 0.4),
				   	1.0 - v*8);
				   	//1.0 - v/4);
					
    }
    else {
/*
        // Flat color version
        float c;
        if (lev < 0.0) {
            c = 0.5 + (cstep * lev);
        }
        else {
            // inverse
            //c = 1.1 - (cstep*2 * -lev);
            c = 0.95 - (cstep * -lev);
        }
        color = vec3(c, c, c);
*/
        float c;
        if (lev < 0.0) {
            //c = 0.5 + (cstep * lev);
            //c = 0.5 + ((cstep * lev)*0.66); // Softer trans. levels
            //c = 0.5 + ((cstep * lev)*0.75);
            //c = 0.5 + (cstep * lev*1.5); // Harder trans. step
            c = 1.5 + (cstep * lev*4); // Harder trans. step
			// Neutral color
            //color = vec3(c, c, c);
			// The blue is more intensive
            //color = vec3(c, c, c * 1.16);
            //color = vec3(c * 0.8, c * 0.8, c * 0.96);
			//float bshift = 0.2;
			if (lev < -1.0) {
			    float bshift = 0.55;
                color = vec3(c * (1.0-bshift), c * (1.0-bshift), c * bshift);
			} else {
                color = vec3(0.61, 0.76, 0.8);
                //color = vec3(1.0, 0, 0);
			}
		}
        else {
            // inverse
            //c = 1.1 - (cstep*2 * -lev);
            c = 0.95 - (cstep * -lev);
            color = vec3(c, c, c);
        }
    }
    //color = vec3(1.0, wpos.y, 1.0);

    gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
