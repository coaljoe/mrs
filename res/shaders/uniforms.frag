uniform vec2 buffersize;

struct Material {
    vec3 diffuse;
    vec3 specular; // .1, .1, .1
    float hardness; // 50
};
struct Light {
    float intensity; // 1.0
	//vec3 position;
};

uniform Material mat;
uniform Light light; // sun
uniform bool use_shadows = false;

uniform vec3 ambient = vec3(.6);
//uniform float diffuse_intensity = 1.0;
//uniform float specular_intensity = 1.0;
//uniform float shadow_intensity = 0.2;

uniform vec3 lightPosition;

const int MAX_LIGHTS = 8;

// tunable variables
/*
const vec3 ambient = vec3(.6);
const float diffuse_intensity = 1.0;
const float specular_intensity = 1.0;
const float shadow_intensity = 0.2;
*/

/*
varying vec3 L; // light position
varying vec3 N;
varying vec3 V;
varying vec3 H;
*/

varying vec3 f_position;
varying vec3 f_normal;
varying vec2 f_texCoord;

#ifdef _TEXTURE_MAP
uniform sampler2D texMap;
//varying vec2 UV;
#endif

#ifdef _NORMAL_MAP
/*
uniform sampler2D normalMap;
//varying vec2 UV;
*/
#endif

#define _SHADOWS 0
#ifdef _SHADOWS
// shadows
uniform sampler2D shadowMap;
varying vec4 shadowCoord;
#endif
