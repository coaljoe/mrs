#include "uniforms.frag"
#include "lighting.frag"
uniform vec4 fill_color;
//uniform vec2 scale_texCoord; //= vec2(0.5, 0.5);
uniform vec2 scale_texCoord = vec2(1.0, 1.0); // XXX default
//uniform vec2 scale_texCoord = vec2(2.0, 2.0);
//uniform vec2 scale_texCoord = vec2(0.5, 0.5);
//uniform vec2 scale_texCoord = vec2(1.0/6.0, 1.0/6.0);
//uniform vec2 shift_texCoord = vec2(0.5, 0.5);
//uniform vec2 shift_texCoord = vec2(1.0, 0);
//uniform vec2 shift_texCoord = vec2(2.0, 0);
//uniform vec2 shift_texCoord = vec2(0.5, 0);
uniform vec2 shift_texCoord; // XXX default


void main() {
    vec3 color;
	float color_a = 1.0;

    //color = ambient + calcLambertLight(L, N, mat.diffuse, light.intensity);
    //color = ambient + calcLambertLight2(lightPosition, f_normal, mat.diffuse);
    //color = ambient + mat.diffuse + (f_normal * 0.00000001);
    color =  vec3(1.0, 1.0, 1.0) + (f_normal * 0.00000001); // fixme?
 

#ifdef _TEXTURE_MAP
	vec2 tc = f_texCoord;
	//tc *= scale_texCoord;
	//tc += shift_texCoord;
	tc.x = (tc.x + shift_texCoord.x) * scale_texCoord.x;
	tc.y = (tc.y + shift_texCoord.y) * scale_texCoord.y;
    vec4 tex = texture2D(texMap, tc);
	//if(tex.a < 0.5)
	//	discard;
    color *= tex.rgb;
    color_a = tex.a;
    
    //color = (color / 2) * (tex.rgb / 2);
    //color_a = 0.5;
    //color = color + (tex.rgb * 0.000001);
    //color = color * (tex.rgb * 0.5);
    //color_a = 0.8;
#endif

#ifdef _SHADOWS
    //color *= calcShadow();
#endif

	vec4 color_ = fill_color;
    //gl_FragColor = xcolor;


    gl_FragColor.rgb = color;
    gl_FragColor = vec4(color, color_a);
}
