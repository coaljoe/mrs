// Game's keyboard configuration.
package main

import (
	"encoding/json"
	"fmt"

	"github.com/go-gl/glfw/v3.3/glfw"
)

type KeymapRec struct {
	Name    string
	Key1    glfw.Key // Default key combo, currently supports
	Key2    glfw.Key // Only two keys, ex: Ctrl+Q
	AltKey1 glfw.Key // Alt. key combo
	AltKey2 glfw.Key
	//action      func
	Description     string `json:"-"` // Key description
	DescriptionLong string `json:"-"` // Long key description
	Disabled        bool   // The keymap is disabled
}

type Keymap struct {
	*GameSystem
	Keymap map[string]KeymapRec `json:"keymap"`
}

func newKeymap() *Keymap {
	k := &Keymap{
		Keymap: make(map[string]KeymapRec, 0),
	}
	k.GameSystem = newGameSystem("Keymap", "Keymap subsystem", k)
	return k
}

func (k *Keymap) init() {

}

func (k *Keymap) reinit() {

}

func (k *Keymap) start() {
	// Add some keymaps

	// Game keys
	k.addKey(KeymapRec{Name: "weapon1Key", Key1: glfw.KeyZ, //glfw.KeyS,
		Description:     "Weapon 1",
		DescriptionLong: ""})
	k.addKey(KeymapRec{Name: "weapon2Key", Key1: glfw.KeyX, //glfw.KeyD,
		Description:     "Weapon 2",
		DescriptionLong: ""})
	k.addKey(KeymapRec{Name: "moveUpKey", Key1: glfw.KeyUp,
		Description: "Move up"})
	k.addKey(KeymapRec{Name: "moveDownKey", Key1: glfw.KeyDown,
		Description: "Move down"})
	k.addKey(KeymapRec{Name: "moveLeftKey", Key1: glfw.KeyLeft,
		Description: "Move left"})
	k.addKey(KeymapRec{Name: "moveRightKey", Key1: glfw.KeyRight,
		Description: "Move right"})

	/*
		k.addKey(KeymapRec{Name: "rebuildLastBuildingKey", Key1: glfw.KeyF3,
			Description:     "SaveLoad test",
			DescriptionLong: ""})
	*/
	k.addKey(KeymapRec{Name: "pause", Key1: glfw.KeySpace,
		Description:     "Game pause",
		DescriptionLong: ""})

	//pp(k.getKey1("scrollUpKey"))
	//pp(k.getAltKey1("scrollUpKey"))
}

func (k *Keymap) addKey(rec KeymapRec) {
	k.Keymap[rec.Name] = rec
}

func (k *Keymap) getKey1(recName string) glfw.Key {
	return k.Keymap[recName].Key1
}

func (k *Keymap) getKey2(recName string) glfw.Key {
	return k.Keymap[recName].Key2
}

func (k *Keymap) getAltKey1(recName string) glfw.Key {
	return k.Keymap[recName].AltKey1
}

func (k *Keymap) getAltKey2(recName string) glfw.Key {
	return k.Keymap[recName].AltKey2
}

func (k *Keymap) saveToJson() string {
	//di, err := json.Marshal(k)
	di, err := json.MarshalIndent(k, "", "  ")
	if err != nil {
		p("[Keymap] saveToJson failed.")
		panic(err)
	}
	fmt.Println(di)
	fmt.Println(string(di))
	return string(di)
}

func (k *Keymap) loadFromJson(s string) {
	err := json.Unmarshal([]byte(s), &k)
	if err != nil {
		p("[Keymap] loadFromFile failed.")
		panic(err)
	}
}

func (k *Keymap) update(dt float64) {

}
