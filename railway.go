package main

// Railway type
// https://wiki.openstreetmap.org/wiki/OpenRailwayMap/Tagging
// https://www.tt-forums.net/viewtopic.php?t=84591
// http://trn.trains.com/railroads/abcs-of-railroading/2006/05/track-classifications
// TODO: rename to railtacktype?
type RailwayType struct {
	WayType
	//gauge int
	//highspeed   bool // speed > 200kph
	embedded    bool // for use in cities?
	ballastless bool
	//metreLoad     float64
	//axleLoad      float64

	fence bool // XXX fence like in openttd?
}

func (rt RailwayType) highspeed() bool {
	return rt.maxSpeed > 200
}

// TODO: use map with ids?
var railwayTypes []*RailwayType

func init() {
	// Create rail types
	// Class 1: 10 mph for freight, 15 mph for passenger.
	// Much yard, branch line, short line, and industrial spur trackage falls into category.
	rt := &RailwayType{
		WayType: WayType{
			id:          0,
			name:        "rt_25kmh",
			description: "25 km/h short line",
			maxSpeed:    25,
			//maxSpeed: 10,
			cost: 5,
		},
	}
	railwayTypes = append(railwayTypes, rt)

	rt = &RailwayType{
		WayType: WayType{
			id:          1,
			name:        "rt_60kmh",
			description: "60 km/h branch line (wooden)",
			maxSpeed:    60,
			cost:        10,
		},
	}
	railwayTypes = append(railwayTypes, rt)

	rt = &RailwayType{
		WayType: WayType{
			id:          2,
			name:        "rt_100kmh",
			description: "100 km/h main line",
			maxSpeed:    100,
			cost:        10,
		},
	}
	railwayTypes = append(railwayTypes, rt)

	rt = &RailwayType{
		WayType: WayType{
			id:          3,
			name:        "rt_140kmh",
			description: "140 km/h fast line",
			maxSpeed:    140,
			cost:        50,
		},
	}
	railwayTypes = append(railwayTypes, rt)

	rt = &RailwayType{
		WayType: WayType{
			id:              4,
			name:            "rt_220kmh",
			description:     "220 km/h high speed line",
			maxSpeed:        220,
			maxSpeedFreight: 120,
			cost:            200,
		},
	}
	railwayTypes = append(railwayTypes, rt)

}
