package main

import (
	//"fmt"
	. "math"

	//ps "lib/pubsub"
	ps "kristallos.ga/lib/pubsub"
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"

	"github.com/go-gl/glfw/v3.3/glfw"
)

type CameraState int

const (
	CameraState_Idle      CameraState = iota
	CameraState_Scrolling             // Panning
)

type Camera struct {
	cam              *rx.Node // Fixme: rename to camn
	mx, my           int
	pos, rot         Vec3
	prevPos, prevRot Vec3
	rad,
	rotSpeed,
	rotStep,
	tilt,
	tiltMin,
	tiltMax,
	tiltStep,
	zoomSpeed,
	zoomStep,
	zoomMin,
	zoomMax,
	defaultZoom,
	velocity,
	maxVelocity float64
	state CameraState
}

func newCamera() *Camera {
	c := &Camera{
		state: CameraState_Idle,
	}
	c.setDefaultCamera()
	sub(rx.Ev_mouse_move, c.onMouseMove)
	sub(rx.Ev_key_press, c.onKeyPress)
	return c
}

func (c *Camera) setDefaultCamera() {
	c.rad = Sqrt(Pow(10, 2) + Pow(10, 2)) // Diagonal
	c.rotSpeed = 60
	c.rotStep = 15 //360/16
	c.tilt = 35.264
	c.tiltMin = 20
	c.tiltMax = 60
	c.tiltStep = 10
	c.zoomSpeed = 1
	c.zoomStep = 1
	c.zoomMin = 1
	//c.zoomMin = 0.5
	c.zoomMax = 4
	c.defaultZoom = 3
	c.maxVelocity = 10

	// Standart camera
	c.pos = Vec3{30, 30, 30}
	//c.trg = Vec3{0, 0, 0}

	// Blender camera1
	c.pos = Vec3{30, 30, 30}
	//c.trg = Vec3{0, 0, 0}

	rxi := rx.Rxi()

	/* add camera */

	//app.rxi.camera.transform(10, 10, 10,  -35.264, 45, 0)
	cam := rxi.Camera
	cam.Camera.SetZoom(10)
	cam.Camera.SetZnear(1)
	cam.Camera.SetZfar(1000)
	//cam.Pos = rx.NewVector3(10, 20, 10)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)
	//cam.Pos = rx.NewVector3(0, 50, 0)
	//cam.Rot = rx.NewVector3(90, 0, 0)
	//cam.SetPos(Vec3{-5, 5, -5})
	//cam.SetPos(Vec3{5, 5, 5})
	//cam.SetPos(Vec3{30, 30, 30})
	cam.SetPos(c.pos)
	//cam.Rot = rx.NewVector3(90+35.264, 45, 0)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)

	//cam.SetTarget(c.trg)

	println(cam.Pos().String())
	println(cam.Rot().String())

	c.cam = cam
}

func (c *Camera) setNamedCamera(name string) {
	// Blender-like top camera.
	// Blender:
	// -pos: 0.5, -2.15, 10
	// -rot: 12, 0, 11.8
	if name == "blender1" {

		c.pos = Vec3{5, 100, 21.5} // XXX: not used?
		//c.trg = Vec3{0, 0, 0}
		c.rot = Vec3{12, 0, 11.8}
		// Rot
		// Blender: 12, 0, 11.8

		zoomValue := 11.5 * 2
		// Calculate scale coeff.
		//resScaleX := float64(vars.resX) / float64(vars.nativeResX)
		resScaleY := float64(vars.resY) / float64(vars.nativeResY)
		// Adjust Zoom so its resolution-independent
		//zoomValue *= resScaleX
		zoomValue *= resScaleY

		//c.cam.SetZoom(20)
		//c.cam.SetZoom(11.5)
		c.cam.Camera.SetZoom(zoomValue)

		// Move camera far back so it won't collide with meshes
		c.cam.MoveByVec(Vec3{0, 0, 100})

		c._build()
		/*
			// Blender-like top camera.
			// Blender:
			// -pos: 0.5, -2.15, 10
			// -rot: 12, 0, 11.8
			if name == "blender1" {

				c.pos = Vec3{5, 100, 21.5}
				c.trg = Vec3{0, 0, 0}
				// Rot
				// Blender: 12, 0, 11.8

				//c.cam.SetZoom(20)
				//c.cam.SetZoom(11.5)
				c.cam.SetZoom(11.5*2)

				c.build()
		*/
	} else if name == "topdown" {

		//c.pos = Vec3{5, 100, 21.5} // XXX: not used?
		//c.trg = Vec3{0, 0, 0}
		//c.rot = Vec3{12, 0, 11.8}
		c.rot = Vec3{0, 0, 0}
		// Rot
		// Blender: 12, 0, 11.8

		zoomValue := 11.5 * 2
		// Calculate scale coeff.
		//resScaleX := float64(vars.resX) / float64(vars.nativeResX)
		resScaleY := float64(vars.resY) / float64(vars.nativeResY)
		// Adjust Zoom so its resolution-independent
		//zoomValue *= resScaleX
		zoomValue *= resScaleY

		//c.cam.SetZoom(20)
		//c.cam.SetZoom(11.5)
		//c.cam.Camera.SetZoom(zoomValue)
		c.cam.Camera.SetZoom(18)

		// Move camera far back so it won't collide with meshes
		//c.cam.MoveByVec(Vec3{0, 0, 100})

		c._build()
		c.pan(0, 20)
	} else if name == "iso" {

		//c.pos = Vec3{5, 100, 21.5} // XXX: not used?
		//c.trg = Vec3{0, 0, 0}
		//c.rot = Vec3{12, 0, 11.8}

		c.pos = Vec3{12.24745, -12.24745, 10.0}
		// Game iso 1:2
		c.rot = Vec3{60, 0, 45}
		// True iso
		//c.rot = Vec3{54.736, 0, 45}
		c.cam.SetPos(c.pos)
		c.cam.SetRot(c.rot)

		// Rot
		// Blender: 12, 0, 11.8

		// new rot-based coords
		//c.rot = Vec3{10, -10, 5}
		//c.pos = Vec3{70.562, 0, 45}

		//zoomValue := 11.5 * 2
		zoomValue := 11.5 * 2
		// Calculate scale coeff.
		//resScaleX := float64(vars.resX) / float64(vars.nativeResX)
		resScaleY := float64(vars.resY) / float64(vars.nativeResY)
		// Adjust Zoom so its resolution-independent
		//zoomValue *= resScaleX
		zoomValue *= resScaleY

		//c.cam.SetZoom(20)
		//c.cam.SetZoom(11.5)
		//c.cam.Camera.SetZoom(zoomValue)
		//c.cam.Camera.SetZoom(18)
		c.cam.Camera.SetZoom(10)

		// Move camera far back so it won't collide with meshes
		//c.cam.MoveByVec(Vec3{0, 0, 100})
		c.cam.MoveByVec(Vec3{0, 0, 60})

		p("before:", c.pos, c.rot, c.cam.Pos(), c.cam.Rot())
		c._build()
		p("after:", c.pos, c.rot, c.cam.Pos(), c.cam.Rot())
		//c.pan(0, 20)
	} else {
		pp("unknown camera; name: ", name)
	}
}

func (c *Camera) onMouseMove(ev *ps.Event) {
	//pp(ev.Data.(struct{ x, y int }))
	//p := ev.Data.(struct{ x, y int })
	//pp(ev.Data)
	p := ev.Data.(rx.EvMouseMoveData)
	_log.Trc("cam.onmousemove", p.X, p.Y)
	c.mx = p.X
	c.my = p.Y
}

func (c *Camera) onKeyPress(ev *ps.Event) {
	k := ev.Data.(glfw.Key)
	_log.Dbg("cam.onkeypress", k)
	//dt := game.app.GetDt() // Fixme
	/*
		switch k {
		case vars.scrollUpKey, vars.scrollDownKey,
				vars.scrollLeftKey, vars.scrollRightKey:
			//c.pos.Y += vars.scrollSpeed * dt
			c.velocity = c.maxVelocity
		}
	*/
}

func (c *Camera) _build() {
	c.prevPos = c.pos   // Update prevPos XXX: fixme?
	c.pos = c.cam.Pos() // Update c.pos
	c.cam.SetPos(c.pos)
	c.cam.SetRot(c.rot)
	//pp(c.prevPos, c.pos)
}

/*
// Move camera and target to a new position.
func (c *Camera) MoveTargetTo(v Vec3) {
	diff := c.pos.Sub(c.trg)
	//pp(diff)
	c.pos = v.Add(diff)
	c.trg = v
	c.cam.SetPos(c.pos)
	c.cam.SetTarget(c.trg)
}
*/

func (c *Camera) moveAdj(v Vec3) {
	c.pos = c.pos.Add(v)
	//c.trg = c.trg.Add(v)
	c.cam.SetPos(c.pos)
}

// Main way to pan camera.
func (c *Camera) pan(x, y float64) {
	// Move abvolutely
	//c.translate(x, y, 0)
	//correct_height(c.cam)

	// Move relatively
	zPrev := c.cam.PosZ()
	c.cam.MoveByVec(Vec3{x, y, 0})
	zNow := c.cam.PosZ()
	zCorr := zPrev - zNow
	_ = zCorr
	// TODO: fix z-position
	//p("zCorr:", zCorr)
	// Return previous z-position
	//c.cam.AdjPos(Vec3{0, 0, zCorr})

	c._build()
}

func (c *Camera) translate(x, y, z float64) {
	c.cam.Translate(Vec3{x, y, z})
	// Fixme: update _moveAdj
	//c._moveAdj = c._moveAdj.Add(Vec3{x, y, z})
	c._build()
}

func (c *Camera) update(dt float64) {
	//vel := c.maxVelocity
	//vel := vars.scrollSpeed
	c.cam.SetPos(c.pos)
	c.cam.SetRot(c.rot)

	/*
		c.pos = c.pos.Add(moveAdj)
		c.trg = c.trg.Add(moveAdj)
		c.cam.SetPos(c.pos)
		c.cam.SetTarget(c.trg)
	*/
}
