module gitlab.com/coaljoe/mrs

go 1.19

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/drbig/perlin v0.0.0-20141206142450-e4c467936143
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210727001814-0db043d8d5be
	github.com/go-spatial/tegola v0.10.4
	github.com/google/go-jsonnet v0.15.0
	github.com/hjson/hjson-go v3.0.1+incompatible
	github.com/paulmach/osm v0.1.0
	github.com/pelletier/go-toml v1.7.0
	gonum.org/v1/gonum v0.7.0
	//bitbucket.org/coaljoe/lib v0.0.0-20200427192711-0f2a343b3155
	//bitbucket.org/coaljoe/lib/debug v0.0.0-20200427195229-c4b5b0335e26
	//bitbucket.org/coaljoe/lib/debug/extra v0.0.0-20200401115413-0af24abf6199
	//bitbucket.org/coaljoe/lib/ider v0.0.0-20200427195229-c4b5b0335e26
	//bitbucket.org/coaljoe/lib/sr v0.0.0-20200401115413-0af24abf6199 // indirect
	//bitbucket.org/coaljoe/rx v0.0.0
	//bitbucket.org/coaljoe/rx/math v0.0.0
	//bitbucket.org/coaljoe/rx/transform v0.0.0-20200411182640-295f5c63143d

	kristallos.ga/lib/debug v0.0.0-20210224191029-7556e6f5a067
	kristallos.ga/lib/debug/extra v0.0.0-20210224191029-7556e6f5a067
	kristallos.ga/lib/ider v0.0.0-20210224191029-7556e6f5a067
	kristallos.ga/lib/pubsub v0.0.0-20210224191029-7556e6f5a067
	kristallos.ga/lib/xlog v0.0.0-20210224191029-7556e6f5a067
	kristallos.ga/rx v0.0.0-20210204203003-c247cfc90488
	kristallos.ga/rx/math v0.0.0-20210204203003-c247cfc90488
	kristallos.ga/rx/transform v0.0.0-20210204203003-c247cfc90488
)

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/cheekybits/genny v1.0.0 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/go-gl/gl v0.0.0-20210905235341-f7a045908259 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/paulmach/orb v0.1.6 // indirect
	github.com/y0ssar1an/q v1.0.10 // indirect
	golang.org/x/exp v0.0.0-20190627132806-fd42eb6b336f // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa // indirect
	gonum.org/v1/netlib v0.0.0-20190331212654-76723241ea4e // indirect
	kristallos.ga/lib/sr v0.0.0-20210224191029-7556e6f5a067 // indirect
	kristallos.ga/rx/phys v0.0.0-20210204203003-c247cfc90488 // indirect
)

//replace bitbucket.org/coaljoe/rx/math => ../rx/math
//replace bitbucket.org/coaljoe/rx => ../rx_upstream
//replace bitbucket.org/coaljoe/rx => ../rx

//replace bitbucket.org/coaljoe/rx/math => ../rx/math
//replace bitbucket.org/coaljoe/rx/transform => ../rx/transform

// bb_local
// replace bitbucket.org/coaljoe/lib => ../bb_local/lib
// replace bitbucket.org/coaljoe/lib/debug => ../bb_local/lib/debug
// replace bitbucket.org/coaljoe/lib/debug/extra => ../bb_local/lib/debug/extra
// replace bitbucket.org/coaljoe/lib/ider => ../bb_local/lib/ider
// replace bitbucket.org/coaljoe/lib/sr => ../bb_local/lib/sr
// replace bitbucket.org/coaljoe/rx => ../bb_local/rx
// replace bitbucket.org/coaljoe/rx/math => ../bb_local/rx/math
// replace bitbucket.org/coaljoe/rx/transform => ../bb_local/rx/transform
// replace bitbucket.org/coaljoe/rx/phys => ../bb_local/rx/phys
